// Import Thư viện Express
const mongoose = require("mongoose");
// Khai báo Schema
const Schema = mongoose.Schema;
// Khởi tạo schema
const orderDetailSchema = new Schema({
  product: {
    type: Schema.Types.ObjectId,
    ref: "Product",
  },
  quantity: {
    type: Number,
    default: 0,
  },
});
// Biên dịch Schema
module.exports = mongoose.model("OrderDetail", orderDetailSchema);
