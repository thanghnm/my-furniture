// Import Thư viện Express
const mongoose = require("mongoose");
// Khai báo Schema
const Schema = mongoose.Schema;
// Khởi tạo schema
const productSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  name: {
    type: String,
    unique: true,
    required: true,
  },
  description: {
    type: String,
  },
  type: {
    type: Schema.Types.ObjectId,
    ref: "productType",
    required: true,
  },
  imageUrl: {
    type: String,
    required:true
  },
  price: {
    type: Number,
    required: true,
  },
  buyPrice: {
    type: Number,
    required: true,
  },
  amount: {
    type: Number,
    default: 0,
  },
});
// Biên dịch Schema
module.exports = mongoose.model("Product", productSchema);
