// Import Thư viện Express
const mongoose = require("mongoose");
// Khai báo Schema
const Schema = mongoose.Schema;
// Khởi tạo schema
const productImageSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  imageData: {
    data: Buffer,
    contentType: String,
  },
  path:{
    contentType:String,
  }
});
// Biên dịch Schema
module.exports = mongoose.model("ProductImage", productImageSchema);
