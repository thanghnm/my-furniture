// Import Thư viện Express
const mongoose = require("mongoose");
// Khai báo Schema
const Schema = mongoose.Schema;
// Khởi tạo schema
const productTypeSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  name: {
    type: String,
    unique: true,
    required: true,
  },
});
// Biên dịch Schema
module.exports = mongoose.model("productType", productTypeSchema);
