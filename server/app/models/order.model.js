// Import Thư viện Express
const dayjs = require("dayjs");
const mongoose = require("mongoose");
// Khai báo Schema
const Schema = mongoose.Schema;
const characters =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
// Hàm tạo chuỗi ngẫu nhiên 6 ký tự từ mảng ký tự
function generateRandomString() {
  let result = "";
  for (let i = 0; i < 6; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    result += characters[randomIndex];
  }
  return result;
}

// Khởi tạo schema
const orderSchema = new Schema({
  _id: mongoose.Types.ObjectId,
  orderDate: {
    type: Date,
    default: Date.now(),
  },
  orderCode: {
    type: String,
    default: generateRandomString,
    unique: true,
  },
  shippedDate: {
    type: Date,
  },
  note: {
    type: String,
  },
  orderDetails: [{ type: Schema.Types.ObjectId, ref: "OrderDetail" }],
  cost: {
    type: Number,
    default: 0,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  status: {
    type: String,
    default: "Chưa xác nhận",
  },
});
// Biên dịch Schema
module.exports = mongoose.model("Order", orderSchema);
