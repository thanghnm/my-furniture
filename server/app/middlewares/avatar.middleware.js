var multer = require("multer");
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + "/../../uploads/avatar/");
  },
  filename: (req, file, cb) => {
    let imgName = "avatar" + "-" + req.body.phone;
    cb(null, imgName);
  },
});
var upload = multer({ storage: storage }).fields([{ name: 'avatar', maxCount: 1 }]);
module.exports = upload;
