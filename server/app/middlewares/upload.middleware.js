var multer = require("multer");
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + "/../../uploads/products/");
  },
  filename: (req, file, cb) => {
    let imgName = "image" + "-" + req.body.name;
    //  req.file.filename = imgName
    cb(null, imgName);
  },
});
var upload = multer({ storage: storage });
module.exports = upload;
