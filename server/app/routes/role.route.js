// Import Thư viện Express
const express = require("express");
const { createRole, getAllRole } = require("../controllers/role.controller");

// var router
const router = express.Router();
// Router create product type
router.post("/", createRole);
router.get("/", getAllRole);

module.exports = router;
