// Import Thư viện Express
const express = require("express");
var bodyParser = require("body-parser");
var upload = require("../middlewares/upload.middleware");
// Import Controller
const {
  createProduct,
  getAllProduct,
  getProductById,
  checkStock,
  updateProductById,
  deleteProductById,
} = require("../controllers/product.controller");
// Khai báo router
const router = express.Router();
router.use(bodyParser.json({ limit: "50mb" }));
router.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
// Router tạo product product
router.post("/", upload.single("productImg"), createProduct);
// Router lấy danh sách product
router.get("/", getAllProduct);
// Router kiem tra kho hang
router.post("/checkstock/", checkStock);
// Router lấy product bằng Id
router.get("/:productId/", getProductById);
// Router update product bằng Id
router.put("/:productId/",upload.single('productImg'), updateProductById)
// // // Router xóa product bằng Id
router.delete("/:productId/", deleteProductById)
// Export
module.exports = router;
