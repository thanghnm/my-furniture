// Import Thư viện Express
const express = require("express");
const { createOrderOfUser, getAllOrder, getAllOrderOfCustomer, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/order.controller");
// Import Controller
// Khai báo router
const router = express.Router();

// Router tạo order và add vào customer 
router.post("/:phone", createOrderOfUser)
// Router lấy danh sách order
router.get("/", getAllOrder)
// Router lấy danh sách order của customer
router.get("/user/:userId", getAllOrderOfCustomer)
// Router lấy order bằng Id
router.get("/all/:orderId", getOrderById)
// Router update order bằng Id
router.put("/:orderId", updateOrderById)
// // Router xóa order bằng Id
router.delete("/:orderId", deleteOrderById)
// Export
module.exports = router
