// Import Thư viện Express
const express = require("express");
// Import Controller
const {
  createProductType,
  getAllType,
} = require("../controllers/productType.controller");
// Khai báo router
const router = express.Router();

// Router tạo product type
router.post("/", createProductType);
// Router get all  product type
router.get("/", getAllType);
//
module.exports = router;
