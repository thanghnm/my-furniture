// Import Thư viện Express
const express = require("express");
// Import Controller
const {
  getAllUser,
  createUser,
  logIn,
  getUserById,
  getUserByOrderId,
  updateUserById,
  updateAvatar,
} = require("../controllers/user.controller");
var upload = require("../middlewares/avatar.middleware");

// var router
const router = express.Router();
// Router create product type
router.post("/", createUser);
// Router get all  product type
router.get("/", getAllUser);
// Router lấy user bằng Id
router.get("/:userId/", getUserById);
// Router login
router.put("/:userId",upload, updateUserById);
// Router login
router.post("/login", logIn);
// 
router.get("/ord/:orderId",getUserByOrderId)
module.exports = router;
