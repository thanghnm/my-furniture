const { v4: uuidv4 } = require("uuid");
const refreshTokenModel = require("../models/refreshToken.model");
const createToken = async (user) => {
  var expiredAt = new Date();
  expiredAt.setSeconds(expiredAt.getSeconds() + 86000);
  let token = uuidv4();
  let refreshTokenObj = new refreshTokenModel({
    token: token,
    user: user._id,
    expiredDate: expiredAt.getTime(),
  });
  const refreshToken = await refreshTokenObj.save();
  return refreshToken.token;
};
module.exports = { createToken };
