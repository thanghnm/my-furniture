//Import router
const productModel = require("../models/product.model");
const router = require("../routes/product.route");
var path = require("path");
var fs = require("fs");
var multer = require("multer");
const upload = require("../middlewares/upload.middleware");

// Import Thư viện Mongoose
const mongoose = require("mongoose");
// Hàm tạo productType
const createProduct = async (req, res) => {
  // B1 Thu thập dữ liệu
  const { name, description, type, price, buyPrice, amount } = req.body;
  // B2 Kiểm tra dữ liệu
  try {
    if (!name) {
      return res.status(400).json({
        status: "Bad request",
        message: "name is required",
      });
    }
    if (!type) {
      return res.status(400).json({
        status: "Bad request",
        message: "type is required",
      });
    }
    // const checkName=productModel.find({name:name})
    // if((await checkName).length>0){
    //     return res.status(400).json({
    //         status: "Bad request",
    //         message: "Tên sản phẩm đã được sử dụng"
    //     })
    // }
    if (!price) {
      return res.status(400).json({
        status: "Bad request",
        message: "price is required",
      });
    }
    if (!buyPrice) {
      return res.status(400).json({
        status: "Bad request",
        message: "buyPrice is required",
      });
    }

    // B3 Xử lý API
    // upload.single("productImg")
    const newProduct = {
      _id: new mongoose.Types.ObjectId(),
      name,
      description,
      imageUrl: path.join(
        __dirname + "../../../uploads/products/" + req.file.filename,
      ),
      price,
      buyPrice,
      amount,
      type,
    };
    var result = await productModel.create(newProduct);
    return res.status(201).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
const getAllProduct = async (req, res) => {
  // B1 Thu thập dữ liệu
  // B2 Kiểm tra dữ liệu
  // B3 Xử lý API
  const limit = req.query.limit ? req.query.limit : 0;
  const min = req.query.min ? req.query.min : 0;
  const max = req.query.max ? req.query.max : 100;
  const name = req.query.name ? req.query.name : false;
  const amount = req.query.amount ? req.query.amount : false;
  const price = req.query.price ? req.query.price : false;
  const buyPrice = req.query.buyPrice ? req.query.buyPrice : false;
  const page = req.query.page ? req.query.page : 1;
  const type = req.query.type ? req.query.type : false;
  var condition = {};
  if (type) {
    condition.type = type;
  }
  if (min && max) {
    condition.buyPrice = { $gte: min * 1000000, $lte: max * 1000000 };
  }
  if (name) {
    condition.name = { $regex: `.*${name}.*`, $options: 'i'  };
  }
  if (amount) {
    condition.amount = amount;
  }
  if (price) {
    condition.price = price;
  }

  try {
    var result;
    result = await productModel
      .find(condition)
      .limit(limit)
      .skip((page - 1) * limit)
      .populate("type");
    return res.status(200).json({
      result,
    });
  } catch (error) {
    console.log(error.message);
    return res.status(500).json({
      status: "Internal Server Error",
      message: error,
    });
  }
};
// Hàm lấy type bằng id
const getProductById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const productId = req.params.productId;
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined",
    });
  }
  // B3 Xử lý API
  try {
    var result = await productModel.findById(productId);
    return res.status(200).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Kiểm tra kho
const checkStock = async (req, res) => {
  try {
    // B1 Thu thập dữ liệu
    const order = req.body;
    // B2 Kiểm tra dữ liệu
    var message = [];
    for (let orderItem of order) {
      const product = await productModel.findById(orderItem.id).exec();
      if (!product || product.amount < orderItem.quantity) {
        var outstock = `${orderItem.name} Chỉ còn lại ${product.amount} cái.`;
        message.push(outstock);
      }
    }
    return res.status(200).json({
      message: message,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm update type bằng id
const updateProductById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const productId = req.params.productId
  const { name, description, type, buyPrice, price, amount } = req.body
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined"
    })
  }
  // B3 Xử lý API
  const oldVersion = await productModel.findById(productId)
  const oldName = oldVersion.name
  const oldPath = oldVersion.imageUrl
  const updateProduct = {
  }
  if (name !== oldName) {
    const newPath = path.join(
      __dirname + "../../../uploads/products/image-" + name,
    )
    updateProduct.name = name
    fs.rename(oldPath, newPath, (err) => {
      if (err) throw err;
      console.log('Đổi tên file thành công!');
    })
    updateProduct.imageUrl = newPath
  }
  else if (name !== oldName && req.file) {
    fs.unlinkSync(oldPath)
  }
  description ? updateProduct.description = description : [];
  req.file ?
    updateProduct.imageUrl = path.join(
      __dirname + "../../../uploads/products/image-" + name,
    ) : []
  price ? updateProduct.price = price : [];
  buyPrice ? updateProduct.buyPrice = buyPrice : [];
  amount ? updateProduct.amount = amount : [];
  type ? updateProduct.type = type : [];
  try {
    var result = await productModel.findByIdAndUpdate(productId, updateProduct)
    return res.status(200).json({
      message: "update successfully",
      result
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message
    })
  }
}
// Hàm xóa type bằng id
const deleteProductById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const productId = req.params.productId
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined"
    })
  }
  // B3 Xử lý API
  try {
    const product = await productModel.findById(productId)
    fs.unlinkSync(product.imageUrl)
    var result = await productModel.deleteOne({ _id: productId })
    return res.status(200).json({
      message: "delete successfully",
    });
  } catch (error) {
    // console.log(error)
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message
    })
  }
}

// Export
module.exports = { createProduct, getAllProduct, getProductById, checkStock, updateProductById, deleteProductById };
