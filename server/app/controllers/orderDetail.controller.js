//Import router
const orderDetailModel = require("../models/orderDetail.model")
const orderModel = require("../models/order.model")
const productModel = require("../models/product.model")
const router = require("../routes/orderDetail.route")
// Import Thư viện Mongoose
const mongoose = require("mongoose")
// Hàm tạo details và add vào order
const createDetailOfOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { product, quantity } = req.body
    const orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    const newDetail = {
        _id: new mongoose.Types.ObjectId,
        product,
        quantity
    }
    try {
        var result = await orderDetailModel.create(newDetail)
        var addDetails = await orderModel.findByIdAndUpdate(orderId, {
            $push: { orderDetails: result._id }
        })
        var findProduct = await productModel.findById(product)
        findProduct.amount = findProduct.amount - quantity
        await findProduct.save()
        return res.status(201).json({
            result,
            addDetails
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách order
const getAllDetailOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    const productId = req.query.productId?req.query.productId:0
    var condition = {}
    if(productId){
        condition={product:productId}
    }
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    try {
        var result = await orderDetailModel.find(condition)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy danh sách order của order theo id
const getAllDetailsOfOrder = async (req, res) => {
    // B1 Thu thập dữ liệu
    orderId = req.params.orderId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý API
    try {
        var result = await orderModel.findById(orderId)
        return res.status(200).json({
            result: result.orderDetails
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm lấy order bằng id
const getOrderDetailById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const detailId = req.params.detailId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(detailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await orderDetailModel.findById(detailId)
        return res.status(200).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm update order bằng id
const updateDetailById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const detailId = req.params.detailId
    const { product, quantity } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(detailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    const updateDetail = {
        product,
        quantity
    }
    try {
        var result = await orderDetailModel.findByIdAndUpdate(detailId, updateDetail)
        return res.status(200).json({
            message: "update successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm xóa order bằng id
const deleteDetailById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const detailId = req.params.detailId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(detailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id undefined"
        })
    }
    // B3 Xử lý API
    try {
        var result = await orderDetailModel.findByIdAndDelete(detailId)
        return res.status(200).json({
            message: "delete successfully",
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


// Exports
module.exports = { createDetailOfOrder, getAllDetailOrder, getAllDetailsOfOrder, getOrderDetailById, updateDetailById, deleteDetailById }