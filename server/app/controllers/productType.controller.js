//Import router
const productTypeModel = require("../models/productType.model");
// Import Thư viện Mongoose
const mongoose = require("mongoose");
// Hàm tạo productType
const createProductType = async (req, res) => {
  // B1 Thu thập dữ liệu
  const { name, description } = req.body;
  // B2 Kiểm tra dữ liệu
  if (!name) {
    return res.status(400).json({
      status: "Bad request",
      message: "name is required",
    });
  }
  // B3 Xử lý API
  const newProductType = {
    _id: new mongoose.Types.ObjectId(),
    name,
    description,
  };
  try {
    var result = await productTypeModel.create(newProductType);
    return res.status(201).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy danh sách type
const getAllType = async (req, res) => {
  // B1 Thu thập dữ liệu
  // B2 Kiểm tra dữ liệu
  // B3 Xử lý API
  try {
    var result = await productTypeModel.find();
    return res.status(200).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
module.exports = { createProductType, getAllType };
