//Import router
const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");
const orderDetailModel = require("../models/orderDetail.model");

// Import Thư viện Mongoose
const mongoose = require("mongoose");
// Hàm tạo order và add vào user
const createOrderOfUser = async (req, res) => {
  // B1 Thu thập dữ liệu
  const phone = req.params.phone
  const { address, shippedDate, note, details, cost, city } = req.body;
  // B2 Kiểm tra dữ liệu
  // B3 Xử lý API
  const newOrder = {
    _id: new mongoose.Types.ObjectId(),
    shippedDate,
    note,
    cost,
    address,
    city,
  };
  try {
    var order = await orderModel.create(newOrder);
    var addOrder = await userModel.findOneAndUpdate({ phone: phone }, {
      $push: { orders: order._id },
    });
    // Sử dụng Promise.all để đợi tất cả các truy vấn hoàn thành
    const createDetails = await Promise.all(
      details.map(async (element) => {
        const item = {
          product: element._id,
          quantity: element.quantity
        }
        const detailsItems = await orderDetailModel.create(item)
        const result = await orderModel.findByIdAndUpdate(order._id, { $push: { orderDetails: detailsItems._id } })
        return result;
      })
    );
    return res.status(201).json({
      data: createDetails
    });
  } catch (error) {
    console.log(error)
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy danh sách order
const getAllOrder = async (req, res) => {
  // B1 Thu thập dữ liệu
  const limit = req.query.limit;
  const page = req.query.page;
  const status = req.query.status ? req.query.status : false;
  const code = req.query.code ? req.query.code : false;
  // const itemNumbers = req.query.itemNumbers ? req.query.itemNumbers : false;
  // B2 Kiểm tra dữ liệu
  const condition = {};
  if (status) {
    condition.status = status;
  }
  if (code) {
    condition.orderCode = { $regex: code };
  }
  try {
    var result = await orderModel
      .find(condition)
      .limit(limit ? limit : 0)
      .skip((page - 1) * limit)
      .populate("orderDetails");
    // Sử dụng Promise.all để đợi tất cả các truy vấn hoàn thành
    const dataArr = await Promise.all(
      result.map(async (element) => {
        const user = await userModel.findOne({ orders: element._id }).populate("orders");
        const data = {
          name: user.name,
          phone: user.phone,
          orders: element
        }
        return data;
      })
    );
    return res.status(200).json({
      result: dataArr
    });

  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy danh sách order của customer theo id
const getAllOrderOfUser = async (req, res) => {
  // B1 Thu thập dữ liệu
  userId = req.params.userId;

  // B2 Kiểm tra dữ liệu
  // B3 Xử lý API
  try {
    var result = await userModel.findById(userId).populate("orders");
    // Sử dụng Promise.all để đợi tất cả các truy vấn hoàn thành
    const getDetail = await Promise.all(
      result.orders.map(async (element) => {
        const orderItems = await Promise.all(
          element.orderDetails.map(async (e, index) => {
            const details = await orderDetailModel.findById(e._id).populate("product")
            return {
              quantity: details.quantity,
              name: details.product.name
            };
          }
        ));
        const order = {
          code: element.orderCode,
          cost: element.cost,
          status: element.status,
          details: orderItems,
        }
        return order
      })
    );
    const data = {
      _id: result._id,
      email: result.email,
      name: result.name,
      phone: result.phone,
      avatar:result.avatar?result.avatar:null,
      orders: getDetail,
    }

    return res.status(200).json({
      result: data
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy order bằng id
const getOrderById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const orderId = req.params.orderId;
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined",
    });
  }
  // B3 Xử lý API
  try {
    var result = await orderModel.findById(orderId).populate("orderDetails");
    // Sử dụng Promise.all để đợi tất cả các truy vấn hoàn thành
    const dataArr = await Promise.all(
      result.orderDetails.map(async (element) => {
        const details = await orderDetailModel.findById(element._id).populate("product")

        const data = {
          // quantity: details.quantity,
          // product: details.product,
          name: details.product.name,
          buyPrice: details.product.buyPrice,
          _id: details.product._id,
          quantity: details.quantity,
          amount: details.product.amount,
        }
        return data;
      })
    );
    const form = {
      shippedDate: result.shippedDate,
      note: result.note,
      address: result.address,
      status: result.status,
      city: result.city,
      _id: result._id
    }
    return res.status(200).json({
      result: { form, details: dataArr },
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm update order bằng id
const updateOrderById = async (req, res) => {
  try {
    // B1 Thu thập dữ liệu
    const orderId = req.params.orderId;
    const { shippedDate, note, details, cost, address, city, status } = req.body;
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      return res.status(400).json({
        status: "Bad request",
        message: "Id undefined",
      });
    }
    // B3 Xử lý API
    const updateOrder = {
      cost,
      orderDetails: []
    };
    shippedDate !== "" ? updateOrder.shippedDate = shippedDate : []
    note !== "" ? updateOrder.note = note : []
    address !== "" ? updateOrder.address = address : []
    city !== "" ? updateOrder.city = city : []
    status !== "" ? updateOrder.status = status : []
    // Sử dụng Promise.all để đợi tất cả các truy vấn hoàn thành
    var order = await orderModel.findById(orderId);
    const oldDetails = order.orderDetails
    const clearDetails = await orderModel.findOneAndUpdate({ _id: orderId },
      {
        $set: updateOrder
      })
    const createDetails = await Promise.all(
      details.map(async (element) => {
        const item = {
          product: element._id,
          quantity: element.quantity
        }
        const detailsItems = await orderDetailModel.create(item)
        const result = await orderModel.findByIdAndUpdate(order._id, { $push: { orderDetails: detailsItems._id } })
        return result;
      })
    );
    const deleteOldDetails = await orderDetailModel.deleteMany({ _id: { $in: oldDetails } })
    //   var result = await orderModel.findByIdAndUpdate(orderId, updateOrder);
    return res.status(200).json({
      message: "update successfully",
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm xóa order bằng id
const deleteOrderById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const orderId = req.params.orderId;
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined",
    });
  }
  // B3 Xử lý API
  try {
    var order = await orderModel.findById(orderId);
    const oldDetails = order.orderDetails
    const deleteOldDetails = await orderDetailModel.deleteMany({ _id: { $in: oldDetails } })
    var result = await orderModel.findByIdAndDelete(orderId);
    return res.status(200).json({
      message: "delete successfully",
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};

// Exports
module.exports = {
  createOrderOfUser,
  getAllOrder,
  getAllOrderOfCustomer: getAllOrderOfUser,
  getOrderById,
  updateOrderById,
  deleteOrderById,
};
