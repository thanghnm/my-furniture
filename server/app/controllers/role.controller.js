const roleModel = require("../models/role.model");

const createRole = async (req, res) => {
  try {
    const role = {
      name: req.body.name,
    };
    const data = await roleModel.create(role);
    return res.status(201).json({
      data,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
const getAllRole = async (req, res) => {
  try {
    const data = await roleModel.find();
    return res.status(200).json({
      data,
    });
  } catch (error) {
    return res.status(500).json({
      message: "Internal server error",
    });
  }
};
module.exports = { createRole,getAllRole };
