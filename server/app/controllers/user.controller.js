//Import router
const userModel = require("../models/user.model");
const roleModel = require("../models/role.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var path = require("path");

const refreshTokenService = require("../services/refreshToken.service");
// Import Thư viện Mongoose
const mongoose = require("mongoose");
const { log } = require("console");
// Hàm tạo user
const createUser = async (req, res) => {
  // B1 Thu thập dữ liệu
  const { name, password, phone, email } = req.body;
  const roleName = req.query.roleName === "admin" ? "admin" : "user";
  const role = await roleModel.findOne({
    name: roleName,
  });
  // B2 Kiểm tra dữ liệu
  if (!name) {
    return res.status(400).json({
      status: "Bad request",
      message: "name is required",
    });
  }
  if (!password) {
    return res.status(400).json({
      status: "Bad request",
      message: "password is required",
    });
  }
  if (!phone) {
    return res.status(400).json({
      status: "Bad request",
      message: "phone is required",
    });
  }
  if (!email) {
    return res.status(400).json({
      status: "Bad request",
      message: "Email is required",
    });
  }
  // find exist user by phone
  const existUser = await userModel.findOne({
    phone,
  });
  // phone used
  if (existUser) {
    return res.status(400).json({
      message: "phone was used",
    });
  }
  // B3 Xử lý API
  const newUser = {
    _id: new mongoose.Types.ObjectId(),
    name,
    password: bcrypt.hashSync(password, 8),
    phone,
    email,
    role: role._id,
  };
  try {
    var result = await userModel.create(newUser);
    return res.status(201).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy danh sách user
const getAllUser = async (req, res) => {
  // B1 Thu thập dữ liệu
  // B1 Thu thập dữ liệu
  const username = req.query.username;
  const phone = req.query.phone;
  const email = req.query.email;
  const condition = {};
  // B2 Kiểm tra dữ liệu
  if (phone) {
    condition.phone = phone;
  }
  if (username) {
    condition.username = username;
  }
  if (email) {
    condition.email = email;
  }
  // B3 Xử lý API
  try {
    var result = await userModel.find(condition).populate("role");
    return res.status(200).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy user bằng id
const getUserById = async (req, res) => {
  // B1 Thu thập dữ liệu
  const userId = req.params.userId;
  // B2 Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      status: "Bad request",
      message: "Id undefined",
    });
  }
  // B3 Xử lý API
  try {
    var result = await userModel.findById(userId).populate("role");
    return res.status(200).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm edit user bằng id
const updateUserById = async (req, res) => {
  // B1 Thu thập dữ liệu
  try {
    const roleName = req.body.role === "admin" ? "admin" : "user";
    const userId = req.params.userId;
    const { name, email,phone } = req.body
    const password = req.body.password ? req.body.password : null
    const newPassword = req.body.newPassword ? req.body.newPassword : null

    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        status: "Bad request",
        message: "Id undefined",
      });
    }
    // B3 Xử lý API
    const roleQuery = await roleModel.findOne({
      name: roleName,
    });
    const OldUser = await userModel.findById(userId)
    const newUser = {
      name: name,
      email: email,
      role: roleQuery._id,
    }
    
    if(req.file){
      newUser.avatar= path.join(
        __dirname + "../../../uploads/avatar/avatar-" + phone,
      )
    }
    // compare password
    if (password, newPassword) {
      var passwordIsValid = bcrypt.compareSync(password, OldUser.password);
      // wrong password
      if (!passwordIsValid) {
        console.log("x");
        
        return res.status(400).json({
          message: "Password Invalid"
        });
      }
      else {
        newUser.password = bcrypt.hashSync(newPassword, 8)
      }
    }

    var result = await userModel.findByIdAndUpdate(userId, newUser, { new: true });
    return res.status(200).json({
      message:"Update success"
    });
  } catch (error) {
    console.log(error);

    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Hàm lấy  user bang order id
const getUserByOrderId = async (req, res) => {
  // B1 Thu thập dữ liệu
  // B1 Thu thập dữ liệu
  const orderId = req.params.orderId;
  const condition = {};
  // B2 Kiểm tra dữ liệu
  if (orderId) {
    condition.orders = orderId;
  }
  // B3 Xử lý API
  try {
    var result = await userModel.findOne({ orders: orderId });
    return res.status(200).json({
      result,
    });
  } catch (error) {
    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
// Log in controller
const logIn = async (req, res) => {
  const { phone, password } = req.body;
  try {
    // Check if username and password is provided
    if (!phone || !password) {
      return res.status(400).json({
        message: "Phone or Password not present",
      });
    }
    // find exist user by phone
    const existUser = await userModel.findOne({
      phone: phone,
    });
    // user not found
    if (!existUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    // user existed
    // compare password
    var passwordIsValid = bcrypt.compareSync(password, existUser.password);
    // wrong password
    if (!passwordIsValid) {
      return res.status(400).json({
        message: "Password Invalid",
      });
    }
    const secretKey = "YouCanNotHackMyPassword";
    const maxAge = 3 * 60 * 60;
    const token = await jwt.sign(
      {
        id: existUser._id,
      },
      secretKey,
      {
        algorithm: "HS256",
        allowInsecureKeySizes: true,
        expiresIn: maxAge,
      },
    );
    //Sinh thêm refresh token
    const refreshToken = await refreshTokenService.createToken(existUser);
    return res.status(200).json({
      accessToken: token,
      refreshToken: refreshToken,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal servel error",
      error,
    });
  }
};
// Hàm edit user bằng id
const updateAvatar = async (req, res) => {
  // B1 Thu thập dữ liệu
  try {
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        status: "Bad request",
        message: "Id undefined",
      });
    }

    const newUser = {
      avatar: path.join(
        __dirname + "../../../uploads/avatar/" + req.file.filename,
      ),
    }
    var result = await userModel.findByIdAndUpdate(userId, newUser, { new: true });
    return res.status(200);
  } catch (error) {
    console.log(error);

    return res.status(500).json({
      status: "Internal Server Error",
      message: error.message,
    });
  }
};
module.exports = { createUser, getAllUser, logIn, getUserById, getUserByOrderId, updateUserById,updateAvatar };
