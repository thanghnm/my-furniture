# Getting Started

### `npm install` to download package

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

# Admin

you need account have admin role to open Admin page

open (http://localhost:3000/admin/) + adminPath 

- adminPath:
     - users
     - orders,
     - products