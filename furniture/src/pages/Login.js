// prettier-ignore
/* eslint-disable */
import { Visibility, VisibilityOff } from '@mui/icons-material';
import PersonIcon from "@mui/icons-material/Person"
import {
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
} from "@mui/material"
import VpnKeyIcon from "@mui/icons-material/VpnKey"
import FacebookIcon from "@mui/icons-material/Facebook"
import XIcon from "@mui/icons-material/X"
import GoogleIcon from "@mui/icons-material/Google"
import InstagramIcon from "@mui/icons-material/Instagram"
import EmailIcon from "@mui/icons-material/Email"
import React from "react"
import PhoneIcon from "@mui/icons-material/Phone"
import { useDispatch, useSelector } from "react-redux"
import { changePhoneInput } from "actions/login.action"
import { changePasswordInput } from "actions/login.action"
import { SignIn } from "actions/login.action"
import loading from "../assets/images/loading.gif"
import { changePhoneInputSignUp } from "actions/login.action"
import { changeNameInputSignUp } from "actions/login.action"
import { changeEmailInputSignUp } from "actions/login.action"
import { changePasswordInputSignUp } from "actions/login.action"
import { changeConfirmPasswordInputSignUp } from "actions/login.action"
import { signUp } from "actions/login.action"
import { ModalSuccess } from "components/modal/successModal"

const Login = () => {
  const dispatch = useDispatch()
  const { signInForm, displayError, pending, signUpForm, signUpSuccess, errorMessage } =
    useSelector((reduxData) => reduxData.loginReducer)
  const [showPassword, setShowPassword] = React.useState(false)

  const handleClickShowPassword = () => setShowPassword((show) => !show)

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }
  const onInputPhoneSignInChangeHandler = (event) => {
    dispatch(changePhoneInput(event))
  }
  const onInputPasswordSignInChangeHandler = (event) => {
    dispatch(changePasswordInput(event))
  }
  const onInputPhoneSignUpChangeHandler = (event) => {
    dispatch(changePhoneInputSignUp(event))
  }
  const onInputNameSignUpChangeHandler = (event) => {
    dispatch(changeNameInputSignUp(event))
  }
  const onInputEmailSignUpChangeHandler = (event) => {
    dispatch(changeEmailInputSignUp(event))
  }
  const onInputPasswordSignUpChangeHandler = (event) => {
    dispatch(changePasswordInputSignUp(event))
  }
  const onInputConfirmPasswordSignUpChangeHandler = (event) => {
    dispatch(changeConfirmPasswordInputSignUp(event))
  }
  const onBtnSignInClickHandler = (event) => {
    event.preventDefault()
    dispatch(SignIn(signInForm))
  }
  const onBtnSignUpClickHandler = (event) => {
    event.preventDefault()
    dispatch(signUp(signUpForm))
  }
  const onBtnCloseModalSignUpSuccess = () => {
    dispatch(SignIn(signUpForm))
  }
  const signUpAction = () => {
    const container = document.getElementById("login-container")
    container.classList.add("sign-up-mode")
  }
  const signInAction = () => {
    const container = document.getElementById("login-container")
    container.classList.remove("sign-up-mode")
  }
  return (
    <>
      <div id="login-container" className="container">
        <div className="forms-container">
          <div className="signin-signup">
            <form action="#" className="sign-in-form">
              <h2 className="title">Đăng nhập </h2>
              <div className="input-field">
                <PersonIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <TextField
                    placeholder="Số điện thoại"
                    variant="outlined"
                    color="secondary"
                    value={signInForm.phone}
                    onChange={onInputPhoneSignInChangeHandler}
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.phone ? "block" : "none" }}
              >
                {errorMessage}
              </p>
              <div className="input-field">
                <VpnKeyIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    placeholder="Mật khẩu"
                    value={signInForm.password}
                    onChange={onInputPasswordSignInChangeHandler}
                    id="outlined-adornment-password"
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.password ? "block" : "none" }}
              >
                {errorMessage}
              </p>
              <Button
                type="submit"
                variant="contained"
                onClick={onBtnSignInClickHandler}
                className="btn submit-login"
              >
                Login{" "}
                {pending.signIn ? (
                  <img
                    style={{ width: 24, height: 24 }}
                    className="pl-2"
                    src={loading}
                    alt="mygif"
                  />
                ) : (
                  ""
                )}
              </Button>
            </form>
            <form action="#" className="sign-up-form">
              <h2 className="title">Đăng ký</h2>
              <div className="input-field">
                <PhoneIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    onChange={onInputPhoneSignUpChangeHandler}
                    placeholder="Số điện thoại"
                    variant="outlined"
                    value={signUpForm.phone}
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.phoneSU ? "block" : "none" }}
              >
                {errorMessage}
              </p>
              <div className="input-field">
                <PersonIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    onChange={onInputNameSignUpChangeHandler}
                    placeholder="Tên người dùng"
                    variant="outlined"
                    value={signUpForm.name}
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.nameSU ? "block" : "none" }}
              >
                {errorMessage}
              </p>
              <div className="input-field">
                <EmailIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    onChange={onInputEmailSignUpChangeHandler}
                    placeholder="Email"
                    variant="outlined"
                    value={signUpForm.email}
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.emailSU ? "block" : "none" }}
              >
                {errorMessage}
              </p>
              <div className="input-field">
                <VpnKeyIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    placeholder="Mật khẩu"
                    onChange={onInputPasswordSignUpChangeHandler}
                    id="outlined-signup-password"
                    value={signUpForm.password}
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{ display: displayError.passwordSU ? "block" : "none" }}
              >
                {errorMessage}
              </p>

              <div className="input-field">
                <VpnKeyIcon />
                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                  <OutlinedInput
                    placeholder="Xác nhận mật khẩu"
                    onChange={onInputConfirmPasswordSignUpChangeHandler}
                    value={signUpForm.confirmPassword}
                    id="outlined-signup-confirm-password"
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </div>
              <p
                className="invalid-textfield"
                style={{
                  display: displayError.confirmPasswordSU ? "block" : "none",
                }}
              >
                {errorMessage}
              </p>
              <Button
                type="submit"
                variant="contained"
                onClick={onBtnSignUpClickHandler}
                className="btn submit-login"
              >
                Đăng ký{" "}
                {pending.signUp ? (
                  <img
                    style={{ width: 24, height: 24 }}
                    className="pl-2"
                    src={loading}
                    alt="mygif"
                  />
                ) : (
                  ""
                )}
              </Button>
            </form>
          </div>
        </div>
        <div className="panels-container">
          <div className="panel left-panel">
            <div className="content">
              <h3>Chưa có tài khoản</h3>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis, ex ratione.
                Aliquid!
              </p>
              <button onClick={signUpAction} className="btn transparent" id="sign-up-btn">
                Đăng ký
              </button>
            </div>
            <img src="img/log.svg" className="image" alt="" />
          </div>
          <div className="panel right-panel">
            <div className="content">
              <h3>Đã có tài khoản</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum laboriosam ad
                deleniti.
              </p>
              <button onClick={signInAction} className="btn transparent" id="sign-in-btn">
                Đăng nhập
              </button>
            </div>
            <img src="img/register.svg" className="image" alt="" />
          </div>
        </div>
      </div>
      <ModalSuccess
        close={onBtnCloseModalSignUpSuccess}
        openModal={signUpSuccess}
        content="Đăng nhập và tiếp tục mua hàng"
        title="Đăng ký thành công"
      />
    </>
  )
}
export default Login
