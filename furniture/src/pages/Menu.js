import Banner from "components/banner/banner"
import MyBreadcrumb from "../components/breadcrumb/breadcrumb"
import Contact from "../components/contact/contact"
import Footer from "../components/footer/footer"
import Header from "../components/header/Header.components"
import MenuContent from "../components/menuContent/menuContent"

const Menu = () => {
  return (
    <div>
      <Header />
      <Banner content="Tất cả sản phẩm" />
      <MyBreadcrumb thisPage="Menu" />
      <MenuContent />
      <Footer />
      <Contact />
    </div>
  )
}
export default Menu
