/* eslint-disable */
import { Button } from "@mui/material"
import notFound from "../assets/images/not-found.jpg"
const NotFound =()=>{
    const onBtnClick =()=>{
        window.location.href="/"
    }
return(
    <div className="flex items-center justify-center flex-col bg-white h-screen">
        <h2 >Có vẻ bạn đang lạc mất rồi !!!</h2>
        <Button onClick={onBtnClick} variant="contained" className="btn-base mt-5">Trở lại trang chủ</Button>
        <img src={notFound} style={{maxWidth:"30%"}} />
    </div>
)
}
export default  NotFound 