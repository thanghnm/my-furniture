/* eslint-disable */
import Banner from "components/banner/banner"
import MyBreadcrumb from "components/breadcrumb/breadcrumb"
import Contact from "components/contact/contact"
import Footer from "components/footer/footer"
import Header from "components/header/Header.components"
import Detail from "components/productDetail/detail"

const ProductInfo = () => {
  return (
    <div>
      <Header />
      <Banner content="Chi tiết sản phẩm" />
      <MyBreadcrumb hrefParent="/menu" parentThisPage="Menu" thisPage="Chi tiết sản phẩm" />
      <Detail />
      <Footer />
      <Contact />
    </div>
  )
}
export default ProductInfo
