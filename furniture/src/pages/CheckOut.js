/* eslint-disable */

import Banner from "components/banner/banner"
import MyBreadcrumb from "components/breadcrumb/breadcrumb"
import Contact from "components/contact/contact"
import Footer from "components/footer/footer"
import Header from "components/header/Header.components"

const CheckOut = () => {
  return (
    <div>
      <Header />
      <Banner content="Thanh toán" />
      <MyBreadcrumb thisPage="Checkout" />
      <Footer />
      <Contact />
    </div>
  )
}
export default CheckOut
