/* eslint-disable */
import { Avatar, Button, FormControl, IconButton, InputAdornment, OutlinedInput, TextField } from "@mui/material"
import MyBreadcrumb from "components/breadcrumb/breadcrumb"
import Footer from "components/footer/footer"
import Header from "components/header/Header.components"
import EditIcon from "@mui/icons-material/Edit"
import { useCallback, useEffect, useRef, useState } from "react"
import PhoneIcon from "@mui/icons-material/Phone"
import EmailIcon from "@mui/icons-material/Email"
import PersonIcon from "@mui/icons-material/Person"
import { useDispatch, useSelector } from "react-redux"
import { editFormAction } from "actions/profile.action"
import { imageFileChange } from "actions/profile.action"
import { PROFILE_FETCH_SUCCESS } from "constants/profile.constants"
import { PROFILE_FETCH_ERROR } from "constants/profile.constants"
import { Visibility, VisibilityOff } from '@mui/icons-material';
import VpnKeyIcon from "@mui/icons-material/VpnKey"
import { nameChange } from "actions/profile.action"
import { emailChange } from "actions/profile.action"
import { passwordChange } from "actions/profile.action"
import { newPasswordChange } from "actions/profile.action"
import { updateProfile } from "actions/profile.action"
import { ModalSuccess } from "components/modal/successModal"
import { closeModal } from "actions/profile.action"
import homeicon from '../assets/images/icons/homeicon.png';
const Profile = () => {
  const dispatch = useDispatch()
  const { visible, user, success, displayError, errorMessage, avatarUrl, order } = useSelector((reduxData) => reduxData.profileReducer)
  const [showPassword, setShowPassword] = useState(false)

  useEffect(() => {
    const vUser = sessionStorage.getItem("user")
    const id = JSON.parse(vUser).id
    const fetchData = async (id) => {
      try {
        var requestOptions = {
          method: "GET",
          redirect: "follow",
        }
        const responseUser = await fetch(
          process.env.REACT_APP_API_HOST + "/orders/user/" + id,
          requestOptions
        )
        const dataUser = await responseUser.json()
        return dispatch({
          type: PROFILE_FETCH_SUCCESS,
          payload: dataUser.result
        })
      } catch (error) {
        console.log(error)
        return dispatch({
          type: PROFILE_FETCH_ERROR,
          error: error,
        })
      }
    }
    fetchData(id)
  }, [])
  const handleClickShowPassword = () => setShowPassword((show) => !show)
  const onBtnEditClick = () => {
    dispatch(editFormAction())
  }
  const onBtnUpdateClick = () => {
    dispatch(updateProfile(user))
  };
  const fileInputRef = useRef(null);

  const handleImageClick = () => {
    fileInputRef.current.click();
  };
  const handleFileChange = (event) => {
    dispatch(imageFileChange(event.target.files[0]))
  }
  const onTextFieldNameChange = useCallback((event) => {
    dispatch(nameChange(event.target.value))
  }, [])
  const onTextFieldEmailChange = useCallback((event) => {
    dispatch(emailChange(event.target.value))
  }, [])
  const onTextFieldPasswordChange = useCallback((event) => {
    dispatch(passwordChange(event.target.value))
  }, [])
  const onTextFieldNewPasswordChange = useCallback((event) => {
    dispatch(newPasswordChange(event.target.value))
  }, [])
  const closeModalHandler = () => {
    dispatch(closeModal())
  }
  return (
    <div>
      <Header />
      <MyBreadcrumb hrefParent="/menu" parentThisPage="Menu" thisPage="Profile" />
      <div className="profile-container">
        <div className="flex items-center relative data-div">
          <img id="avatar" onClick={handleImageClick} src={avatarUrl?avatarUrl:homeicon} />
          <input
            name="avatar"
            onChange={handleFileChange}
            type="file"
            ref={fileInputRef}
            style={{ display: 'none' }}
          />
          <div className="user-info ml-5">
            <p><span>Người dùng:</span> {user.name}</p>
            <p><span>Email:</span> {user.email}</p>
            <p><span>Số điện thoại:</span> {user.phone}</p>
          </div>
          <Button id="btn-edit-form" className="btn-base" onClick={onBtnEditClick} style={{ right: "10%", position: "absolute" }} variant="contained">
            <EditIcon />
          </Button>
        </div>
        <div id="edit-form" className={visible ? "form-visible" : "form-hidden"}>
          <div className=" flex items-center ">
            <PhoneIcon />
            <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
              <OutlinedInput
                disabled
                placeholder="Số điện thoại"
                variant="outlined"
                value={user.phone}
              />
            </FormControl>
          </div>
          <div className=" flex items-center ">
            <PersonIcon />
            <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
              <OutlinedInput
                onChange={onTextFieldNameChange}
                placeholder="Tên người dùng"
                variant="outlined"
                value={user.name}
              />
            </FormControl>
          </div>
          <div className=" flex items-center ">
            <EmailIcon />
            <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
              <OutlinedInput
                onChange={onTextFieldEmailChange}
                placeholder="Email"
                variant="outlined"
                value={user.email}
              />
            </FormControl>
          </div>
          <div className=" flex items-center ">
            <VpnKeyIcon />
            <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
              <OutlinedInput
                placeholder="Mật khẩu cũ"
                onChange={onTextFieldPasswordChange}
                value={user.password}
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
          <p
            className="invalid-textfield"
            style={{ display: displayError ? "block" : "none" }}
          >
            {errorMessage}
          </p>
          <div className=" flex items-center ">
            <VpnKeyIcon />
            <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
              <OutlinedInput
                placeholder="Mật khẩu mới"
                onChange={onTextFieldNewPasswordChange}
                value={user.newPassword}
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
          <Button onClick={onBtnUpdateClick} className="btn-base" variant="contained">
            Cập nhật
          </Button>
        </div>
        <div className="profile-tracking">
          <div className="grid grid-cols-4 justify-center items-center text-center tracking-header">
            <h6>Mã đơn</h6>
            <h6>Các sản phẩm</h6>
            <h6>Thành tiền</h6>
            <h6>Trạng thái</h6>
          </div>
          {order && order.length > 0 ?
            order.map((e, index) => {
              return (
              <div key={index} className="grid grid-cols-4 justify-center items-center text-center tracking-row">
                <div className="tracking-cell">{e.code}</div>
                <div className="tracking-cell">
                  {e.details.map((element,i) => {
                    return (
                      <div key={i} className="flex justify-between">
                        <p>{element.name}</p>
                        <p>x{element.quantity}</p>
                      </div>
                    )
                  })}
                </div>
                <div className="tracking-cell">{e.cost.toLocaleString()} vnđ</div>
                <div className="tracking-cell">{e.status}</div>
              </div>)
            })
            : []}
        </div>
      </div>
      <Footer />
      <ModalSuccess title="Cập nhật thông tin thành công" close={closeModalHandler} openModal={success} content="Đóng thông báo" />
    </div>
  )
}
export default Profile
