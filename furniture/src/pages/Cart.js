/* eslint-disable */

import Banner from "components/banner/banner"
import MyBreadcrumb from "components/breadcrumb/breadcrumb"
import CartContent from "components/cart/CartContent"
import Contact from "components/contact/contact"
import Footer from "components/footer/footer"
import Header from "components/header/Header.components"

const Cart = () => {
  return (
    <div>
      <Header />
      <Banner content="Giỏ hàng" />
      <MyBreadcrumb thisPage="Menu" />
      <CartContent />
      <Footer />
      <Contact />
    </div>
  )
}
export default Cart
