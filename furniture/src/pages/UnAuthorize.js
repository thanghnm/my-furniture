/* eslint-disable */
import { Button } from "@mui/material"
import unauthorize from "../assets/images/unauthorize.jpg"
const UnAuthorize =()=>{
    const onBtnClick =()=>{
        window.location.href="/"
    }
return(
    <div className="flex items-center justify-center flex-col bg-white h-screen">
        <h2 className="mt-5">Bạn không được cấp quyền để vào trang này !!!</h2>
        <img src={unauthorize} style={{maxWidth:"50%"}} />
        <Button onClick={onBtnClick} variant="contained" className="btn-base">Quay lại trang chủ</Button>
    </div>
)
}
export default  UnAuthorize 