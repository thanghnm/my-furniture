import "./index.css"
import React from "react"
import ReactDOM from "react-dom/client"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import { BrowserRouter } from "react-router-dom"
import { Provider } from "react-redux"
import rootReducer from "./reducers/rootReducer"
import { applyMiddleware, createStore } from "redux"
// Material Dashboard 2 React Context Provider
import { MaterialUIControllerProvider } from "context"
import { thunk } from "redux-thunk"
const store = createStore(rootReducer, applyMiddleware(thunk))
const container = document.getElementById("app")
const root = ReactDOM.createRoot(container)
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <MaterialUIControllerProvider>
          <App />
        </MaterialUIControllerProvider>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
