/* eslint-disable */

/**
=========================================================
* Material Dashboard 2 React - v2.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid"
import Card from "@mui/material/Card"

// Material Dashboard 2 React components
import MDBox from "components/MDBox"
import MDTypography from "components/MDTypography"

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout"
import DashboardNavbar from "examples/Navbars/DashboardNavbar"
import Footer from "examples/Footer"
import DataTable from "examples/Tables/DataTable"
// Data
import { Backdrop, CircularProgress, IconButton, Pagination } from "@mui/material"
import AddCircleIcon from "@mui/icons-material/AddCircle"
import AddProductModal from "components/modal/table-product/addProductModal"
import { useDispatch, useSelector } from "react-redux"
import { openAddProductModal } from "actions/admin/product.action"
import { closeProductModal } from "actions/admin/product.action"
import { ModalSuccess } from "components/modal/successModal"
import React, { useCallback, useEffect, useState } from "react"
import { fetchProduct } from "actions/admin/product.action"
import { ADMIN_PRODUCT_FETCH_ERROR } from "constants/admin.constants"
import EditIcon from "@mui/icons-material/Edit"
import DeleteIcon from "@mui/icons-material/Delete"
import EditProductModal from "components/modal/table-product/editProductModal"
import { pageChangePagination } from "actions/admin/product.action"
import { openEditModalAction } from "actions/admin/product.action"
import { openDeleteModalAction } from "actions/admin/product.action"
import DeleteProductModal from "components/modal/table-product/deleteProductModal"
import AdminFilter from "components/filter/product.filter"
import { ADMIN_PRODUCT_INIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_FETCH_PENDIND } from "constants/admin.constants"

function ProductTable() {
  const dispatch = useDispatch()
  const { noPage, page, productData, openAddModal, addProductForm, displayError,
    errorMessage, productType, openSuccessModal, openEditModal,title,
    editProductForm, pending, disable, openDeleteModal, deleteName, deleteProductId,filter
  } = useSelector((reduxData) => reduxData.productReducer)

  var rowsData = []
  var columns = [
    { Header: "Tên sản phẩm", accessor: "name", align: "left" },
    { Header: "Hình ảnh", accessor: "image", align: "left" },
    { Header: "Giá tiền", accessor: "buyPrice", align: "left" },
    { Header: "Giá khuyến mãi", accessor: "promotionPrice", align: "center" },
    { Header: "Số lượng", accessor: "amount", align: "center" },
    { Header: "action", accessor: "action", align: "center" },
  ]
  var rows = rowsData
  useEffect(() => {
    const fetchData = async (limit, page) => {
      try {
        var requestOptions = {
          method: "GET",
          redirect: "follow",
        }
        // await dispatch({
        //   type: ADMIN_PRODUCT_FETCH_PENDIND,
        // })
        const paramSearch = new URLSearchParams({
          limit: limit,
          page: page,
        })

        const responseTotal = await fetch(
          process.env.REACT_APP_API_HOST + "/products",
          requestOptions
        )
        const dataTotal = await responseTotal.json()
        const responseType = await fetch(
          process.env.REACT_APP_API_HOST + "/types",
          requestOptions
        )
        const dataType = await responseType.json()
        const response = await fetch(
          process.env.REACT_APP_API_HOST + "/products?" + paramSearch.toString(),
          requestOptions
        )

        const data = await response.json()
        return dispatch({
          type: ADMIN_PRODUCT_INIT_SUCCESS,
          payload: {
            total: dataTotal.result.length,
            data: data.result,
            type: dataType.result
          },
        })
      } catch (error) {
        return dispatch({
          type: ADMIN_PRODUCT_FETCH_ERROR,
          error: error,
        })
      }
    }
    if (productData.length==0){
      fetchData(10, page)
    }
  }, [dispatch])

  const onBtnEditClick = (id) => {
    dispatch(openEditModalAction(id))
  }
  const onBtnDeleteClick = (name, id) => {
    dispatch(openDeleteModalAction(name, id))
  }
  if (productData.length > 0) {
    productData.map((element, index) => {
      rowsData.push({
        name: <p style={{ overflow: "clip" }}>{element.name}</p>,
        image: <img className="" src={`${process.env.REACT_APP_API_UPLOAD}image-${element.name}`}></img>,
        buyPrice: <p>{element.price.toLocaleString()}đ</p>,
        promotionPrice: <p>{element.buyPrice.toLocaleString()}đ</p>,
        amount: <p>{element.amount}</p>,
        action: (
          < >
            <IconButton onClick={() => onBtnEditClick(element._id)} color="info" aria-label="edit">
              <EditIcon />
            </IconButton>
            <IconButton onClick={() => onBtnDeleteClick(element.name, element._id)} color="error" aria-label="edit">
              <DeleteIcon id={element._id} />
            </IconButton>
          </>
        ),
      })
    })
  }

  const onBtnAddProductClick = useCallback(() => {
    dispatch(openAddProductModal())
  }, [dispatch])

  const handleCloseModal = useCallback(() => {
    dispatch(closeProductModal())
  }, [dispatch])
  const handleCloseModalSuccess = () => {
    dispatch(closeProductModal())
    dispatch(fetchProduct(10, page,filter))
  }
  const onChangePagination = (event, value) => {
    dispatch(pageChangePagination(value))
    dispatch(fetchProduct(10, value,filter))
  }
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
                className="table-title"
              >
                <MDTypography variant="h6" color="white">
                  Sản phẩm
                </MDTypography>
                <IconButton onClick={onBtnAddProductClick} style={{ color: "white" }}>
                  <AddCircleIcon />
                </IconButton>
                
              </MDBox>
              <MDBox pt={3}>
              <AdminFilter name={filter.name} amount={filter.amount}/>
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>

            </Card>
            <div className="flex justify-center">
            <Pagination
              className="mt-5"
              onChange={onChangePagination}
              count={noPage}
              value={page}
              variant="outlined"
              shape="rounded"
            />
            </div>
           
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
      <AddProductModal
        open={openAddModal} addProductForm={addProductForm}
        name={addProductForm.name} nameError={displayError.name}
        type={addProductForm.type} typeError={displayError.type}
        buyPrice={addProductForm.buyPrice} buyPriceError={displayError.buyPrice}
        price={addProductForm.price} priceError={displayError.price}
        amount={addProductForm.amount} amountError={displayError.amount}
        description={addProductForm.description} descriptionError={displayError.description}
        imageUrl={addProductForm.imageUrl}
        errorMessage={errorMessage} imageUrlError={displayError.imageUrl}
        productType={productType} close={handleCloseModal}
      />
      <ModalSuccess
        close={handleCloseModalSuccess}
        openModal={openSuccessModal}
        content="Đóng thông báo"
        title={title}
      />
      <EditProductModal
        open={openEditModal} editProductForm={editProductForm} id={editProductForm._id}
        name={editProductForm.name} nameError={displayError.name}
        type={editProductForm.type} typeError={displayError.type}
        buyPrice={editProductForm.buyPrice} buyPriceError={displayError.buyPrice}
        price={editProductForm.price} priceError={editProductForm.price}
        amount={editProductForm.amount} amountError={displayError.amount}
        description={editProductForm.description} descriptionError={displayError.description}
        imageUrl={editProductForm.imageUrl} errorMessage={errorMessage}
        productType={productType} close={handleCloseModal}
        disabled={disable}
      />
      <DeleteProductModal open={openDeleteModal} close={handleCloseModal} name={deleteName} id={deleteProductId} />
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={pending}
        onClick={handleCloseModal}
      >
        <CircularProgress color="warning" />
      </Backdrop>
    </DashboardLayout>
  )
}
export default ProductTable
