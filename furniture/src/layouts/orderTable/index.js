/* eslint-disable */

/**
=========================================================
* Material Dashboard 2 React - v2.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2023 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid"
import Card from "@mui/material/Card"

// Material Dashboard 2 React components
import MDBox from "components/MDBox"
import MDTypography from "components/MDTypography"

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout"
import DashboardNavbar from "examples/Navbars/DashboardNavbar"
import Footer from "examples/Footer"
import DataTable from "examples/Tables/DataTable"
// Data
import { Backdrop, CircularProgress, IconButton, Pagination } from "@mui/material"
import AddCircleIcon from "@mui/icons-material/AddCircle"
import { useDispatch, useSelector } from "react-redux"
import { ModalSuccess } from "components/modal/successModal"
import React, { useCallback, useEffect, useState } from "react"
import { fetchProduct } from "actions/admin/product.action"
import EditIcon from "@mui/icons-material/Edit"
import DeleteIcon from "@mui/icons-material/Delete"
import { pageChangePagination } from "actions/admin/product.action"
import { openEditModalAction } from "actions/admin/product.action"
import { ADMIN_ORDER_INIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_ERROR } from "constants/admin.constants"
import CreateOrderModal from "components/modal/table-order/createOrderModal"
import { openCreateOrderModal } from "actions/admin/order.action"
import CreateOrderModalAdmin from "components/modal/table-order/createOrderModal.admin"
import { closeModalHandler } from "actions/admin/order.action"
import { fetchOrder } from "actions/admin/order.action"
import { closeModalOrderSuccess } from "actions/admin/order.action"
import EditOrderModal from "components/modal/table-order/editOrderModal"
import { openEditOrderModal } from "actions/admin/order.action"
import DeleteOrderModal from "components/modal/table-order/deleteOrderModal"
import { openDeleteModalAction } from "actions/admin/order.action"
import OrderFilter from "components/filter/order.filter"

function OrderTable() {
  const dispatch = useDispatch()
  const {
    orderData,noPage,page,createOrderForm,displayError,errorMessage,pending,
    productData,openAddModal,selectedItems, step, createOrderSuccess,title,
    filter,openEditModal,editOrderForm,editOrderDetails,openDeleteModal,deleteId,orderCode,
  } =useSelector((reduxData)=>reduxData.orderReducer)

  useEffect(() => {
    const fetchData = async (limit, page) => {
      try {
        var requestOptions = {
          method: "GET",
          redirect: "follow",
        }
        // await dispatch({
        //   type: ADMIN_PRODUCT_FETCH_PENDIND,
        // })
        const paramSearch = new URLSearchParams({
          limit: limit,
          page: page,
        })
        const responseTotal = await fetch(
          process.env.REACT_APP_API_HOST + "/orders",
          requestOptions
        )
        const dataTotal = await responseTotal.json()
        const responseType = await fetch(
          process.env.REACT_APP_API_HOST + "/products",
          requestOptions
        )
        const dataProduct = await responseType.json()
        const responseOrder = await fetch(
          process.env.REACT_APP_API_HOST + "/orders?" + paramSearch.toString(),
          requestOptions
        )
        const dataOrder = await responseOrder.json()
         return  dispatch({
          type: ADMIN_ORDER_INIT_SUCCESS,
          payload: {
            total: dataTotal.result.length,
            data: dataOrder.result,
            product: dataProduct.result
          },
        })
      } catch (error) {
        console.log(error)
        return dispatch({
          type: ADMIN_ORDER_FETCH_ERROR,
          error: error,
        })
      }
    }
    fetchData(10, 1)
  }, [dispatch])
  var rowsData = []
  var columns = [
    { Header: "Mã đơn hàng", accessor: "orderCode", align: "left" },
    { Header: "Nguời nhận", accessor: "user", align: "left" },
    { Header: "SĐT", accessor: "phone", align: "left" },
    { Header: "Thời gian giao hàng", accessor: "shippedDate", align: "left" },
    { Header: "Thành tiền", accessor: "cost", align: "left" },
    { Header: "Địa chỉ", accessor: "address", align: "center" },
    { Header: "Trạng thái", accessor: "status", align: "center" },
    { Header: "action", accessor: "action", align: "center" },
  ]
  var rows = rowsData
  if (orderData.length > 0) {
    orderData.map(async(element, index) => {
      const date = new Date(element.orders.shippedDate);
      rowsData.push({
        orderCode: <p style={{ overflow: "clip" }}>{element.orders.orderCode}</p>,
        user:<p>{element.name}</p>,
        phone:<p>{element.phone}</p>,
        shippedDate:<p>{date.toLocaleDateString()}</p>,
        cost: <p>{element.orders.cost.toLocaleString()}đ</p>,
        address: <p>{element.orders.address}</p>,
        status: <p>{element.orders.status}</p>,
        action: (
          < >
            <IconButton onClick={() => onBtnEditClick(element.orders._id)} color="info" aria-label="edit">
              <EditIcon />
            </IconButton>
            <IconButton onClick={() => onBtnDeleteClick(element.orders.orderCode, element.orders._id)} color="error" aria-label="edit">
              <DeleteIcon id={element._id} />
            </IconButton>
          </>
        ),
      })
    })
  }

  const onBtnEditClick = (id) => {
    dispatch(openEditOrderModal(id))
  }
  const onBtnDeleteClick = (code, id) => {
    dispatch(openDeleteModalAction(code, id))
  }
  const onBtnAddProductClick = useCallback(() => {
    dispatch(openCreateOrderModal())
  }, [dispatch])

  const handleCloseModal = useCallback(() => {
    dispatch(closeModalHandler())
  }, [dispatch])
  const handleCloseModalSuccess = () => {
    dispatch(closeModalHandler())
    dispatch(closeModalOrderSuccess())
    dispatch(fetchOrder(10, page,filter))
  }
  const onChangePagination = (event, value) => {
    dispatch(pageChangePagination(value))
    // dispatch(fetchProduct(10, value,filter))
  }
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
                className="table-title"
              >
                <MDTypography variant="h6" color="white">
                  Đơn hàng
                </MDTypography>
                <IconButton onClick={onBtnAddProductClick} style={{ color: "white" }}>
                  <AddCircleIcon />
                </IconButton>
                
              </MDBox>
              <MDBox pt={3}
                className="order-table"
              >
                <OrderFilter code={filter.code} status={filter.status} /> 
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>

            </Card>
            <div className="flex justify-center">
            <Pagination
              sx={{ justifyContent: "center" }}
              className="mt-5"
              onChange={onChangePagination}
              count={noPage}
              value={page}
              variant="outlined"
              shape="rounded"
            />
            </div>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
      <CreateOrderModalAdmin
        open={openAddModal} name={createOrderForm.name}
        email={createOrderForm.email} phone={createOrderForm.phone}
        address={createOrderForm.address} city={createOrderForm.city}
        note={createOrderForm.note} shippedDate={createOrderForm.shippedDate}
        displayError={displayError} errorMessage={errorMessage}
        menu={productData} selectedItems={selectedItems}
        step={step}
      />
       <ModalSuccess
        close={handleCloseModalSuccess}
        openModal={createOrderSuccess}
        content="Đóng thông báo"
        title={title}
      />
      <EditOrderModal
        displayError={displayError} errorMessage={errorMessage}
        open={openEditModal} address={editOrderForm.address} city={editOrderForm.city}
        note={editOrderForm.note} status={editOrderForm.status} shippedDate={editOrderForm.shippedDate}
        step={step}  menu={productData} details ={editOrderDetails} orderId = {editOrderForm._id}
      />

      <DeleteOrderModal open={openDeleteModal} close={handleCloseModal} name={orderCode} id={deleteId} />
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={pending}
        onClick={handleCloseModal}
      >
        <CircularProgress color="warning" />
      </Backdrop> 
    </DashboardLayout>
  )
}
export default OrderTable
