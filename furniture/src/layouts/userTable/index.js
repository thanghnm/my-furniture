/* eslint-disable */



// @mui material components
import Grid from "@mui/material/Grid"
import Card from "@mui/material/Card"

// Material Dashboard 2 React components
import MDBox from "components/MDBox"
import MDTypography from "components/MDTypography"

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout"
import DashboardNavbar from "examples/Navbars/DashboardNavbar"
import Footer from "examples/Footer"
import DataTable from "examples/Tables/DataTable"
// Data
import { Backdrop, CircularProgress, IconButton, Pagination } from "@mui/material"
import AddCircleIcon from "@mui/icons-material/AddCircle"
import { useDispatch, useSelector } from "react-redux"
import { ModalSuccess } from "components/modal/successModal"
import React, { useCallback, useEffect, useState } from "react"
import EditIcon from "@mui/icons-material/Edit"
import DeleteIcon from "@mui/icons-material/Delete"
import { pageChangePagination } from "actions/admin/user.action"
import { ADMIN_USER_INIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_INIT_PENDING } from "constants/admin.constants"
import { ADMIN_USER_INIT_ERROR } from "constants/admin.constants"
import CreateUserModal from "components/modal/table-user/createUserModal"
import { openAddModalAction } from "actions/admin/user.action"
import { fetchUser } from "actions/admin/user.action"
import { closeModal } from "actions/admin/user.action"
import { openEditUserModal } from "actions/admin/user.action"
import EditUserModal from "components/modal/table-user/editUserModal"

function UserTable() {
  const dispatch = useDispatch()
  const { limit, page, userData, addForm, openAddModal, displayError, errorMessage, success, title,noPage
    ,editForm,openEditModal,pending
  } = useSelector((reduxData) => reduxData.userReducer)
  useEffect(() => {
    const fetchData = async (limit, page) => {
      try {
        var requestOptions = {
          method: "GET",
          redirect: "follow",
        }
        // await dispatch({
        //   type: ADMIN_USER_INIT_PENDING,
        // })
        const paramSearch = new URLSearchParams({
          limit: limit,
          page: page,
        })
        const responseTotal = await fetch(
          process.env.REACT_APP_API_HOST + "/users",
          requestOptions
        )
        const dataTotal = await responseTotal.json()
        const responseType = await fetch(
          process.env.REACT_APP_API_HOST + "/roles",
          requestOptions
        )
        const roleData = await responseType.json()
        const responseOrder = await fetch(
          process.env.REACT_APP_API_HOST + "/users?" + paramSearch.toString(),
          requestOptions
        )
        const UserData = await responseOrder.json()
        return dispatch({
          type: ADMIN_USER_INIT_SUCCESS,
          payload: {
            total: dataTotal.result.length,
            data: UserData.result,
            roles: roleData.result
          },
        })
      } catch (error) {
        console.log(error)
        return dispatch({
          type: ADMIN_USER_INIT_ERROR,
          error: error,
        })
      }
    }
    fetchData(10, 1)
  }, [dispatch])
  var rowsData = []
  var columns = [
    { Header: "Tên người dùng", accessor: "name", align: "left" },
    { Header: "SĐT", accessor: "phone", align: "left" },
    { Header: "Email", accessor: "email", align: "left" },
    { Header: "Cấp quyền", accessor: "role", align: "center" },
    { Header: "action", accessor: "action", align: "center" },
  ]
  var rows = rowsData
  if (userData && userData.length > 0) {
    userData.map(async (element, index) => {
      rowsData.push({
        name: <p style={{ overflow: "clip" }}>{element.name}</p>,
        phone: <p>{element.phone}</p>,
        email: <p>{element.email}</p>,
        role: <p>{element.role.map((element) => element.name)}</p>,
        action: (
          < >
            <IconButton onClick={() => onBtnEditClick(element._id)} color="info" aria-label="edit">
              <EditIcon />
            </IconButton>
            {/* <IconButton onClick={() => onBtnDeleteClick(element.orderCode, element.orders._id)} color="error" aria-label="edit">
              <DeleteIcon id={element._id} />
            </IconButton> */}
          </>
        ),
      })
    })
  }

  const onBtnEditClick = (id) => {
    dispatch(openEditUserModal(id))
  }
  // const onBtnDeleteClick = (code, id) => {
  //   dispatch(openDeleteModalAction(code, id))
  // }
  const onBtnAddClick = useCallback(() => {
    dispatch(openAddModalAction())
  }, [dispatch])

  const handleCloseModal = useCallback(() => {
    dispatch(closeModal())
  }, [dispatch])
  const handleCloseModalSuccess = () => {
    dispatch(closeModal())
    dispatch(fetchUser(limit, page))
  }
  const onChangePagination = (event, value) => {
    dispatch(pageChangePagination(value))
    dispatch(fetchUser(10, value))
  }
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Card>
              <MDBox
                mx={2}
                mt={-3}
                py={3}
                px={2}
                variant="gradient"
                bgColor="info"
                borderRadius="lg"
                coloredShadow="info"
                className="table-title"
              >
                <MDTypography variant="h6" color="white">
                 Người dùng
                </MDTypography>
                <IconButton onClick={onBtnAddClick} style={{ color: "white" }}>
                  <AddCircleIcon />
                </IconButton>

              </MDBox>
              <MDBox pt={3}
                className="order-table"
              >
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>

            </Card>
            <div className="flex justify-center">
            <Pagination
              sx={{ justifyContent: "center" }}
              className="mt-5"
              onChange={onChangePagination}
              count={noPage}
              value={page}
              variant="outlined"
              shape="rounded"
            />
            </div>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
      <CreateUserModal close={handleCloseModal} open={openAddModal} name={addForm.name} email={addForm.email}
       errorMessage={errorMessage}  phone={addForm.phone} password={addForm.password} confirmPassword={addForm.confirmPassword}
        displayError={displayError} />
      <ModalSuccess
        close={handleCloseModalSuccess}
        openModal={success}
        content="Đóng thông báo"
        title={title}
      />
      <EditUserModal close={handleCloseModal}  open={openEditModal} name={editForm.name} id={editForm.id} role={editForm.role}
      email={editForm.email} displayError={displayError} errorMessage={errorMessage} />
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={pending}
        onClick={handleCloseModal}
      >
        <CircularProgress color="warning" />
      </Backdrop>  
    </DashboardLayout>
  )
}
export default UserTable
