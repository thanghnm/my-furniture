/* eslint-disable */


// Material Dashboard 2 React layouts
import ProductTable from "layouts/productTable"
import OrderTable from "layouts/orderTable"

// @mui icons
import Icon from "@mui/material/Icon"
import UserTable from "layouts/userTable"
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts"
import CategoryIcon from "@mui/icons-material/Category"
import StarBorderIcon from "@mui/icons-material/StarBorder"
import AuthorizeRoute from "components/protectRoute/authorizeRoute"

const routes = [

  {
    type: "collapse",
    name: "Products",
    key: "Products",
    icon: <CategoryIcon />,
    route: "/admin/products",
    component: 
    <AuthorizeRoute>
    <ProductTable />
    </AuthorizeRoute>,
  },
  {
    type: "collapse",
    name: "Orders",
    key: "Orders",
    icon: <StarBorderIcon />,
    route: "/admin/orders",
    component: 
    <AuthorizeRoute>
      <OrderTable />
      </AuthorizeRoute>,
  },
  {
    type: "collapse",
    name: "Users",
    key: "Users",
    icon: <ManageAccountsIcon />,
    route: "/admin/users",
    component: 
    <AuthorizeRoute>
    <UserTable />
    </AuthorizeRoute>,
  },
]

export default routes
