/* eslint-disable */

import { PRODUCT_DETAIL_QUANTITY_PLUS } from "constants/detail.constants"
import { PRODUCT_DETAIL_DESC_TAB_CHANGE } from "constants/detail.constants"
import { PRODUCT_DETAIL_FETCH_SUCCESS } from "constants/detail.constants"
import { PRODUCT_RELATE_FETCH_SUCCESS } from "constants/detail.constants"
import { PRODUCT_RELATE_FETCH_PENDING } from "constants/detail.constants"
import { PRODUCT_DETAIL_FETCH_PENDING } from "constants/detail.constants"
import { PRODUCT_DETAIL_QUANTITY_SUBTRACT } from "constants/detail.constants"
import { PRODUCT_DETAIL_SIZE_CHANGE } from "constants/detail.constants"
import { PRODUCT_DETAIL_COLOR_CHANGE } from "constants/detail.constants"

const initialState = {
  color: 0,
  size: 0,
  quantity: 1,
  tab: 0,
  pending: false,
  relatePending: false,
  detail: false,
}
const detailReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCT_DETAIL_COLOR_CHANGE:
      state.color = action.payload
      break
    case PRODUCT_DETAIL_SIZE_CHANGE:
      state.size = action.payload
      break
    case PRODUCT_DETAIL_QUANTITY_PLUS:
      // max quantity is 10
      if (state.quantity >= 10) {
        state.quantity = 10
      } else {
        state.quantity += 1
      }
      break
    case PRODUCT_DETAIL_QUANTITY_SUBTRACT:
      // min quantity is 0
      if (state.quantity <= 1) {
        state.quantity = 1
      } else {
        state.quantity -= 1
      }
      break
    case PRODUCT_DETAIL_DESC_TAB_CHANGE:
      state.tab = action.payload
      break
    case PRODUCT_DETAIL_FETCH_PENDING:
      state.pending = true
      break
    case PRODUCT_DETAIL_FETCH_SUCCESS:
      state.pending = false
      state.detail = action.payload
      break
    case PRODUCT_RELATE_FETCH_PENDING:
      state.relatePending = true
      break
    case PRODUCT_RELATE_FETCH_SUCCESS:
      state.relatePending = false
      state.relate = action.payload
      break
    default:
      break
  }
  return { ...state }
}
export default detailReducer
