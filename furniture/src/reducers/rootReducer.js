import { combineReducers } from "redux"
import homepageReducer from "./homepageReducer"
import headerReducer from "./headerReducer"
import menuReducer from "./menuReducer"
import loginReducer from "./loginReducer"
import detailReducer from "./detailReducer"
import cartReducer from "./cartReducer"
import productReducer from "./admin/productReducer"
import orderReducer from "./admin/orderReducer"
import userReducer from "./admin/userReducer"
import profileReducer from "./profileReducer"

const rootReducer = combineReducers({
  homepageReducer,
  headerReducer,
  menuReducer,
  productReducer,
  loginReducer,
  detailReducer,
  cartReducer,
  orderReducer,
  userReducer,
  profileReducer,
})
export default rootReducer
