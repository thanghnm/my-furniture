/* eslint-disable */

import { PROFILE_FETCH_SUCCESS } from "constants/profile.constants"
import { PROFILE_EMAIL_CHANGE } from "constants/profile.constants"
import { PROFILE_NEW_PASSWORD_CHANGE } from "constants/profile.constants"
import { PROFILE_UPDATE_PENDING } from "constants/profile.constants"
import { PROFILE_UPDATE_PASSWORD_INVALID } from "constants/profile.constants"
import { PROFILE_CLOSE_MODAL } from "constants/profile.constants"
import { PROFILE_UPDATE_SUCCESS } from "constants/profile.constants"
import { PROFILE_PASSWORD_CHANGE } from "constants/profile.constants"
import { PROFILE_NAME_CHANGE } from "constants/profile.constants"
import { PROFILE_IMAGE_CHANGE } from "constants/profile.constants"
import { PROFILE_OPEN_EDIT_FORM } from "constants/profile.constants"

const initialState = {
visible:false
,user:{
  role:"user",
    name:"",
    email:"",
    phone:"",
    password:"",
    newPassword:"",
    avatar:""
},
order:null,
pending:true,
success:false,
displayError:false,
errorMessage:""
}
const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROFILE_OPEN_EDIT_FORM: 
    state.visible =!state.visible
    break
    case PROFILE_IMAGE_CHANGE: 
    state.user.avatar =action.payload
    break
    case PROFILE_NAME_CHANGE: 
    state.user.name =action.payload
    break
    case PROFILE_EMAIL_CHANGE: 
    state.user.email =action.payload
    break
    case PROFILE_PASSWORD_CHANGE: 
    state.user.password =action.payload
    state.displayError = false
    break
    case PROFILE_NEW_PASSWORD_CHANGE: 
    state.user.newPassword =action.payload
    break
    case PROFILE_FETCH_SUCCESS: 
    state.user.id = action.payload._id
    state.user.name = action.payload.name
    state.user.email = action.payload.email
    state.user.phone = action.payload.phone
    state.order =action.payload.orders
    state.avatarUrl =action.payload.avatar==null?null:`${process.env.REACT_APP_API_AVATAR}avatar-${state.user.phone}`
    break
    case PROFILE_UPDATE_PENDING: 
    state.pending = false
    break
    case PROFILE_UPDATE_SUCCESS: 
    state.success = true
    state.avatarUrl =`${process.env.REACT_APP_API_AVATAR}avatar-${state.user.phone}?timestamp=${new Date().getTime()}`
    break
    case PROFILE_CLOSE_MODAL: 
    state.success = false
    break
    case PROFILE_UPDATE_PASSWORD_INVALID: 
    state.errorMessage = action.message
    state.displayError = true
    break
    default:
      break
  }
  return { ...state }
}
export default profileReducer
