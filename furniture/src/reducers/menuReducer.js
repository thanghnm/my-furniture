/* eslint-disable */

import {
  ADD_TO_CART,
  FILTER_NAME_CHANGE,
  FILTER_PRICE_CHANGE,
  FILTER_TYPE_CHANGE,
  MENU_PRODUCT_FETCH_ERROR,
  MENU_PRODUCT_FETCH_PENDIND,
  MENU_PRODUCT_FETCH_SUCCESS,
  MENU_PRODUCT_PAGE_CHANGE,
  MENU_PRODUCT_TYPE_FETCH_ERROR,
  MENU_PRODUCT_TYPE_FETCH_PENDIND,
  MENU_PRODUCT_TYPE_FETCH_SUCCESS,
} from "../constants/menu.constants"

const initialState = {
  filter: {
    price: "",
    type: "",
    name: "",
  },
  productType: [],
  limit: 12,
  dataProduct: [],
  pending: false,
  noPage: 0,
  page: 1,
}
// const urlParams = new URLSearchParams(window.location.search);
// const myParam = urlParams.get('myParam');
// console.log(myParam)
const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case FILTER_PRICE_CHANGE:
      state.filter.price = action.payload
      break
    case FILTER_NAME_CHANGE:
      state.filter.name = action.payload
      break
    case FILTER_TYPE_CHANGE:
      state.filter.type = action.payload
      console.log(state.filter.type)
      break
    case MENU_PRODUCT_TYPE_FETCH_PENDIND:
      break
    case MENU_PRODUCT_TYPE_FETCH_SUCCESS:
      state.productType = action.payload

      break
    // case MENU_PRODUCT_TYPE_FETCH_ERROR:
    //   alert(action.error)
    //   break
    case MENU_PRODUCT_FETCH_PENDIND:
      state.pending = true
      break
    case MENU_PRODUCT_FETCH_SUCCESS:
      state.pending = false
      state.dataProduct = action.payload.data
      state.noPage = Math.ceil(action.payload.total / state.limit)
      break
    case MENU_PRODUCT_PAGE_CHANGE:
      state.page = action.page
      break
    // case MENU_PRODUCT_FETCH_ERROR:
    //   alert(action.error)
    //   break
    case ADD_TO_CART:
      state.updateCart = true
      // Nếu đã có item trong local thì giữ nguyên
      if (localStorage.getItem("cartItems")) {
        const localCart = JSON.parse(localStorage.getItem("cartItems"))
        state.cartItems = localCart
      }
      // nếu chưa thì là mảng rỗng
      else {
        state.cartItems = []
      }
      // update cart
      // Khi click add cart , kiểm tra xem mặt hàng đã có trong đơn hàng chưa nếu có rồi thì tăng số lượng thêm 1
      state.cartItems.map((e) => {
        if (e._id == action.payload._id) {
          return (e.quantity += 1)
        }
      })
      // Nếu chưa thì thêm mặt hàng vào đơn hàng
      if (!state.cartItems.map((e) => e._id == action.payload._id).includes(true)) {
        state.cartItems.push(action.payload)
      }
      // lưu lại localstorage
      localStorage.setItem("cartItems", JSON.stringify(state.cartItems))
      break
    default:
      break
  }
  return { ...state }
}
export default menuReducer
