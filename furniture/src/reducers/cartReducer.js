/* eslint-disable */

import { ADMIN_ORDER_OPEN_ADD_MODAL } from "constants/admin.constants"
import { CART_QUANTITY_SUBTRACT } from "constants/cart.contants"
import { CART_CLOSE_ALERT } from "constants/cart.contants"
import { CART_CLOSE_MODAL_ORDER_SUCCESS } from "constants/cart.contants"
import { CART_CLOSE_MODAL } from "constants/cart.contants"
import { CART_TO_CHECKOUT } from "constants/cart.contants"
import { CART_REMOVE } from "constants/cart.contants"
import { CART_QUANTITY_PLUS } from "constants/cart.contants"
const itemArray =localStorage.getItem("cartItems")? JSON.parse(localStorage.getItem("cartItems")):[]
const initialState = {
  cartItems: itemArray,
  openLoginDirector: false,
}
const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case CART_REMOVE:
      state.cartItems = JSON.parse(localStorage.getItem("cartItems"))
      //remove item theo index
      state.cartItems.splice(action.payload, 1)
      // set lai storage
      localStorage.setItem("cartItems", JSON.stringify(state.cartItems))
      break
    case CART_QUANTITY_PLUS:
      // Neu so luong + lon hon 10 thi` return 10
      if (state.cartItems[action.payload].quantity > 10) {
        state.cartItems[action.payload].quantity = 10
      } else {
        //++
        state.cartItems[action.payload].quantity += 1
        localStorage.setItem("cartItems", JSON.stringify(state.cartItems))
      }
      break
    case CART_QUANTITY_SUBTRACT:
      // Neu so luong nho hon 2 thi return 1
      if (state.cartItems[action.payload].quantity < 2) {
        state.cartItems[action.payload].quantity = 1
      } else {
        //--
        state.cartItems[action.payload].quantity -= 1
        localStorage.setItem("cartItems", JSON.stringify(state.cartItems))
      }
      break

    default:
      break
  }
  return { ...state }
}
export default cartReducer
