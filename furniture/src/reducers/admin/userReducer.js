/* eslint-disable */

import { ADMIN_USER_INIT_ERROR } from "constants/admin.constants"
import { ADMIN_USER_CLOSE_MODAL } from "constants/admin.constants"
import { ADMIN_USER_ADD_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_PASSWORD_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_PHONE_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_CONFIRM_INVALID } from "constants/admin.constants"
import { ADMIN_USER_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_PAGE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_EDIT_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_EDIT_ROLE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_EDIT_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_USER_EDIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_EDIT_EMAIL_INVALID } from "constants/admin.constants"
import { ADMIN_USER_FETCH_TO_EDIT_PENDIND } from "constants/admin.constants"
import { ADMIN_USER_EDIT_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_OPEN_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_USER_FETCH_PENDING } from "constants/admin.constants"
import { ADMIN_USER_CREATE_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_ADD_PASSWORD_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_EMAIL_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_CONFIRM_PASSWORD_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_PHONE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_OPEN_ADD_MODAL } from "constants/admin.constants"
import { ADMIN_USER_INIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_INIT_PENDING } from "constants/admin.constants"

const initialState = {
    page: 1,
    limit: 10,
    success: false,
    title: "",
    openAddModal: false,
    openEditModal: false,
    pending: false,
    addForm: {
        name: "",
        email: "",
        phone: "",
        password: "",
        confirmPassword: ""
    },
    editForm: {
        name: "",
        email: "",
        role: "user"
    },
    displayError: {
        name: false,
        password: false,
        email: false,
        phone: false,
        confirmPassword: false,
        nameEdit: false,
        emailEdit: false
    },
    errorMessage: ""
}
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADMIN_USER_INIT_PENDING:
            break
        case ADMIN_USER_INIT_SUCCESS:
            state.userData = action.payload.data
            state.rolesData = action.payload.roles
            state.noPage = Math.ceil(action.payload.total / 10)
            state.pending=false
            break
        case ADMIN_USER_INIT_ERROR:
            break
        case ADMIN_USER_OPEN_ADD_MODAL:
            state.openAddModal = true
            break
        case ADMIN_USER_CLOSE_MODAL:
            state.openAddModal = false
            state.success = false
            state.openEditModal = false
            break
        case ADMIN_USER_ADD_NAME_CHANGE:
            state.addForm.name = action.payload
            state.displayError.name = false
            break
        case ADMIN_USER_ADD_EMAIL_CHANGE:
            state.addForm.email = action.payload
            state.displayError.email = false
            break
        case ADMIN_USER_ADD_PHONE_CHANGE:
            state.addForm.phone = action.payload
            state.displayError.phone = false
            break
        case ADMIN_USER_ADD_PASSWORD_CHANGE:
            state.addForm.password = action.payload
            state.displayError.password = false
            break
        case ADMIN_USER_ADD_CONFIRM_PASSWORD_CHANGE:
            state.addForm.confirmPassword = action.payload
            state.displayError.confirmPassword = false
            break
        case ADMIN_USER_ADD_NAME_INVALID:
            state.displayError.name = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_ADD_EMAIL_INVALID:
            state.displayError.email = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_ADD_PHONE_INVALID:
            state.displayError.phone = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_ADD_PASSWORD_INVALID:
            state.displayError.password = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_ADD_CONFIRM_INVALID:
            state.displayError.confirmPassword = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_CREATE_SUCCESS:
            state.success = true
            state.title = "Tạo người dùng thành công"
            break
        case ADMIN_USER_FETCH_SUCCESS:
            state.userData = action.payload.data
            state.noPage = Math.ceil(action.payload.total / 10)
            state.pending = false

            break
        case ADMIN_USER_FETCH_PENDING:
            state.pending = true
            break
        case ADMIN_USER_PAGE_CHANGE:
            state.page = action.page
            break
        case ADMIN_USER_FETCH_TO_EDIT_PENDIND:
            state.pending = true
            break
        case ADMIN_USER_OPEN_EDIT_MODAL:
            state.openEditModal = true
            state.pending = false
            state.editForm.name = action.payload.name
            state.editForm.email = action.payload.email
            state.editForm.id = action.payload._id
            break
        case ADMIN_USER_EDIT_NAME_CHANGE:
            state.editForm.name = action.payload
            state.displayError.nameEdit = false
            break
        case ADMIN_USER_EDIT_EMAIL_CHANGE:
            state.editForm.email = action.payload
            state.displayError.emailEdit = false
            break
        case ADMIN_USER_EDIT_ROLE_CHANGE:
            state.editForm.role = action.payload
            break
        case ADMIN_USER_EDIT_NAME_INVALID:
            state.displayError.nameEdit = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_EDIT_EMAIL_INVALID:
            state.displayError.emailEdit = true
            state.errorMessage = action.message
            break
        case ADMIN_USER_EDIT_SUCCESS:
            state.pending=false
            state.success = true,
            state.title = "Chỉnh sửa người dùng thành công"
            break
        default:
            break
    }
    return { ...state }
}
export default userReducer
