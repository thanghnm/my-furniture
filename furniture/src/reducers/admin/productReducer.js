// prettier-ignore
/* eslint-disable */
import { ADMIN_PRODUCT_ADD_NAME_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_ADD_PRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_AMOUNT_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_IMG_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_PRICE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_AMOUNT_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_CREATE_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_TYPE_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_FETCH_PENDIND } from "constants/admin.constants";
import { ADMIN_PRODUCT_FETCH_ERROR } from "constants/admin.constants";
import { ADMIN_PRODUCT_FULFILL_EDIT_MODAL } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_TYPE_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_BUYPRICE_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_DESCRIPTION_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_PAGE_CHANGE } from "constants/admin.constants";
import { ADMIN_ORDER_FILTER_NAME_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_FILTER_NAME_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_INIT_SUCCESS } from "constants/admin.constants";
import { ADMIN_PRODUCT_DELETE_SUCCESS } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_SUCCESS } from "constants/admin.constants";
import { ADMIN_PRODUCT_FILTER_AMOUNT_CHANGE } from "constants/admin.constants";
import { ADMIN_ORDER_FILTER_AMOUNT_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_OPEN_DELETE_MODAL } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_IMG_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_AMOUNT_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_PRICE_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_EDIT_NAME_CHANGE } from "constants/admin.constants";
import { ADMIN_PRODUCT_OPEN_EDIT_MODAL } from "constants/admin.constants";
import { ADMIN_PRODUCT_FETCH_SUCCESS } from "constants/admin.constants";
import { ADMIN_PRODUCT_TYPE_FETCH_ERROR } from "constants/admin.constants"
import { ADMIN_PRODUCT_TYPE_FETCH_PENDIND } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_IMAGE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_BUYPRICE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_TYPE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_CONFIRM } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_DESCRIPTION_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_BUYPRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_TYPE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_CLOSE_MODAL } from "constants/admin.constants"
import { ADMIN_PRODUCT_OPEN_ADD_MODAL } from "constants/admin.constants"

const initialState = {
  openAddModal: false,
  openEditModal: false,
  openDeleteModal: false,
  addProductForm: {
    name: "",
    type: "",
    price: "",
    buyPrice: "",
    amount: "",
    description: "",
    imageUrl: "",
  },
  editProductForm: {
    name: "",
    type: "",
    price: "",
    buyPrice: "",
    amount: "",
    description: "",
    imageUrl: "",
  },
  filter: {
    amount: "",
    name: "",
  },
  productData: [],
  displayError: {
    name: false,
    type: false,
    price: false,
    buyPrice: false,
    amount: false,
    description: false,
    imageUrl: false,
  },
  errorMessage: "",
  productType: [],
  openSuccessModal: false,
  page: 1,
  noPage: 0,
  pending: false,
  deleteName: null,
  title: ""
}
const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADMIN_PRODUCT_OPEN_ADD_MODAL:
      state.openAddModal = true
      break
    case ADMIN_PRODUCT_CLOSE_MODAL:
      state.openDeleteModal = false
      state.openAddModal = false
      state.openEditModal = false
      state.openSuccessModal = false
      break
    case ADMIN_PRODUCT_ADD_NAME_CHANGE:
      state.addProductForm.name = action.payload
      state.displayError.name = false
      break
    case ADMIN_PRODUCT_ADD_TYPE_CHANGE:
      state.addProductForm.type = action.payload
      state.displayError.type = false
      break
    case ADMIN_PRODUCT_ADD_PRICE_CHANGE:
      state.addProductForm.price = action.payload
      state.displayError.price = false
      break
    case ADMIN_PRODUCT_ADD_BUYPRICE_CHANGE:
      state.addProductForm.buyPrice = action.payload
      state.displayError.buyPrice = false
      break
    case ADMIN_PRODUCT_ADD_AMOUNT_CHANGE:
      state.addProductForm.amount = action.payload
      state.displayError.amount = false
      break
    case ADMIN_PRODUCT_ADD_DESCRIPTION_CHANGE:
      state.addProductForm.description = action.payload
      break
    case ADMIN_PRODUCT_ADD_IMG_CHANGE:
      state.addProductForm.imageUrl = action.payload
      state.displayError.imageUrl = false
      break
    case ADMIN_PRODUCT_ADD_CONFIRM:
      // state.addProductForm.imageUrl = action.payload
      break
    case ADMIN_PRODUCT_ADD_NAME_INVALID:
      state.displayError.name = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_ADD_TYPE_INVALID:
      state.displayError.type = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_ADD_PRICE_INVALID:
      state.displayError.price = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_ADD_BUYPRICE_INVALID:
      state.displayError.buyPrice = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_ADD_AMOUNT_INVALID:
      state.displayError.amount = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_ADD_IMAGE_INVALID:
      state.displayError.imageUrl = true
      state.errorMessage = action.message
      break
    case ADMIN_PRODUCT_CREATE_SUCCESS:
      state.openSuccessModal = true
      state.title = "Thêm hàng hóa thành công"
      break
    case ADMIN_PRODUCT_TYPE_FETCH_PENDIND:
      break
    case ADMIN_PRODUCT_TYPE_FETCH_SUCCESS:
      state.productType = action.payload
      break
    case ADMIN_PRODUCT_TYPE_FETCH_ERROR:
      break

    case ADMIN_PRODUCT_INIT_SUCCESS:
      state.productData = action.payload.data
      state.productType = action.payload.type
      // limit = 10
      state.noPage = Math.ceil(action.payload.total / 10)
      break

    case ADMIN_PRODUCT_FETCH_PENDIND:
      state.pending = true
      break
    case ADMIN_PRODUCT_FETCH_SUCCESS:
      state.pending = false
      state.productData = action.payload.data
      // limit = 10
      state.noPage = Math.ceil(action.payload.total / 10)
      break
    case ADMIN_PRODUCT_FETCH_ERROR:
      break
    //EDIT
    case ADMIN_PRODUCT_OPEN_EDIT_MODAL:
      state.openEditModal = true
      state.disable = true
      break
    case ADMIN_PRODUCT_FULFILL_EDIT_MODAL:
      state.editProductForm = action.payload
      state.editProductForm.imageUrl = ""
      state.disable = false
      break
    case ADMIN_PRODUCT_EDIT_NAME_CHANGE:
      state.editProductForm.name = action.payload
      state.displayError.name = false
      break
    case ADMIN_PRODUCT_EDIT_TYPE_CHANGE:
      state.editProductForm.type = action.payload
      state.displayError.type = false
      break
    case ADMIN_PRODUCT_EDIT_PRICE_CHANGE:
      state.editProductForm.price = action.payload
      state.displayError.price = false
      break
    case ADMIN_PRODUCT_EDIT_BUYPRICE_CHANGE:
      state.editProductForm.buyPrice = action.payload
      state.displayError.buyPrice = false
      break
    case ADMIN_PRODUCT_EDIT_AMOUNT_CHANGE:
      state.editProductForm.amount = action.payload
      state.displayError.amount = false
      break
    case ADMIN_PRODUCT_EDIT_DESCRIPTION_CHANGE:
      state.editProductForm.description = action.payload
      break
    case ADMIN_PRODUCT_EDIT_IMG_CHANGE:
      state.editProductForm.imageUrl = action.payload
      state.displayError.imageUrl = false
      break
    case ADMIN_PRODUCT_EDIT_SUCCESS:
      state.openSuccessModal = true
      state.title = "Sửa hàng hóa thành công"
      break
    case ADMIN_PRODUCT_PAGE_CHANGE:
      state.page = action.page
      break
    //DELETE
    case ADMIN_PRODUCT_OPEN_DELETE_MODAL:
      state.openDeleteModal = true
      state.deleteProductId = action.payload.id
      state.deleteName = action.payload.name
      break
      case ADMIN_PRODUCT_DELETE_SUCCESS:
        state.openSuccessModal = true
        state.title="Xóa hàng hóa thành công"
        break
    //FILTER
    case ADMIN_PRODUCT_FILTER_NAME_CHANGE:
      state.filter.name = action.payload
      break
    case ADMIN_PRODUCT_FILTER_AMOUNT_CHANGE:
      state.filter.amount = action.payload
      break
    default:
      break
  }

  return { ...state }
}
export default productReducer
