/* eslint-disable */

import { ADMIN_ORDER_CREATE_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_ADDRESS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_CITY_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CLOSE_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_OUTSTOCK } from "constants/admin.constants"
import { CREATE_ORDER_NAME_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_EMAIL_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_CITY_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_INIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_QUANTITY_PLUS } from "constants/admin.constants"
import { CREATE_ORDER_BLANK_CART } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_PREV_STEP } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_TO_EDIT_PENDING } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_ADDRESS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_SHIPPED_TIME_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_EDIt_QUANTITY_SUBTRACT } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_EDIT_QUANTITY_SUBTRACT } from "constants/admin.constants"
import { EDIT_ORDER_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_DELETE_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_DELETE_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_STATUS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_PAGE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_CODE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_DELETE_SUCCESS } from "constants/admin.constants"
import { EDIT_ORDER_PENDING } from "constants/admin.constants"
import { EDIT_ORDER_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_CART_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_EDIT_QUANTITY_PLUS } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_STATUS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_CITY_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_NOTE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NEXT_STEP } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_QUANTITY_SUBTRACT } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_CART_CHANGE } from "constants/admin.constants"
import { ADMIN_CLOSE_MODAL_ORDER_SUCCESS } from "constants/admin.constants"
import { CREATE_ORDER_SHIPDATE_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_ADDRESS_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_PHONE_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_PENDING } from "constants/admin.constants"
import { DATE_PICKER_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NOTE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_PHONE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_ADD_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NAME_CHANGE } from "constants/admin.constants"
import { CART_CLOSE_ALERT } from "constants/cart.contants"
const initialState = {
  openAddModal: false,
  createOrderForm: {
    address: "",
    city: "",
    note: "",
    shippedDate: undefined,
  },
  editOrderForm: {
    address: "",
    city: "",
    note: "",
    shippedDate: undefined,
    status: ""
  },
  filter: {
    code: "",
    status: ""
  },
  step: 0,
  page: 1,
  pending: false,
  outStockMessage: [],
  displayError: {
    phone: false,
    email: false,
    name: false,
    address: false,
    city: false,
    shippedDate: false,
    cart: false,
  },
  openEditModal: false,
  openDeleteModal: false,
  errorMessage: "",
  openLoginDirector: false,
  createOrderSuccess: false,
  orderData: [],
  selectedItems: []
}
const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADMIN_ORDER_OPEN_ADD_MODAL:
      if (window.location.pathname == "/admin/orders") {
        state.openAddModal = true
        state.createOrderForm.name = ""
        state.createOrderForm.email = ""
        state.createOrderForm.phone = ""
        state.step = 0
      } else {
        const user = JSON.parse(sessionStorage.getItem("user"))
        if (!user) {
          state.openLoginDirector = true
        } else {
          state.openAddModal = true
          state.createOrderForm.name = user.name
          state.createOrderForm.email = user.email
          state.createOrderForm.phone = user.phone
        }
      }
      break
    case ADMIN_ORDER_CLOSE_MODAL:
      state.openAddModal = false
      state.openEditModal = false
      state.step = 0
      state.openDeleteModal = false

      break
    case ADMIN_ORDER_CREATE_NAME_CHANGE:
      state.createOrderForm.name = action.payload
      state.displayError.name = false
      break
    case ADMIN_ORDER_CREATE_EMAIL_CHANGE:
      state.createOrderForm.email = action.payload
      state.displayError.email = false
      break
    case ADMIN_ORDER_CREATE_PHONE_CHANGE:
      state.createOrderForm.phone = action.payload
      state.displayError.phone = false
      break
    case ADMIN_ORDER_CREATE_ADDRESS_CHANGE:
      state.createOrderForm.address = action.payload
      state.displayError.address = false
      break
    case ADMIN_ORDER_CREATE_NOTE_CHANGE:
      state.createOrderForm.note = action.payload
      break

    case ADMIN_ORDER_CREATE_CITY_CHANGE:
      state.createOrderForm.city = action.payload
      state.displayError.city = false
      break
    case DATE_PICKER_CHANGE:
      state.createOrderForm.shippedDate = action.payload
      state.displayError.shippedDate = false
      break
    case ADMIN_ORDER_CREATE_OUTSTOCK:
      state.outStockMessage = action.message
      state.openAddModal = false
      state.pending = false
      break
    case CREATE_ORDER_PENDING:
      state.pending = true
      break
    case CART_CLOSE_ALERT:
      state.outStockMessage = []
      break
    case CREATE_ORDER_NAME_INVALID:
      state.displayError.name = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_PHONE_INVALID:
      state.displayError.phone = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_EMAIL_INVALID:
      state.displayError.email = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_ADDRESS_INVALID:
      state.displayError.address = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_CITY_INVALID:
      state.displayError.city = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_SHIPDATE_INVALID:
      state.displayError.shippedDate = true
      state.errorMessage = action.message
      state.pending = false
      state.step = 0
      break
    case CREATE_ORDER_BLANK_CART:
      state.displayError.cart = true
      state.errorMessage = action.message
      state.pending = false
      break
    case CREATE_ORDER_SUCCESS:
      state.createOrderSuccess = true
      state.title = "Tạo đơn hàng thành công"
      state.pending = false
      break
    case ADMIN_CLOSE_MODAL_ORDER_SUCCESS:
      state.createOrderSuccess = false
      state.openAddModal = false
      localStorage.setItem("cartItems", [])
      window.location.reload()
      break
    case ADMIN_ORDER_INIT_SUCCESS:
      state.productData = action.payload.product
      state.orderData = action.payload.data
      state.noPage = Math.ceil(action.payload.total / 10)
      break
    case ADMIN_ORDER_FETCH_SUCCESS:
      state.orderData = action.payload.data
      state.noPage = Math.ceil(action.payload.total / 10)
      break
    case ADMIN_ORDER_CREATE_CART_CHANGE:
      state.selectedItems = action.payload
      state.displayError.cart = false
      break
    case ADMIN_ORDER_MINICART_QUANTITY_PLUS:
      if (state.selectedItems[action.payload].quantity > 9) {
        state.selectedItems[action.payload].quantity = 10
      }
      else {
        state.selectedItems[action.payload].quantity += 1
      }
      break
    case ADMIN_ORDER_MINICART_QUANTITY_SUBTRACT:
      if (state.selectedItems[action.payload].quantity < 2) {
        state.selectedItems[action.payload].quantity = 1
      }
      else {
        state.selectedItems[action.payload].quantity -= 1
      }
      break
    case ADMIN_ORDER_CREATE_NEXT_STEP:
      state.step += 1
      break
    case ADMIN_ORDER_CREATE_PREV_STEP:
      state.step -= 1
      break
    case ADMIN_ORDER_FETCH_TO_EDIT_PENDING:
      state.pending = true
      break
    case ADMIN_ORDER_OPEN_EDIT_MODAL:
      state.pending = false
      state.openEditModal = true
      state.editOrderForm = action.payload.result.form
      state.editOrderDetails = action.payload.result.details
      state.editOrderForm.shippedDate = ""
      break
    case ADMIN_ORDER_EDIT_NOTE_CHANGE:
      state.editOrderForm.note = action.payload
      break
    case ADMIN_ORDER_EDIT_ADDRESS_CHANGE:
      state.editOrderForm.address = action.payload
      break
    case ADMIN_ORDER_EDIT_CITY_CHANGE:
      state.editOrderForm.city = action.payload
      break
    case ADMIN_ORDER_EDIT_SHIPPED_TIME_CHANGE:
      state.editOrderForm.shippedDate = action.payload
      break
    case ADMIN_ORDER_EDIT_STATUS_CHANGE:
      state.editOrderForm.status = action.payload
      break
    case ADMIN_ORDER_MINICART_EDIT_QUANTITY_SUBTRACT:
      if (state.editOrderDetails[action.payload].quantity <= 1) {
        state.editOrderDetails.splice(action.payload, 1)
      }
      else {
        state.editOrderDetails[action.payload].quantity -= 1
      }
      break
    case ADMIN_ORDER_MINICART_EDIT_QUANTITY_PLUS:
      if (state.editOrderDetails[action.payload].quantity < state.editOrderDetails[action.payload].amount) {
        state.editOrderDetails[action.payload].quantity += 1
      }
      break
    case ADMIN_ORDER_EDIT_CART_CHANGE:
      state.editOrderDetails = action.payload
      break
    case EDIT_ORDER_PENDING:
      state.pending = true
      break
    case EDIT_ORDER_SUCCESS:
      state.createOrderSuccess = true
      state.title = "Sửa đơn hàng thành công"
      state.pending = false
      break
    case ADMIN_ORDER_OPEN_DELETE_MODAL:
      state.openDeleteModal = true
      state.orderCode = action.payload.code
      state.deleteId = action.payload.id

      break
    case ADMIN_ORDER_DELETE_SUCCESS:
      state.createOrderSuccess = true
      state.title = "Xóa đơn hàng thành công"
      state.pending = false
      break
    case ADMIN_ORDER_DELETE_ERROR:
      console.log(action.error);
      break
    case ADMIN_ORDER_FILTER_CODE_CHANGE:
      state.filter.code = action.payload
      break
    case ADMIN_ORDER_FILTER_STATUS_CHANGE:
      state.filter.status = action.payload
      break
    case ADMIN_ORDER_PAGE_CHANGE:
      state.page = action.page
      break
    default:
      break
  }
  return { ...state }
}
export default orderReducer
