/* eslint-disable */

import { HEADER_LOG_OUT } from "constants/header.constants"
import { CART_DRAWER_CLOSE } from "constants/header.constants"
import { CART_DRAWER_OPEN } from "constants/header.constants"
const itemArray = localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")) : []
const user = sessionStorage.getItem("user") ? JSON.parse(sessionStorage.getItem("user")) : null

const initialState = {
  openCartDrawer: false,
  cartItems: itemArray,
  user: user
}
const headerReducer = (state = initialState, action) => {
  switch (action.type) {
    case CART_DRAWER_OPEN:
      state.openCartDrawer = true
      break
    case CART_DRAWER_CLOSE:
      state.openCartDrawer = false
      break
    case HEADER_LOG_OUT:
      sessionStorage.clear()
      state.user = undefined
      break
    default:
      break
  }
  return { ...state }
}
export default headerReducer
