// prettier-ignore
/* eslint-disable */

import { SIGN_IN_SUCCESS } from "constants/login.constant";
import { SIGN_IN_PHONE_INVALID } from "constants/login.constant"
import { SIGN_IN_WRONG_PASSWORD } from "constants/login.constant"
import { SIGN_UP_PHONE_CHANGE } from "constants/login.constant"
import { SIGN_UP_CONFIRM_PASSWORD_CHANGE } from "constants/login.constant"
import { SIGN_UP_EMAIL_CHANGE } from "constants/login.constant"
import { SIGN_UP_USERNAME_INVALID } from "constants/login.constant"
import { SIGN_UP_PASSWORD_INVALID } from "constants/login.constant"
import { SIGN_UP_PHONE_USED } from "constants/login.constant"
import { SIGN_UP_ERROR } from "constants/login.constant"
import { SIGN_IN_PASSWORD_BLANK } from "constants/login.constant"
import { SIGN_UP_SUCCESS } from "constants/login.constant"
import { SIGN_UP_CONFIRM_PASSWORD_INVALID } from "constants/login.constant"
import { SIGN_UP_EMAIL_INVALID } from "constants/login.constant"
import { SIGN_UP_PHONE_INVALID } from "constants/login.constant"
import { SIGN_UP_NAME_CHANGE } from "constants/login.constant"
import { SIGN_UP_PASSWORD_CHANGE } from "constants/login.constant"
// import { SIGN_IN_MODE } from "constants/login.constant";
// import { SIGN_UP_MODE } from "constants/login.constant";
import { SIGN_IN_ERROR } from "constants/login.constant"
import { SIGN_IN_PHONE_NOT_FOUNDED } from "constants/login.constant"
import { SIGN_IN_PENDING } from "constants/login.constant"
import { SIGN_IN_PASSWORD_CHANGE } from "constants/login.constant"
import { SIGN_IN_PHONE_CHANGE } from "constants/login.constant"
const initialState = {
  signInForm: {
    phone: "",
    password: "",
  },
  displayError: {
    phone: false,
    password: false,
    phoneSU: false,
    nameSU: false,
    emailSU: false,
    passwordSU: false,
    confirmPasswordSU: false,
  },
  errorMessage: "",
  signUpForm: {
    phone: "",
    password: "",
    confirmPassword: "",
    email: "",
    name: "",
  },
  signUpSuccess: false,
  pending: {
    signIn: false,
    signUp: false,
  },
}
const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGN_IN_PHONE_CHANGE:
      state.signInForm.phone = action.payload
      state.displayError.phone = false
      break
    case SIGN_IN_PASSWORD_CHANGE:
      state.signInForm.password = action.payload
      state.displayError.password = false
      break
    case SIGN_IN_PENDING:
      state.pending.signIn = true
      break
    case SIGN_IN_PHONE_INVALID:
      state.pending.signIn = false
      state.displayError.phone = true
      state.errorMessage = action.message
      break
    case SIGN_IN_PASSWORD_BLANK:
      state.pending.signIn = false
      state.displayError.password = true
      state.errorMessage = action.message
      break
    case SIGN_IN_PHONE_NOT_FOUNDED:
      state.pending.signIn = false
      state.displayError.phone = true
      state.errorMessage = action.message
      break
    case SIGN_IN_WRONG_PASSWORD:
      state.pending.signIn = false
      state.displayError.password = true
      state.errorMessage = action.message
      break
    case SIGN_IN_SUCCESS:
      state.pending.signIn = false
      sessionStorage.setItem("refreshToken", action.refreshToken)
      var user = action.user
      sessionStorage.setItem("user", JSON.stringify(user))
      window.location.href = "/"
      break
    case SIGN_IN_ERROR:
      console.log("error")
      break
    case SIGN_UP_PHONE_CHANGE:
      state.signUpForm.phone = action.payload
      state.displayError.phoneSU = false
      break
    case SIGN_UP_PASSWORD_CHANGE:
      state.signUpForm.password = action.payload
      state.displayError.passwordSU = false
      break
    case SIGN_UP_CONFIRM_PASSWORD_CHANGE:
      state.signUpForm.confirmPassword = action.payload
      state.displayError.confirmPasswordSU = false
      break
    case SIGN_UP_NAME_CHANGE:
      state.signUpForm.name = action.payload
      state.displayError.nameSU = false
      break
    case SIGN_UP_EMAIL_CHANGE:
      state.signUpForm.email = action.payload
      state.displayError.emailSU = false
      break
    case SIGN_IN_PENDING:
      state.pending.signUp = true
      break
    case SIGN_UP_PHONE_INVALID:
      state.pending.signUp = false
      state.displayError.phoneSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_USERNAME_INVALID:
      state.pending.signUp = false
      state.displayError.nameSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_EMAIL_INVALID:
      state.pending.signUp = false
      state.displayError.emailSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_PASSWORD_INVALID:
      state.pending.signUp = false
      state.displayError.passwordSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_CONFIRM_PASSWORD_INVALID:
      state.pending.signUp = false
      state.displayError.confirmPasswordSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_PHONE_USED:
      state.pending.signUp = false
      state.displayError.phoneSU = true
      state.errorMessage = action.message
      break
    case SIGN_UP_SUCCESS:
      state.pending.signUp = false
      state.signUpSuccess = true
      break
    case SIGN_UP_ERROR:
      state.pending.signUp = false
      break
    default:
      break
  }
  return { ...state }
}
export default loginReducer
