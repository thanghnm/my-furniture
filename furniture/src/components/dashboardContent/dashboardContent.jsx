// prettier-ignore
/* eslint-disable */
import AboutUs from "./aboutUs/aboutUs"
import HomepageCarousel from "./carousel/homepageCarousel"
import News from "./news/news"
import Propose from "./propose/propose"

const DashboardContent = () => {
  const showItem = () => {
    var reveals = document.querySelectorAll(".reveal")
    for (var bI = 0; bI < reveals.length; bI++) {
      var windowHeight = window.innerHeight
      var revealTop = reveals[bI].getBoundingClientRect().top
      var revealPoint = 300
      if (revealTop < windowHeight - revealPoint) {
        reveals[bI].classList.add("active")
      }
    }
  }
  window.addEventListener("scroll", showItem)

  return (
    <div>
      <HomepageCarousel />
      <AboutUs />
      <Propose />
      <News />
    </div>
  )
}
export default DashboardContent
