/* eslint-disable */
import React from "react"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
var settings = {
  autoplay: true,
  autoplaySpeed: 3000,
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  cssEase: "linear",
}
const HomepageCarousel = () => {
  return (
    <div className="">
      <Slider {...settings} id="crsl">
        <div className="relative">
          <img
            src="https://kika.vn/wp-content/uploads/2022/08/noi-that-dep-phong-khach.jpg"
            alt="crsl"
            className="img-crsl"
          />
          <div className="absolute crsl-content" style={{ left: "60%" }}>
            <p>
              Chào mừng đến với <span>My Furniture</span>
              <br></br>Chúng tôi chọn cho bạn nội thất tinh tế nhất
            </p>
          </div>
        </div>
        <div className="relative">
          <img src="https://wallpapercave.com/wp/wp9625940.jpg" alt="crsl" className="img-crsl" />
          <div className="absolute crsl-content" style={{ left: "60%" }}>
            <p>
              <span>Kiểu dáng</span> tinh tế, thời trang<br></br>
              <span>Giá cả</span> hợp lý<br></br>
              <span>Chất lượng</span> vững bền
            </p>
          </div>
        </div>
        <div className="relative">
          <img src="https://wallpapercave.com/wp/wp9625933.jpg" alt="crsl" className="img-crsl" />
          <div className="absolute crsl-content" style={{ left: "10%" }}>
            <p>
              <span>Miễn phí</span> vận chuyển cho đơn hàng trên 10 triệu đồng<br></br>
              <span>Bảo hành</span> từ 2 đến 5 năm
            </p>
          </div>
        </div>
      </Slider>
    </div>
  )
}
export default HomepageCarousel
