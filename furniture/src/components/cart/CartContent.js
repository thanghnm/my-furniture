/* eslint-disable */
import { Alert, Button, ButtonGroup, Collapse, Divider, IconButton, Snackbar } from "@mui/material"
import { openCreateOrderModal } from "actions/admin/order.action"
import { subtractItem } from "actions/cart.action"
import { closeModal } from "actions/cart.action"
import { plusItem } from "actions/cart.action"
import { removeItem } from "actions/cart.action"
import { DirModal } from "components/modal/directorModal"
import CreateOrderModal from "components/modal/table-order/createOrderModal"
import { element } from "prop-types"
import { useDispatch, useSelector } from "react-redux"
import CloseIcon from "@mui/icons-material/Close"
import { closeAlert } from "actions/cart.action"
import { ModalSuccess } from "components/modal/successModal"
import { closeModalOrderSuccess } from "actions/admin/order.action"
import EmptyCart from "../../assets/images/empty-cart.jpg"

const CartContent = () => {
  const dispatch = useDispatch()
  const { cartItems } = useSelector((reduxData) => reduxData.cartReducer)
  const { openLoginDirector, outStockMessage, createOrderSuccess, openAddModal,
    createOrderForm, pending, displayError, errorMessage
  } = useSelector((reduxData) => reduxData.orderReducer)

  function uint8ArrayToString(uint8Array) {
    return Array.from(uint8Array)
      .map((byte) => String.fromCharCode(byte))
      .join("")
  }
  const transformVndToKvnd = (value) => {
    return `${value / 1000} K`
  }
  const onBtnPlusClick = (args) => {
    dispatch(plusItem(args))

  }
  const onBtnSubtractClick = (args) => {
    dispatch(subtractItem(args))
  }
  const onBtnRemoveClick = (args) => {
    dispatch(removeItem(args))
  }
  const onBtnCheckOutClick = () => {
    dispatch(openCreateOrderModal())
  }
  const onCloseModal = () => {
    dispatch(closeModal())
  }
  const onBtnMenuClick = () => {
    window.location.href = "/menu"
  }
  const onBtnLogInClick = () => {
    window.location.href = "/login"
  }
  const handleClose = () => {
    dispatch(closeAlert())
  }
  const onCloseModalOrderSuccess = () => {
    dispatch(closeModalOrderSuccess())
  }
  const initialValue = 0
  var subTotal = cartItems ? cartItems.reduce(
    (accumulator, currentValue) => accumulator + currentValue.buyPrice * currentValue.quantity,
    initialValue
  ) : 0
  var shippingFee
  shippingFee = 0 < subTotal && subTotal < 10000000 ? 300000 : 0
  return (
    <div className="cart-content flex flex-col">
      {cartItems && cartItems.length > 0 ?
        <>
          <div className="cart-products">
            <div className=" grid grid-cols-5 cart-title ">
              <div></div>
              <div className="cart-name ">Sản phẩm</div>
              <div className="cart-price ">Đơn giá</div>
              <div className="cart-quantity ">Số lượng</div>
              <div className="subtotal ">Thành tiền</div>
            </div>
            {cartItems && cartItems.length > 0
              ? cartItems.map((element, index) => {
                return (
                  <div key={index} className=" grid grid-cols-5 cart-items ">
                    <div className="cart-img">
                      <img src={`${process.env.REACT_APP_API_UPLOAD}image-${element.name}`} alt="cart-item" />
                    </div>
                    <div className="cart-name">
                      <p> {element.name}</p>
                    </div>
                    <div className="cart-price">{element.buyPrice.toLocaleString()} vnđ</div>
                    <div className="cart-quantity relative flex justify-center">
                      <div className="flex detail-btn-group">
                        <ButtonGroup variant="outlined" aria-label="Basic button group">
                          <Button onClick={() => onBtnSubtractClick(index)}>-</Button>
                          <Button>{element.quantity}</Button>
                          <Button onClick={() => onBtnPlusClick(index)}>+</Button>
                        </ButtonGroup>
                      </div>
                      <div className="div-cart-remove absolute">
                        <button className="cart-btn-remove" onClick={() => onBtnRemoveClick(index)}>
                          Xóa khỏi giỏ hàng
                        </button>
                      </div>
                    </div>
                    <div className="subtotal">
                      {(element.quantity * element.buyPrice).toLocaleString()} vnđ
                    </div>
                  </div>
                )
              })
              : []}
          </div>
          <div className="grid grid-cols-4 items-center justify-items-center div-cart-action">
            <div className="cart-btn-menu">
              <Button onClick={onBtnMenuClick} className="design-color-btn" variant="outline">
                Tiếp tục mua hàng
              </Button>
            </div>
            <div className="total flex flex-col col-span-2  ">
              <h2>Tổng đơn hàng</h2>
              <div className="grid grid-cols-2">
                <div className="">Giá đơn hàng:</div>
                <div>{cartItems && cartItems.length > 0 ? subTotal.toLocaleString() : 0} Vnđ</div>

                <div className="">Phí vận chuyển:</div>
                <div>{shippingFee > 0 ? shippingFee.toLocaleString() + " Vnđ" : 0}</div>
                <div className=" font-bold">Tổng tiền:</div>
                <div className="font-bold">{(subTotal + shippingFee).toLocaleString() + " Vnđ"}</div>
              </div>
            </div>
            <div className="cart-btn-checkout">
              <Button onClick={onBtnCheckOutClick} className="design-color-btn" variant="outline">
                Thanh toán
              </Button>
            </div>
          </div>
        </>
        : <div className="flex justify-center flex-col items-center">
          <img style={{ maxWidth: "40%" }} src={EmptyCart} />
          <p className="mb-5">Chưa có sản phẩm nào trong giỏ hàng của bạn</p>
          <Button onClick={onBtnMenuClick} className="design-color-btn" variant="outline">
                Tiếp tục mua hàng
              </Button>
        </div>}
      <DirModal
        close={onCloseModal}
        open={openLoginDirector}
        click={onBtnLogInClick}
        contentBtn="Đăng nhập"
        content="Bạn cần đăng nhập trước khi thanh toán"
      />
      <CreateOrderModal items={cartItems} open={openAddModal} createOrderForm={createOrderForm}
        pending={pending} displayError={displayError} errorMessage={errorMessage} disabled={true} />
      <Snackbar
        open={outStockMessage.length > 0 ? true : false}
        onClose={handleClose}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert onClose={handleClose} severity="warning" variant="filled" sx={{ width: "100%" }}>
          {outStockMessage.map((element, index) => {
            return element
          })}
          <br></br>Vui lòng điều chỉnh lại đơn hàng
        </Alert>
      </Snackbar>
      <ModalSuccess title="Tạo đơn hàng thành công" openModal={createOrderSuccess} close={onCloseModalOrderSuccess} content="Đóng" />
    </div>
  )
}
export default CartContent
