/* eslint-disable */
import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Route } from 'react-router-dom';

function AuthorizeRoute( { children } ) {
const user = sessionStorage.getItem("user") ? JSON.parse(sessionStorage.getItem("user")) : null
  
  if (!user ||user.role!=="admin") {
    return <Navigate to="/unauthorize" />;
  }

  return children;
}

export default AuthorizeRoute;
