/* eslint-disable */
import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Route } from 'react-router-dom';

function ProtectedRoute( { children } ) {
  const { user } = useSelector((reduxData) => reduxData.headerReducer)
  if (!user) {
    return <Navigate to="/login" />;
  }

  return children;
}

export default ProtectedRoute;
