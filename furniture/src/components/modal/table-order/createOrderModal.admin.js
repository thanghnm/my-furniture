/* eslint-disable */

import {
    Backdrop,
    Box,
    Button,
    ButtonGroup,
    CircularProgress,
    debounce,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Modal,
    OutlinedInput,
    Select,
    Step,
    StepLabel,
    Stepper,
    TextField,
    Typography,
} from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { provinces } from "./provinces"
import { changeNameCreateOrder } from "actions/admin/order.action"
import { changeEmailCreateOrder } from "actions/admin/order.action"
import { changePhoneCreateOrder } from "actions/admin/order.action"
import { changeAddressCreateOrder } from "actions/admin/order.action"
import { changeNoteCreateOrder } from "actions/admin/order.action"
import { throttle } from "lodash"
import React, { useCallback, useState } from "react"
import { changeCityCreateOrder } from "actions/admin/order.action"
import DatePickerComponent from "./DatePicker"
import { closeModalHandler } from "actions/admin/order.action"
import { createOrder } from "actions/admin/order.action"
import { element } from "prop-types"
import { changeMiniCartSelectCreateOrder } from "actions/admin/order.action"
import { plusItem } from "actions/admin/order.action"
import { subtractItem } from "actions/admin/order.action"
import { nextStep } from "actions/admin/order.action"
import { prevStep } from "actions/admin/order.action"
const steps = ['Nhập thông tin', 'Thêm hàng hóa'];
const CreateOrderAdminModal = (props) => {
    var province = provinces
    const dispatch = useDispatch()
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldFullNameCheckOutChange = (event) => {
        dispatch(changeNameCreateOrder(event.target.value))
    }
    const onTextFieldEmailCheckOutChange = (event) => {
        dispatch(changeEmailCreateOrder(event.target.value))
    }
    const onTextFieldPhoneCheckOutChange = (event) => {
        dispatch(changePhoneCreateOrder(event.target.value))
    }
    const onTextFieldAddressCheckOutChange = useCallback(
        (event) => {
            dispatch(changeAddressCreateOrder(event.target.value))
        },
        [dispatch]
    )
    const onTextFieldNoteCheckOutChange = (event) => {
        dispatch(changeNoteCreateOrder(event.target.value))
    }
    const onSelectCityCheckOutChange = (event) => {
        dispatch(changeCityCreateOrder(event.target.value))
    }
    const handleNext = () => {
        dispatch(nextStep())
    };

    const handleBack = () => {
        dispatch(prevStep())
    };

    const onSelectMenuChange = (event) => {
        const {
            target: { value },
        } = event;
        dispatch(changeMiniCartSelectCreateOrder(value))
       
    }
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };
    const onBtnPlusClick = (args) => {
        dispatch(plusItem(args))
    }
    const onBtnSubtractClick = (args) => {
        dispatch(subtractItem(args))
    }
    const dataForm ={
        name:props.name,
        phone:props.phone,
        email:props.email,
        address:props.address,
        city:props.city,
        shippedDate:props.shippedDate,
    }
    const onBtnCheckOutClick = (event) => {
        dispatch(createOrder(dataForm, props.selectedItems))
    }
    return (
        <>
            <Modal
                open={props.open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className="box-modal" textAlign={"center"}>
                    <Box sx={{ width: '100%' }}>
                        <Stepper activeStep={props.step} alternativeLabel >
                            {steps.map((label, index) => {
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step key={label} {...stepProps}>
                                        <StepLabel {...labelProps}>{label}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <React.Fragment>
                            {props.step == 0 ? <div className="grid grid-cols-2 div-modal-input mt-5">
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        disabled={props.disabled}
                                        required
                                        label="Tên"
                                        value={props.name}
                                        onChange={onTextFieldFullNameCheckOutChange}
                                    />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.name ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        required
                                        label="Email"
                                        disabled={props.disabled}
                                        value={props.email}
                                        onChange={onTextFieldEmailCheckOutChange}
                                    />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.email ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        required
                                        label="Số điện thoại"
                                        disabled={props.disabled}
                                        value={props.phone}
                                        onChange={onTextFieldPhoneCheckOutChange}
                                    />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.phone ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p></div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        label="Note"
                                        value={props.note}
                                        onChange={onTextFieldNoteCheckOutChange}
                                    />
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        label="Địa chỉ"
                                        value={props.address}
                                        onChange={onTextFieldAddressCheckOutChange}
                                    />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.address ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>

                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        onChange={onSelectCityCheckOutChange}
                                        value={props.city}
                                        className="text-field-modal"
                                        select
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        <option value="">Chọn thành phố</option>
                                        {province.map((element, index) => {
                                            return (
                                                <option key={index} value={element.name}>
                                                    {element.name}
                                                </option>
                                            )
                                        })}
                                    </TextField>
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.city ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <DatePickerComponent />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.shippedDate ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                            </div> :
                                <div className="col-order-modal flex flex-col">
                                    <FormControl style={{ height: 40, marginTop: "1rem" }} >
                                        <InputLabel id="demo-multiple-name-label">Thêm sản phẩm vào giỏ hàng</InputLabel>
                                        <Select
                                            labelId="demo-multiple-name-label"
                                            id="demo-multiple-name"
                                            multiple
                                            value={props.selectedItems}
                                            onChange={onSelectMenuChange}
                                            input={<OutlinedInput label="Thêm sản phẩm vào giỏ hàng" />}
                                            MenuProps={MenuProps}
                                        >
                                            {props.menu ? props.menu.map((element, index) => (
                                                <MenuItem
                                                    key={index}
                                                    value={element}
                                                >
                                                    {element.name}
                                                </MenuItem>
                                            )) : []}
                                        </Select>
                                    </FormControl>
                                    <div className=" text-base">
                                        {props.selectedItems ? props.selectedItems.map((element, index) => {
                                            const itemCost = (element.buyPrice * element.quantity).toLocaleString()
                                            return (
                                                <div className="grid grid-cols-3 mt-3 items-center" key={index}>
                                                    <p>
                                                        {element.name}
                                                    </p>
                                                    <div className="cart-quantity relative flex justify-center">
                                                        <div className="flex detail-btn-group">
                                                            <ButtonGroup variant="outlined" aria-label="Basic button group">
                                                                <Button onClick={() => onBtnSubtractClick(index)}>-</Button>
                                                                <Button>{element.quantity}</Button>
                                                                <Button onClick={() => onBtnPlusClick(index)}>+</Button>
                                                            </ButtonGroup>
                                                        </div>
                                                    </div>
                                                    <p>{itemCost} đ</p>
                                                </div>
                                            )
                                        }) : []}
                                    </div>
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.cart ? "block" : "none" }}
                                    >{props.errorMessage}
                                    </p>
                                </div>
                            }
                            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2, marginTop: "1rem" }}>
                                <Button
                                    color="inherit"
                                    disabled={props.step === 0}
                                    onClick={handleBack}
                                    sx={{ mr: 1 }}
                                >
                                    Back
                                </Button>
                                <Box sx={{ flex: '1 1 auto' }} />
                                {props.step == 0 ?
                                    <Button onClick={handleNext}>
                                        Next
                                    </Button> :
                                    <Button onClick={onBtnCheckOutClick}>
                                        Create Order
                                    </Button>
                                }

                            </Box>

                        </React.Fragment>
                    </Box>
                </Box>

            </Modal>
        </>
    )
}
export default CreateOrderAdminModal
