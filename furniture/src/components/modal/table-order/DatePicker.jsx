/* eslint-disable */
import * as React from "react"
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs"
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider"

import { useDispatch, useSelector } from "react-redux"
import dayjs from "dayjs"
import { DatePicker, DateTimePicker } from "@mui/x-date-pickers"
// import { editDatePicked } from '../../../action/table.action';
import { datePicked } from "actions/admin/order.action"

export default function DatePickerComponent() {
  //   const { openModalEditOrder } = useSelector((reduxData) => reduxData.tableReducers)

  const dispatch = useDispatch()
  const onDatePickerChange = (event) => {
    // if (openModalEditOrder === true) {
    //   dispatch(editDatePicked(event))
    // }
    // else {
    dispatch(datePicked(event))
    // }
  }
  const now = dayjs().$d
  return (
    <LocalizationProvider sx={{background:"red"}} dateAdapter={AdapterDayjs}>
      <DatePicker disablePast onChange={onDatePickerChange} label="Thời gian giao hàng" />
    </LocalizationProvider>
  )
}
