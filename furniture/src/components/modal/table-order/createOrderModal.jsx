/* eslint-disable */

import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  debounce,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Modal,
  OutlinedInput,
  Select,
  TextField,
  Typography,
} from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { provinces } from "./provinces"
import { changeNameCreateOrder } from "actions/admin/order.action"
import { changeEmailCreateOrder } from "actions/admin/order.action"
import { changePhoneCreateOrder } from "actions/admin/order.action"
import { changeAddressCreateOrder } from "actions/admin/order.action"
import { changeNoteCreateOrder } from "actions/admin/order.action"
import { throttle } from "lodash"
import React, { useCallback, useState } from "react"
import { changeCityCreateOrder } from "actions/admin/order.action"
import DatePickerComponent from "./DatePicker"
import { closeModalHandler } from "actions/admin/order.action"
import { createOrder } from "actions/admin/order.action"
const CreateOrderModal = (props) => {
  var province = provinces
  const dispatch = useDispatch()

  const handleClose = () => {
    dispatch(closeModalHandler())
  }
  const onTextFieldFullNameCheckOutChange = (event) => {
    dispatch(changeNameCreateOrder(event.target.value))
  }
  const onTextFieldEmailCheckOutChange = (event) => {
    dispatch(changeEmailCreateOrder(event.target.value))
  }
  const onTextFieldPhoneCheckOutChange = (event) => {
    dispatch(changePhoneCreateOrder(event.target.value))
  }
  const onTextFieldAddressCheckOutChange = useCallback(
    (event) => {
      dispatch(changeAddressCreateOrder(event.target.value))
    },
    [dispatch]
  )
  const onTextFieldNoteCheckOutChange = (event) => {
    dispatch(changeNoteCreateOrder(event.target.value))
  }
  const onSelectCityCheckOutChange = (event) => {
    dispatch(changeCityCreateOrder(event.target.value))
  }

  const onBtnCheckOutClick = (event) => {
    dispatch(createOrder(props.createOrderForm, props.items))
  }
  return (
    <>
      <Modal
        open={props.open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="box-modal" textAlign={"center"}>
          <Typography
            className="text-black"
            fontWeight={700}
            fontSize={30}
            marginBottom={2}
            id="modal-modal-description"
            sx={{ mt: 2 }}
          >
            Thanh toán
          </Typography>
          <div className="grid grid-cols-2 div-modal-input">
            <div className="col-order-modal flex flex-col">
              <TextField
                disabled={props.disabled}
                required
                label="Tên"
                value={props.createOrderForm.name}
                onChange={onTextFieldFullNameCheckOutChange}
              />
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.name ? "block" : "none" }}
              >
                {props.errorMessage}
              </p>
            </div>
            <div className="col-order-modal flex flex-col">
              <TextField
                required
                label="Email"
                disabled={props.disabled}
                value={props.createOrderForm.email}
                onChange={onTextFieldEmailCheckOutChange}
              />
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.email ? "block" : "none" }}
              >
                {props.errorMessage}
              </p>
            </div>
            <div className="col-order-modal flex flex-col">
              <TextField
                required
                label="Số điện thoại"
                disabled={props.disabled}
                value={props.createOrderForm.phone}
                onChange={onTextFieldPhoneCheckOutChange}
              />
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.phone ? "block" : "none" }}
              >
                {props.errorMessage}
              </p></div>
            <div className="col-order-modal flex flex-col">
              <TextField
                label="Note"
                value={props.createOrderForm.note}
                onChange={onTextFieldNoteCheckOutChange}
              />
            </div>
            <div className="col-order-modal flex flex-col">
              <TextField
                label="Địa chỉ"
                value={props.createOrderForm.address}
                onChange={onTextFieldAddressCheckOutChange}
              />
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.address ? "block" : "none" }}
              >
                {props.errorMessage}
              </p>
            </div>

            <div className="col-order-modal flex flex-col">
              <TextField
                onChange={onSelectCityCheckOutChange}
                value={props.createOrderForm.city}
                id="outlined-select-currency-native"
                className="text-field-modal"
                select
                SelectProps={{
                  native: true,
                }}
              >
                <option value="">Chọn thành phố</option>
                {province.map((element, index) => {
                  return (
                    <option key={index} value={element.name}>
                      {element.name}
                    </option>
                  )
                })}
              </TextField>
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.city ? "block" : "none" }}
              >
                {props.errorMessage}
              </p>
            </div>
            <div className="col-order-modal flex flex-col">
              <DatePickerComponent />
              <p
                className="invalid-textfield"
                style={{ display: props.displayError.shippedDate ? "block" : "none" }}
              >
                {props.errorMessage}
              </p>
            </div>
          </div>
          <p
            className="invalid-textfield"
            style={{ display: props.displayError.cart ? "block" : "none" }}
          >
            {props.errorMessage}
          </p>
          <Grid container marginTop={2}>
            <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
              <Button
                className="btn-close btn-modal-action"
                onClick={handleClose}
                style={{ marginRight: 10 }}
                variant="contained"
                color="error"
              >
                Close
              </Button>
              <Button
                className="btn-modal-action btn-success"
                onClick={onBtnCheckOutClick}
                variant="contained"
                color="success"
              >
                {props.pending ? <CircularProgress size={"1.5rem"} color="inherit" /> : "Xác nhận"}
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </>
  )
}
export default CreateOrderModal
