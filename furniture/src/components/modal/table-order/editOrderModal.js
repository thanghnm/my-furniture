/* eslint-disable */

import {
    Backdrop,
    Box,
    Button,
    ButtonGroup,
    CircularProgress,
    debounce,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Modal,
    OutlinedInput,
    Select,
    Step,
    StepLabel,
    Stepper,
    TextField,
    Typography,
} from "@mui/material"
import { provinces } from "./provinces"
import { useDispatch, useSelector } from "react-redux"
import React, { useCallback, useState } from "react"
import DatePickerComponent from "./DatePicker"
import { closeModalHandler } from "actions/admin/order.action"
import { createOrder } from "actions/admin/order.action"
import { changeMiniCartSelectCreateOrder } from "actions/admin/order.action"
import { plusItem } from "actions/admin/order.action"
import { subtractItem } from "actions/admin/order.action"
import { nextStep } from "actions/admin/order.action"
import { prevStep } from "actions/admin/order.action"
import { changeAddressEditOrder } from "actions/admin/order.action"
import { changeNoteEditOrder } from "actions/admin/order.action"
import { changeCityEditOrder } from "actions/admin/order.action"
import { changeStatusEditOrder } from "actions/admin/order.action"
import { plusItemEdit } from "actions/admin/order.action"
import { subtractItemEdit } from "actions/admin/order.action"
import { changeMiniCartSelectEditOrder } from "actions/admin/order.action"
import { editOrder } from "actions/admin/order.action"
const steps = ['Nhập thông tin', 'Thêm hàng hóa'];
const EditOrderModal = (props) => {
    var province = provinces
    const dispatch = useDispatch()
    const handleClose = () => {
        dispatch(closeModalHandler())
    }

    const onTextFieldAddressCheckOutChange = useCallback(
        (event) => {
            dispatch(changeAddressEditOrder(event.target.value))
        },
        [dispatch]
    )
    const onTextFieldNoteCheckOutChange = (event) => {
        dispatch(changeNoteEditOrder(event.target.value))
    }
    const onSelectCityCheckOutChange = (event) => {
        dispatch(changeCityEditOrder(event.target.value))
    }
    const onSelectStatusChange = (event) => {
        dispatch(changeStatusEditOrder(event.target.value))
    }
    const handleNext = () => {
        dispatch(nextStep())
    };

    const handleBack = () => {
        dispatch(prevStep())
    };

    const onSelectMenuChange = (event) => {
        const {
            target: { value },
        } = event;
        dispatch(changeMiniCartSelectEditOrder(value))
    }
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };
    const onBtnPlusClick = (args) => {
        dispatch(plusItemEdit(args))
    }
    const onBtnSubtractClick = (args) => {
        dispatch(subtractItemEdit(args))
    }
    const dataForm ={
        address:props.address,
        city:props.city,
        shippedDate:props.shippedDate,
        status:props.status,
        note:props.note,
        id:props.orderId
    }
    const onBtnEditOrderClick = (event) => {
        dispatch(editOrder(dataForm, props.details))
    }
    return (
        <>
            <Modal
                open={props.open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box className="box-modal" textAlign={"center"}>
                    <Box sx={{ width: '100%' }}>
                        <Stepper activeStep={props.step} alternativeLabel >
                            {steps.map((label, index) => {
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step key={label} {...stepProps}>
                                        <StepLabel {...labelProps}>{label}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <React.Fragment>
                            {props.step == 0 ? <div className="grid grid-cols-2 div-modal-input mt-5">
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        label="Note"
                                        value={props.note}
                                        onChange={onTextFieldNoteCheckOutChange}
                                    />
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        label="Địa chỉ"
                                        value={props.address}
                                        onChange={onTextFieldAddressCheckOutChange}
                                    />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.address ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>

                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        onChange={onSelectCityCheckOutChange}
                                        value={props.city}
                                        className="text-field-modal"
                                        select
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        <option value="">Chọn thành phố</option>
                                        {province.map((element, index) => {
                                            return (
                                                <option key={index} value={element.name}>
                                                    {element.name}
                                                </option>
                                            )
                                        })}
                                    </TextField>
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.city ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <TextField
                                        onChange={onSelectStatusChange}
                                        value={props.status}
                                        className="text-field-modal"
                                        select
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        <option value="">Trạng thái</option>
                                        <option value="Chưa xác nhận">Chưa xác nhận</option>
                                        <option value="Đã xác nhận">Đã xác nhận</option>
                                        <option value="Đang vận chuyển">Đang vận chuyển</option>
                                        <option value="Đã hoàn thành">Đã hoàn thành</option>
                                        
                                    </TextField>
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.city ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                                <div className="col-order-modal flex flex-col">
                                    <DatePickerComponent />
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.shippedDate ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                                </div>
                            </div> :
                                <div className="col-order-modal flex flex-col">
                                    <FormControl style={{ height: 40, marginTop: "1rem" }} >
                                        <InputLabel id="demo-multiple-name-label-edit">Thêm sản phẩm vào giỏ hàng</InputLabel>
                                        <Select
                                            labelId="demo-multiple-name-label-edit"
                                            id="demo-multiple-menu"
                                            multiple
                                            value={props.details}
                                            onChange={onSelectMenuChange}
                                            input={<OutlinedInput label="Thêm sản phẩm vào giỏ hàng" />}
                                            MenuProps={MenuProps}
                                        >
                                            {props.menu ? props.menu.map((element, index) => {

                                                return(<MenuItem
                                                    key={index}
                                                    value={element}
                                                >
                                                    {element.name}
                                                </MenuItem>)
                                            } ) : []}
                                        </Select>
                                    </FormControl>
                                    <div className=" text-base">
                                        {props.details ? props.details.map((element, index) => {
                                            const itemCost = (element.buyPrice * element.quantity).toLocaleString()
                                            return (
                                                <div className="grid grid-cols-3 mt-3 items-center" key={index}>
                                                    <p>
                                                        {element.name}
                                                    </p>
                                                    <div className="cart-quantity relative flex justify-center">
                                                        <div className="flex detail-btn-group">
                                                            <ButtonGroup variant="outlined" aria-label="Basic button group">
                                                                <Button onClick={() => onBtnSubtractClick(index)}>-</Button>
                                                                <Button>{element.quantity}</Button>
                                                                <Button onClick={() => onBtnPlusClick(index)}>+</Button>
                                                            </ButtonGroup>
                                                        </div>
                                                    </div>
                                                    <p>{itemCost} đ</p>
                                                </div>
                                            )
                                        }) : []}
                                    </div>
                                    <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.cart ? "block" : "none" }}
                                    >{props.errorMessage}
                                    </p>
                                </div>
                            }
                            <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2, marginTop: "1rem" }}>
                                <Button
                                    color="inherit"
                                    disabled={props.step === 0}
                                    onClick={handleBack}
                                    sx={{ mr: 1 }}
                                >
                                    Trở về
                                </Button>
                                <Box sx={{ flex: '1 1 auto' }} />
                                {props.step == 0 ?
                                    <Button onClick={handleNext}>
                                        Tiếp tục
                                    </Button> :
                                    <Button onClick={onBtnEditOrderClick}>
                                        Sửa đơn hàng
                                    </Button>
                                }

                            </Box>

                        </React.Fragment>
                    </Box>
                </Box>

            </Modal>
        </>
    )
}
export default EditOrderModal
