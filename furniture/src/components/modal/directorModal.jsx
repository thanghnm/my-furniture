// prettier-ignore
/* eslint-disable */
import { useDispatch } from "react-redux"
import warning from "../../assets/images/icons/modal/Alert-PNG-Picture.png"
import { Box, Button, Grid, Modal, Typography } from "@mui/material"
import WarningAmberIcon from "@mui/icons-material/WarningAmber"
export const DirModal = (props) => {
  const dispatch = useDispatch()

  const handleClose = () => {
    dispatch(closeModalHandler())
  }
  const style = {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "30%",
    bgcolor: "background.paper",
    border: "2px solid #000",
    borderRadius: 5,
    boxShadow: 24,
    p: 4,
    alignItems: "center",
  }
  return (
    <>
      <Modal
        open={props.open}
        onClose={props.close}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} textAlign={"center"}>
          <Typography
            fontWeight={700}
            fontSize={24}
            marginBottom={2}
            id="modal-modal-description"
            sx={{ mt: 2 }}
          >
            {props.title}
          </Typography>
          <img src={warning} style={{ width: 100 }} />
          <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
            {props.content}
          </Typography>
          <Grid container marginTop={1} justifyContent={"center"}>
            <Grid sx={{ textAlign: "center", marginTop: 2 }} item xs={12}>
              <Button className="submit-login" onClick={props.click} variant="contained">
                {props.contentBtn}
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </>
  )
}
