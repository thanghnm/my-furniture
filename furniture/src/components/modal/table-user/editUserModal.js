/* eslint-disable */

import {
    Box,
    Button,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    Modal,
    OutlinedInput,
    TextField,
    Typography,
} from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import React, { useCallback, useState } from "react"
import { closeModalHandler } from "actions/admin/order.action"
import CancelIcon from "@mui/icons-material/Cancel"
import VpnKeyIcon from "@mui/icons-material/VpnKey"
import PhoneIcon from "@mui/icons-material/Phone"
import EmailIcon from "@mui/icons-material/Email"
import PersonIcon from "@mui/icons-material/Person"
import BoltIcon from '@mui/icons-material/Bolt';
import { nameChangeEditAction } from "actions/admin/user.action"
import { emailChangeEditAction } from "actions/admin/user.action"
import { roleChangeEditAction } from "actions/admin/user.action"
import { editUser } from "actions/admin/user.action"
const EditUserModal = (props) => {
    const dispatch = useDispatch()
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldNameChange = useCallback((event) => {
        dispatch(nameChangeEditAction(event.target.value))
    },[])
    const onTextFieldEmailChange = useCallback((event) => {
        dispatch(emailChangeEditAction(event.target.value))
    },[])
    const onSelectRoleChange = (event) => {
        dispatch(roleChangeEditAction(event.target.value))
    }
    const onBtnConfirmClick = () => {
        const vDataForm = {
            name:props.name,
            email:props.email,
            role:props.role
        }
        dispatch(editUser(vDataForm,props.id))
    }
    return (
        <>
            <Modal
                open={props.open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ width: "50%", overflowY: "hidden" }} className="box-modal" textAlign={"center"}>
                    <Box sx={{
                        width: '100%', padding: "2rem", display: "flex",
                        flexDirection: "column", alignItems: "center", justifyContent: "center"
                    }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            SỬA NGƯỜI DÙNG
                        </Typography>
                        <CancelIcon onClick={props.close} className="close-modal-icon"></CancelIcon>
                        <div className="grid grid-cols-1 div-modal-input mt-5">
                            <div className=" flex items-center ">
                                <PersonIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        onChange={onTextFieldNameChange}
                                        placeholder="Tên người dùng"
                                        variant="outlined"
                                        value={props.name}
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.nameEdit ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center ">
                                <EmailIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        onChange={onTextFieldEmailChange}
                                        placeholder="Email"
                                        variant="outlined"
                                        value={props.email}
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.emailEdit ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center">
                                    <BoltIcon />
                                    <TextField sx={{ m: 1, width: "25ch" }} variant="outlined"
                                        onChange={onSelectRoleChange}
                                        value={props.role}
                                        id="outlined-select-currency-native"
                                        className="text-field-modal"
                                        select
                                        SelectProps={{
                                            native: true,
                                        }}
                                    >
                                        <option value="user">User</option>
                                        <option value="admin">Admin</option>
                                    </TextField>
                       
                                </div>
                            <div className="mt-3">
                                <button onClick={onBtnConfirmClick} className="btn-success">Xác nhận</button>
                            </div>
                        </div>
                    </Box>
                </Box>
            </Modal>
        </>
    )
}
export default EditUserModal
