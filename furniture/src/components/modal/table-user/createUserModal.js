/* eslint-disable */

import {
    Box,
    Button,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    Modal,
    OutlinedInput,
    TextField,
    Typography,
} from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import React, { useCallback, useState } from "react"
import { closeModalHandler } from "actions/admin/order.action"
import CancelIcon from "@mui/icons-material/Cancel"
import VpnKeyIcon from "@mui/icons-material/VpnKey"
import { Visibility, VisibilityOff } from '@mui/icons-material';
import PhoneIcon from "@mui/icons-material/Phone"
import EmailIcon from "@mui/icons-material/Email"
import PersonIcon from "@mui/icons-material/Person"
import { nameChangeAddAction } from "actions/admin/user.action"
import { emailChangeAddAction } from "actions/admin/user.action"
import { phoneChangeAddAction } from "actions/admin/user.action"
import { passwordChangeAddAction } from "actions/admin/user.action"
import { confirmChangeAddAction } from "actions/admin/user.action"
import { createUser } from "actions/admin/user.action"

const CreateUserModal = (props) => {
    const dispatch = useDispatch()
    const [showPassword, setShowPassword] = React.useState(false)

    const handleClickShowPassword = () => setShowPassword((show) => !show)
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldNameChange = useCallback((event) => {
        dispatch(nameChangeAddAction(event.target.value))
    },[])
    const onTextFieldEmailChange = useCallback((event) => {
        dispatch(emailChangeAddAction(event.target.value))
    },[])
    const onTextFieldPhoneChange =  useCallback((event) => {
        dispatch(phoneChangeAddAction(event.target.value))
    },[])
    const onTextFieldPasswordChange =  useCallback((event) => {
        dispatch(passwordChangeAddAction(event.target.value))
    },[])
    const onTextFieldConfirmChange = useCallback((event) => {
        dispatch(confirmChangeAddAction(event.target.value))
    },[])

    const onBtnConfirmClick = () => {
        const vDataForm = {
            name:props.name,
            email:props.email,
            phone:props.phone,
            password:props.password,
            confirmPassword:props.confirmPassword,
        }
        dispatch(createUser(vDataForm))
    }
    return (
        <>
            <Modal
                open={props.open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={{ width: "50%", overflowY: "hidden" }} className="box-modal" textAlign={"center"}>
                    <Box sx={{
                        width: '100%', padding: "2rem", display: "flex",
                        flexDirection: "column", alignItems: "center", justifyContent: "center"
                    }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            THÊM NGƯỜI DÙNG
                        </Typography>
                        <CancelIcon onClick={props.close} className="close-modal-icon"></CancelIcon>
                        <div className="grid grid-cols-1 div-modal-input mt-5">
                            <div className=" flex items-center ">
                                <PersonIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        onChange={onTextFieldNameChange}
                                        placeholder="Tên người dùng"
                                        variant="outlined"
                                        value={props.name}
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.name ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center ">
                                <EmailIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        onChange={onTextFieldEmailChange}
                                        placeholder="Email"
                                        variant="outlined"
                                        value={props.email}
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.email ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center ">
                                <PhoneIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        onChange={onTextFieldPhoneChange}
                                        placeholder="Số điện thoại"
                                        variant="outlined"
                                        value={props.phone}
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.phone ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center ">
                                <VpnKeyIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        placeholder="Mật khẩu"
                                        onChange={onTextFieldPasswordChange}
                                        id="outlined-signup-password"
                                        value={props.password}
                                        type={showPassword ? "text" : "password"}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.password ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className=" flex items-center ">
                                <VpnKeyIcon />
                                <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
                                    <OutlinedInput
                                        placeholder="Xác nhận mật khẩu"
                                        onChange={onTextFieldConfirmChange}
                                        value={props.confirmPassword}
                                        id="outlined-signup-confirm-password"
                                        type={showPassword ? "text" : "password"}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                            </div>
                            <p
                                        className="invalid-textfield"
                                        style={{ display: props.displayError.confirmPassword ? "block" : "none" }}
                                    >
                                        {props.errorMessage}
                                    </p>
                            <div className="mt-3">
                                <button onClick={onBtnConfirmClick} className="btn-success">Xác nhận</button>
                            </div>
                        </div>
                    </Box>
                </Box>
            </Modal>
        </>
    )
}
export default CreateUserModal
