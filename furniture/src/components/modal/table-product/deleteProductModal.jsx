// prettier-ignore
/* eslint-disable */
import { Box, Button, debounce, FormControl, Input, InputAdornment, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel"
import { useDispatch, useSelector } from "react-redux"
import { createProduct } from "actions/admin/product.action"
import React, { useCallback, useMemo } from "react";
import { deleteProduct } from "actions/admin/product.action";

const deleteProductModal = (props) => {
  const dispatch = useDispatch()
  const onBtnConfirmHandler = (e) => {
    dispatch(deleteProduct(props.id))
  }
  return (
    <Modal
      open={props.open}
      onClose={props.close}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      {/* <form> */}
      <Box className="box-modal">
        <Typography id="modal-modal-title" variant="h6" component="h2">
          XOÁ HÀNG HÓA
        </Typography>
        <CancelIcon onClick={props.close} className="close-modal-icon"></CancelIcon>
        <p className=" mt-4">Bạn có chắc chắn loại bỏ mặt hàng {props.name} không ?</p>
        <div className="div-btn-modal">
          <Button style={{background:"#f53c3c", color:"white"}} onClick={onBtnConfirmHandler}>
            Xoá sản phẩm
          </Button>
        </div>
      </Box>

      {/* </form> */}
    </Modal>
  )
}
export default React.memo(deleteProductModal)
