// prettier-ignore
/* eslint-disable */
import { Box, Button, debounce, FormControl, Input, InputAdornment, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel"
import { useDispatch, useSelector } from "react-redux"
import { changeNameAddProduct } from "actions/admin/product.action"
import { changeTypeAddProduct } from "actions/admin/product.action"
import { changePriceAddProduct } from "actions/admin/product.action"
import { changeBuyPriceAddProduct } from "actions/admin/product.action"
import { changeAmountAddProduct } from "actions/admin/product.action"
import { changeDescAddProduct } from "actions/admin/product.action"
import { changeImageAddProduct } from "actions/admin/product.action"
import { createProduct } from "actions/admin/product.action"
import React, { useCallback, useMemo } from "react";
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});
const AddProductModal = (props) => {
  const dispatch = useDispatch()
  const changeNameHandler = (event) => {
    dispatch(changeNameAddProduct(event))
  }
  const changeTypeHandler = (event) => {
    dispatch(changeTypeAddProduct(event))
  }
  const changePriceHandler = (event) => {
    dispatch(changePriceAddProduct(event))
  }
  const changeBuyPriceHandler = (event) => {
    dispatch(changeBuyPriceAddProduct(event))
  }
  const changeAmountHandler = (event) => {
    dispatch(changeAmountAddProduct(event))
  }
  const changeDescHandler = (event) => {
    dispatch(changeDescAddProduct(event))
  }
  const changeFileImageUrlHandler = (event) => {
    dispatch(changeImageAddProduct(event))
  }
 const addProductForm ={
  name: props.name,
  type: props.type,
  price: props.price,
  buyPrice: props.buyPrice,
  amount: props.amount,
  description: props.description,
  imageUrl: props.imageUrl,
 }
  const onBtnConfirmHandler = (e) => {
    dispatch(createProduct(addProductForm))
  }
  return (
    <Modal
      open={props.open}
      onClose={props.close}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      {/* <form> */}
      <Box className="box-modal">
        <Typography id="modal-modal-title" variant="h6" component="h2">
          THÊM HÀNG HÓA
        </Typography>
        <CancelIcon onClick={props.close} className="close-modal-icon"></CancelIcon>
        <div className="grid grid-cols-2 grid-modal-input gap-5 mt-5">
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changeNameHandler(event.target.value)}
              value={props.name}
              label="Tên sản phẩm"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.nameError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changeTypeHandler(event.target.value)}
              value={props.type}
              id="outlined-select-currency-native"
              className="text-field-modal"
              select
              SelectProps={{
                native: true,
              }}
            >
              <option value={""}>Chọn loại sản phẩm</option>
              {props.productType ? props.productType.map((items) => (
                <option key={items.name} value={items._id}>
                  {items.name}
                </option>
              ))
                : []}
            </TextField>
            <p
              className="invalid-textfield"
              style={{ display: props.typeError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changePriceHandler(event.target.value)}
              value={props.price}
              InputProps={{
                endAdornment: <InputAdornment position="end">vnđ</InputAdornment>,
              }}
              type="number"
              label="Giá tiền"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.priceError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changeBuyPriceHandler(event.target.value)}
              value={props.buyPrice}
              InputProps={{
                endAdornment: <InputAdornment position="end">vnđ</InputAdornment>,
              }}
              type="number"
              label="Giá khuyến mãi"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.buyPriceError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changeAmountHandler(event.target.value)}
              value={props.amount}
              type="number"
              label="Số lượng"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.amountError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(event) => changeDescHandler(event.target.value)}
              value={props.description}
              label="Mô tả"
              className="text-field-modal"
            />
          </div>
          <div className="flex flex-col col-span-2">
            <div className="flex">
            <Button
            style={{color:"white",minWidth:"max-content"}}
              component="label"
              role={undefined}
              variant="contained"
              tabIndex={-1}
              startIcon={<CloudUploadIcon />}
            >
              Upload file
              <VisuallyHiddenInput  type="file" name="productImg" onChange={changeFileImageUrlHandler} />
            </Button>
            <p style={{textOverflow:"hidden",overflow:"hidden"}} className="ml-3">{props.imageUrl?props.imageUrl.name:""}</p>
            </div>
            {/* <input type="file" name="productImg" onChange={changeFileImageUrlHandler}></input> */}
            <p
              className="invalid-textfield"
              style={{ display: props.imageUrlError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
        </div>
        <div className="div-btn-modal">
          <Button className="btn-confirm-modal" onClick={onBtnConfirmHandler}>
            Lưu sản phẩm
          </Button>
        </div>

      </Box>
      {/* </form> */}
    </Modal>
  )
}
export default React.memo(AddProductModal)
