// prettier-ignore
/* eslint-disable */
import { Box, Button, debounce, FormControl, Input, InputAdornment, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel"
import { useDispatch, useSelector } from "react-redux"

import { changeNameEditProduct } from "actions/admin/product.action";
import { changeTypeEditProduct } from "actions/admin/product.action";
import { changePriceEditProduct } from "actions/admin/product.action";
import { changeBuyPriceEditProduct } from "actions/admin/product.action";
import { changeAmountEditProduct } from "actions/admin/product.action";
import { changeDescEditProduct } from "actions/admin/product.action";
import { changeImageEditProduct } from "actions/admin/product.action";
import React, { useCallback, useMemo } from "react";
import { styled } from '@mui/material/styles';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { editProduct } from "actions/admin/product.action";
const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});
const EditProductModal = (props) => {
  const dispatch = useDispatch()
  const changeHandler = useCallback((actionCreator, event) => {
    dispatch(actionCreator(event.target.value));
  }, [dispatch]);
  // const changeNameHandler = useCallback(debounce((event) => {
  //   dispatch(changeNameEditProduct(event))
  // },300),[])
  // const changeTypeHandler = useCallback(debounce((event) => {
  //   dispatch(changeTypeEditProduct(event))
  // },300),[])
  // const changePriceHandler = useCallback(debounce((event) => {
  //   dispatch(changePriceEditProduct(event))
  // },300),[])
  // const changeBuyPriceHandler = useCallback(debounce((event) => {
  //   dispatch(changeBuyPriceEditProduct(event))
  // },300),[])
  // const changeAmountHandler =useCallback(debounce((event) => {
  //   dispatch(changeAmountEditProduct(event))
  // },300),[])
  // const changeDescHandler = useCallback(debounce((event) => {
  //   dispatch(changeDescEditProduct(event))
  // },300),[])
  const changeFileImageUrlHandler = (event) => {
    dispatch(changeImageEditProduct(event))
  }
  const editProductForm ={
    name: props.name,
    type: props.type,
    price: props.price,
    buyPrice: props.buyPrice,
    amount: props.amount,
    description: props.description,
    imageUrl: props.imageUrl,
   }
  const onBtnConfirmHandler = (e) => {
    dispatch(editProduct(editProductForm,props.id))
  }
  return (
    <Modal
      open={Boolean(props.open)}
      onClose={props.close}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      {/* <form> */}
      <Box className="box-modal">
        <Typography id="modal-modal-title" variant="h6" component="h2">
          SỬA HÀNG HÓA
        </Typography>
        <CancelIcon onClick={props.close} className="close-modal-icon"></CancelIcon>
        <div className="grid grid-cols-2 grid-modal-input gap-5 mt-5">
          <div className="flex flex-col">
            <TextField
            disabled={props.disabled}
              onChange={(e)=>changeHandler(changeNameEditProduct,e)}
              value={props.name}
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.nameError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
            disabled={props.disabled}
              onChange={(e)=>changeHandler(changeTypeEditProduct,e)}
              value={props.type}
              id="outlined-select-currency-native"
              className="text-field-modal"
              select
              SelectProps={{
                native: true,
              }}
            >
              <option value={""}>Chọn loại sản phẩm</option>
              {props.productType? props.productType.map((items) => (
                    <option key={items.name} value={items._id}>
                      {items.name}
                    </option>
                  ))
               :[]}
            </TextField>
            <p
              className="invalid-textfield"
              style={{ display: props.typeError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
              onChange={(e)=>changeHandler(changePriceEditProduct,e)}
              value={props.price}
              InputProps={{
                endAdornment: <InputAdornment position="end">vnđ</InputAdornment>,
              }}
              type="number"
              label="Giá tiền"
              className="text-field-modal"
            disabled={props.disabled}

            />
            <p
              className="invalid-textfield"
              style={{ display: props.priceError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
            disabled={props.disabled}
            onChange={(e)=>changeHandler(changeBuyPriceEditProduct,e)}
              value={props.buyPrice}
              InputProps={{
                endAdornment: <InputAdornment position="end">vnđ</InputAdornment>,
              }}
              type="number"
              label="Giá khuyến mãi"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.buyPriceError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
            disabled={props.disabled}
            onChange={(e)=>changeHandler(changeAmountEditProduct,e)}
              value={props.amount}
              type="number"
              label="Số lượng"
              className="text-field-modal"
            />
            <p
              className="invalid-textfield"
              style={{ display: props.amountError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
          <div className="flex flex-col">
            <TextField
            disabled={props.disabled}
            onChange={(e)=>changeHandler(changeDescEditProduct,e)}
              value={props.description}
              label="Mô tả"
              className="text-field-modal"
            />
          </div>
          <div className="flex flex-col col-span-2">
          <div className="flex">
            <Button
            style={{color:"white"}}
              component="label"
              role={undefined}
              variant="contained"
              tabIndex={-1}
              startIcon={<CloudUploadIcon />}
            >
              Upload file
              <VisuallyHiddenInput  type="file" name="productImg" onChange={changeFileImageUrlHandler} />
            </Button>
            <p className="ml-3">{props.imageUrl.name}</p>
            </div>
            <p
              className="invalid-textfield"
              style={{ display: props.imageUrlError ? "block" : "none" }}
            >
              {props.errorMessage}
            </p>
          </div>
        </div>
        <div className="div-btn-modal">
          <Button className="btn-confirm-modal" 
          onClick={onBtnConfirmHandler}
          >
            Sửa sản phẩm
          </Button>
        </div>

      </Box>
      {/* </form> */}
    </Modal>
  )
}
export default EditProductModal
