/* eslint-disable */

import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material"
import { inputAmountFilter } from "actions/admin/product.action";
import { fetchProduct } from "actions/admin/product.action";
import { pageChangePagination } from "actions/admin/product.action";
import { inputNameFilter } from "actions/admin/product.action";
import React from "react";
import { useDispatch } from "react-redux";

const AdminFilter = (props) => {
    const dispatch = useDispatch()
const onTextFieldNameChangeHandler =(event)=>{
    dispatch(inputNameFilter(event.target.value))
}    
const onTextFieldAmountChangeHandler =(event)=>{
    dispatch(inputAmountFilter(event.target.value))
} 
const filter = {
    name:props.name,
    amount:props.amount
}
const onBtnSearchClickHandler =()=>{
    dispatch(pageChangePagination(1))
    dispatch(fetchProduct(10, 1,filter))
}  
return (
        <div className="div-admin-filter grid grid-cols-3 gap-2 ">
           <TextField onChange={onTextFieldNameChangeHandler} color="warning" value={props.name}  label="Lọc theo tên" variant="outlined" />
           <TextField onChange={onTextFieldAmountChangeHandler} value={props.amount}  label="Lọc theo số lượng" variant="outlined" />
            <Button onClick={onBtnSearchClickHandler} className="btn-search">Tìm kiếm</Button>
        </div>
    )
}
export default AdminFilter