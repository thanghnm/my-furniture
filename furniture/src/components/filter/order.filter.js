/* eslint-disable */

import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material"
import { fetchOrder } from "actions/admin/order.action";
import { inputStatusFilter } from "actions/admin/order.action";
import { pageChangePagination } from "actions/admin/order.action";
import React from "react";
import { useDispatch } from "react-redux";
import { inputCodeFilter } from "actions/admin/order.action";
const OrderFilter = (props) => {
    const dispatch = useDispatch()
    const onTextFieldCodeChangeHandler = (event) => {
        dispatch(inputCodeFilter(event.target.value))
    }
    const onTextFieldStatusChangeHandler = (event) => {
        dispatch(inputStatusFilter(event.target.value))
    }
    const filter = {
        code: props.code,
        status: props.status
    }
    const onBtnSearchClickHandler = () => {
        dispatch(pageChangePagination(1))
        dispatch(fetchOrder(10, 1, filter))
    }
    return (
        <div className="div-admin-filter grid grid-cols-3 gap-2 ">
            <TextField onChange={onTextFieldCodeChangeHandler} color="warning" value={props.code} label="Lọc theo mã đơn" variant="outlined" />
            <TextField
                onChange={onTextFieldStatusChangeHandler}
                value={props.status}
                id="outlined-select-currency-native"
                className="text-field-modal"
                select
                SelectProps={{
                    native: true,
                }}
            >
                <option value="">Trạng thái</option>
                <option value="Chưa xác nhận">Chưa xác nhận</option>
                <option value="Đã xác nhận">Đã xác nhận</option>
                <option value="Đang vận chuyển">Đang vận chuyển</option>
                <option value="Đã hoàn thành">Đã hoàn thành</option>
            </TextField>
            <Button onClick={onBtnSearchClickHandler} className="btn-search">Tìm kiếm</Button>
        </div>
    )
}
export default OrderFilter