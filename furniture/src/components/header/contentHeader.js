// prettier-ignore
/* eslint-disable */
import HomeIcon from "../../assets/images/icons/homeicon.png"
import {
  Avatar,
  Badge,
  Button,
  Chip,
  Divider,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  TextField,
  Tooltip,
} from "@mui/material"
import TravelExploreIcon from "@mui/icons-material/TravelExplore"
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart"
import SearchIcon from "@mui/icons-material/Search"
import PersonIcon from "@mui/icons-material/Person"
import React from "react"
import { Logout, PersonAdd, Settings } from "@mui/icons-material"
import CartDrawer from "components/drawer/drawer"
import { useDispatch, useSelector } from "react-redux"
import { openCart } from "actions/header.action"
import { closeCart } from "actions/header.action"
import MenuIcon from "@mui/icons-material/Menu"
import { logOutHandler } from "actions/header.action"
const ContentHeader = () => {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const dispatch = useDispatch()
  const { openCartDrawer,user } = useSelector((reduxData) => reduxData.headerReducer)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const onBtnCartClick = () => {
    dispatch(openCart())
  }
  const onCloseDrawer = () => {
    dispatch(closeCart())
  }
  const goToProfile =()=>{
    window.location.href="/profile"
  }
  const logOut =()=>{
    dispatch(logOutHandler())
  }
  const cartItems =localStorage.getItem("cartItems")? JSON.parse(localStorage.getItem("cartItems")):[]
  return (
    <div className="flex items-center justify-center header-float bg-white h-16 ">
      <div className="header-inner w-full grid grid-cols-7">
        <div className="col-span-2 brand-icon flex flex-row items-center justify-center">
          <img className="branch-logo" src={HomeIcon}></img>
          <a href="/" className="branch-name">
            My Furniture
          </a>
        </div>
        <div className="col-span-3 flex justify-evenly header-nav">
          <a href="/" className="header-director">
            Trang chủ
          </a>
          <a href="/menu" className="header-director">
            Menu
          </a>
          <a href="#" className="header-director">
            Về chúng tôi
          </a>
          <a href="#" className="header-director">
            Liên hệ
          </a>
        </div>
        <div className="user col-span-2 flex items-center justify-center">
          {/* neu da dang nhap hien thi user , hoac hien thi nut log in */}
          {user ? (
            <Chip
              variant="outlined"
              avatar={
                <Avatar sx={{ background: "#fcb900" }}>
                  
                 {user.avatar==null?<PersonIcon/>:<img src={`${process.env.REACT_APP_API_AVATAR}avatar-${user.phone}`} />} 
                </Avatar>
              }
              label={user.name}
              onClick={handleClick}
            ></Chip>
          ) : (
            <Button href="/login" className="btn-login">
              Đăng nhập
            </Button>
          )}
          <Menu
            id="demo-positioned-menu"
            aria-labelledby="demo-positioned-button"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "left",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left",
            }}
          >
            <MenuItem onClick={goToProfile} >
              <PersonIcon /> Profile
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <TravelExploreIcon /> My account
            </MenuItem>
            <MenuItem onClick={logOut}>
              <Logout /> Logout
            </MenuItem>
          </Menu>
          <Badge badgeContent={cartItems ? cartItems.length : 0} color="error">
            <ShoppingCartIcon
              onClick={onBtnCartClick}
              sx={{ color: "#fcb900" }}
              fontSize="large"
              className="ml-5 cursor-pointer"
            />
          </Badge>
          <SearchIcon sx={{ color: "#fcb900" }} fontSize="large" className="ml-5 cursor-pointer" />
        </div>
      </div>
      <CartDrawer close={onCloseDrawer} open={openCartDrawer} cartItems={cartItems} />
    </div>
  )
}
export default ContentHeader
