/* eslint-disable */

import { CircularProgress, Pagination } from "@mui/material"
import CardItem from "./card"
import Filter from "./filter"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { fetchProduct } from "actions/menu.action"
import { fetchProductType } from "actions/menu.action"
import { changePriceFilter } from "actions/menu.action"
import { changeTypeFilter } from "actions/menu.action"
import { changeNameFilter } from "actions/menu.action"
import { pageChangePagination } from "actions/menu.action"
const MenuContent = () => {
  const dispatch = useDispatch()
  const { filter, limit, page, dataProduct, productType, noPage, pending } = useSelector(
    (reduxData) => reduxData.menuReducer
  )
  useEffect(() => {
    dispatch(fetchProduct(limit, 1, filter))
  }, [])
  useEffect(() => {
    dispatch(fetchProductType())
  }, [])
  const onChangePagination = (event, value) => {
    dispatch(pageChangePagination(value))
    dispatch(fetchProduct(limit, value, filter))
  }
  const onBtnFilterClick = () => {
    dispatch(pageChangePagination(1))
    dispatch(fetchProduct(limit, 1, filter))
  }
  return (
    <div className="menu">
      <Filter filter={filter} productType={productType} filterHandle={onBtnFilterClick} />
      <div className="grid grid-cols-4 gap-5 mt-5 card-group">
        {!pending ? (
          dataProduct.map((element, index) => {
            return (
              <CardItem
                key={index}
                image={element.imageUrl}
                name={element.name}
                price={element.price}
                buyPrice={element.buyPrice}
                id={element._id}
                info={element}
              />
            )
          })
        ) : (
          <CircularProgress color="warning" />
        )}
      </div>
      {dataProduct.length > 0 ? (
        <Pagination
          className="mt-5"
          onChange={onChangePagination}
          count={noPage}
          page={page}
          variant="outlined"
          shape="rounded"
        />
      ) : (
        []
      )}
    </div>
  )
}
export default MenuContent
