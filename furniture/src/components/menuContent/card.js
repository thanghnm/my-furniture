// prettier-ignore
/* eslint-disable */
import { Alert, Button, Snackbar } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { ADD_TO_CART } from "../../constants/menu.constants"
import { addCart } from "actions/menu.action"
import React from "react"

const CardItem = (props) => {
  const [open, setOpen] = React.useState(false)

  const dispatch = useDispatch()
  const onBtnAddCartClick = (event) => {
    setOpen(true)
    dispatch(addCart(props.info))
  }
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return
    }

    setOpen(false)
  }
  return (
    <div className="card-item">
      <div className="relative card-img-div flex justify-center">
        <img className="card-img w-full" src={`${process.env.REACT_APP_API_UPLOAD}/image-${props.name}`}alt="item" />
        <Button onClick={onBtnAddCartClick} className="add-cart">
          Add to cart
        </Button>
        <Snackbar
          open={open}
          autoHideDuration={5000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <Alert onClose={handleClose} severity="success" variant="filled" sx={{ width: "100%" }}>
            Thêm vào giỏ hàng thành công
          </Alert>
        </Snackbar>
      </div>
      <div className="p-3">
        <a href={"/menu/" + props.id} className="card-description">
          {props.name}
        </a>
        <div className="flex justify-between mt-3">
          <div>
            <p className="promotion-price">
              {new Intl.NumberFormat("vi-VN", {
                style: "currency",
                currency: "VND",
              }).format(props.buyPrice)}
            </p>
            <p className="price">
              {new Intl.NumberFormat("vi-VN", {
                style: "currency",
                currency: "VND",
              }).format(props.price)}
            </p>
          </div>
          <div className="w-1/4 percent-discount flex items-center justify-center">
            - {(((props.price - props.buyPrice) / props.price) * 100).toFixed(0)}%
          </div>
        </div>
      </div>
    </div>
  )
}
export default CardItem
