// prettier-ignore
/* eslint-disable */

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  Input,
  InputLabel,
  OutlinedInput,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material"
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown"
import { CheckBox } from "@mui/icons-material"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { changePriceFilter } from "actions/menu.action"
import { fetchProductType } from "actions/menu.action"
import { changeTypeFilter } from "actions/menu.action"
import { changeNameFilter } from "actions/menu.action"
const Filter = (props) => {
  const dispatch = useDispatch()
  const onFilterPriceChange = (event) => {
    dispatch(changePriceFilter(event))
  }
  const onFilterTypeChange = (event) => {
    dispatch(changeTypeFilter(event))
  }
  const onFilterNameChange = (event) => {
    dispatch(changeNameFilter(event))
  }
   const priceArr = [{value:0,label:"Dưới 10M vnđ"},{value:10,label:"Từ 10M - 20M vnđ"},
    {value:20,label:"Từ 20M - 30M vnđ"},{value:30,label:"Từ 30M - 40M vnđ"},{value:40,label:"Trên 40M vnđ"},
   ]
  return (
    <div className=" gap-4 mt-5 filter w-full">
      <TextField
        onChange={onFilterPriceChange}
        value={props.filter.price}
        className="text-field-modal grow"
        select
        SelectProps={{
          native: true,
        }}
      >
        <option value="">Khoảng giá</option>
        {priceArr.map((element, index) => {
          return (
            <option key={index} value={element.value}>
              {element.label}
            </option>
          )
        })}
      </TextField>
      <TextField
        onChange={onFilterTypeChange}
        value={props.filter.type}
        className="text-field-modal grow"
        select
        SelectProps={{
          native: true,
        }}
      >
        <option value="">Sản phẩm</option>
        {props.productType? props.productType.map((element, index) => {
          return (
            <option key={index} value={element._id}>
              {element.name}
            </option>
          )
        }):[]}
      </TextField>
      <FormControl className="grow">
        <OutlinedInput
          onChange={onFilterNameChange}
          placeholder="Nhập tên sản phẩm để tìm kiếm"
          className="input-filter-name"
        />
      </FormControl>
      <Button
        onClick={props.filterHandle}
        className="grow-0 btn-search"
        variant="contained"
        color="primary"
      >
        Filter
      </Button>
    </div>
  )
}
export default Filter
