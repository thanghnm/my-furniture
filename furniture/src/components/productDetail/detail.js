/* eslint-disable */
import CircleIcon from "@mui/icons-material/Circle"
import CheckCircleIcon from "@mui/icons-material/CheckCircle"
import { useDispatch, useSelector } from "react-redux"
import { chooseColor } from "actions/detail.action"
import { chooseSize } from "actions/detail.action"
import { Box, Button, ButtonGroup, CircularProgress, Tab, Tabs } from "@mui/material"
import { plusItem } from "actions/detail.action"
import { subtractItem } from "actions/detail.action"
import { changeTab } from "actions/detail.action"
import CardItem from "components/menuContent/card"
import { useEffect } from "react"
import { fetchProductInfo } from "actions/detail.action"
import { fetchRelateProduct } from "actions/detail.action"
import { addCart } from "actions/menu.action"

const Detail = () => {
  const { color, size, quantity, tab, detail, pending, relate } = useSelector(
    (reduxData) => reduxData.detailReducer
  )
  const productId = window.location.pathname.split("/menu/")[1]
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchProductInfo(productId))
  }, [])

  const selectColorHandler = (args) => {
    dispatch(chooseColor(args))
  }
  const selectSizeHandler = (args) => {
    dispatch(chooseSize(args))
  }
  const onBtnPlusClick = (event) => {
    dispatch(plusItem(event))
  }
  const onBtnSubtractClick = (event) => {
    dispatch(subtractItem(event))
  }
  const onDescTabChange = (event, value) => {
    dispatch(changeTab(value))
  }

  const onBtnAddCartClick = (event) => {
    dispatch(addCart(detail, quantity))
  }
  return (
    <div>
      {detail ? (
        <div>
          <div className="flex p-8 detail">
            <div className="w-7/12 px-4 left ">
              <img
                src={`${process.env.REACT_APP_API_UPLOAD}/image-${detail.name}`}
                className="detail-image"
                alt="info"
              />
            </div>
            <div className="w-5/12 px-4 right">
              <h3>{detail.name}</h3>
              <p className="detail-price">
                {detail.buyPrice.toLocaleString()} vnđ
                <span>{detail.price.toLocaleString()} vnđ</span>
              </p>
              <p>
                Setting the bar as one of the loudest speakers in its class, the Kilburn is a
                compact, stout-hearted hero with a well-balanced audio which boasts a clear midrange
                and extended highs for a sound.
              </p>
              <p className="detail-option">Size</p>
              <div className="flex">
                <div
                  onClick={() => selectSizeHandler(1)}
                  className={size == 1 ? "size-btn-active" : "size-btn"}
                >
                  L
                </div>
                <div
                  onClick={() => selectSizeHandler(2)}
                  className={size == 2 ? "size-btn-active" : "size-btn"}
                >
                  XL
                </div>
                <div
                  onClick={() => selectSizeHandler(3)}
                  className={size == 3 ? "size-btn-active" : "size-btn"}
                >
                  XS
                </div>
              </div>
              <p className="detail-option">Color</p>
              <div className="flex">
                <div onClick={() => selectColorHandler(1)} className="color-btn cursor-pointer">
                  {color == 1 ? (
                    <CheckCircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "red" }}
                    ></CheckCircleIcon>
                  ) : (
                    <CircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "red" }}
                    ></CircleIcon>
                  )}
                </div>
                <div onClick={() => selectColorHandler(2)} className="color-btn cursor-pointer">
                  {color == 2 ? (
                    <CheckCircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "blue" }}
                    ></CheckCircleIcon>
                  ) : (
                    <CircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "blue" }}
                    ></CircleIcon>
                  )}
                </div>
                <div onClick={() => selectColorHandler(3)} className="color-btn cursor-pointer">
                  {color == 3 ? (
                    <CheckCircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "brown" }}
                    ></CheckCircleIcon>
                  ) : (
                    <CircleIcon
                      fontSize="medium"
                      className="color-icon"
                      sx={{ color: "brown" }}
                    ></CircleIcon>
                  )}
                </div>
              </div>
              <p className="detail-option">Quantity</p>
              <div className="flex detail-btn-group">
                <ButtonGroup variant="outlined" aria-label="Basic button group">
                  <Button onClick={onBtnSubtractClick}>-</Button>
                  <Button>{quantity}</Button>
                  <Button onClick={onBtnPlusClick}>+</Button>
                </ButtonGroup>
                <Button onClick={onBtnAddCartClick} className="btn-add-cart-detail">
                  Thêm vào giỏ
                </Button>
              </div>
            </div>
          </div>
          <div className="detail-desc p-8 flex flex-col items-center">
            <Box sx={{ width: "60%" }}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <Tabs value={tab} onChange={onDescTabChange} aria-label="basic tabs example">
                  <Tab label="Mô tả" />
                  <Tab label="Thông tin" />
                  <Tab label="Đánh giá" />
                </Tabs>
              </Box>
              {tab == 0 ? (
                <p
                  style={{
                    opacity: tab == 0 ? 1 : 0,
                    display: tab == 0 ? "block" : "none",
                  }}
                  className="tab-context "
                  value={tab}
                  index={0}
                >
                  Với lối thiết kế sử dụng những đường cong mềm mại và hình học đơn giản, sự kết hợp
                  hoàn hả giữa tự nhiên và hiện tại, pha chút táo bạo khi sử dụng những màu sắc tự
                  nhiên nhưng sáng rực, đặc biệt là tập trung vào tính thực tiễn, đảm bảo sự thoải
                  mái tối đa đã tạo nên những thiết kế vô cùng độc đáo, xuất sắc và mang đậm phong
                  cách của những thập niên cũ.
                  <br></br>
                  <br></br>
                  Đây chắc chắn sẽ là những món đồ nội thất đón đầu xu hướng thiết kế của năm 2024
                  và của tương lai. Là một sự lựa chọn hoàn hảo cho những ai yêu thích phong
                  cáchthiết kế hiện. Nội thất không chỉ đơn thuần là "đồ nội thất", nó được coi là
                  tác phẩm nghệ thuật có giátrị và càng trở nên tốt hơn theo thời gian.
                </p>
              ) : tab == 1 ? (
                <p
                  style={{
                    opacity: tab == 1 ? 1 : 0,
                    display: tab == 1 ? "block" : "none",
                  }}
                  className="tab-context"
                  value={tab}
                  index={0}
                >
                  Khung gỗ: Sofa Lango được làm từ khung gỗ cao su với kết cấu chắc chắn, chịu lực
                  tốt và độ bền cao. Đặc biệt, nguyên liệu gỗ đã được xử lý chống mối mọt, giúp hạn
                  chế khả năng bị cong vênh và biến dạng trong quá trình sử dụng
                  <br></br>
                  <br></br>
                  Đệm mút: sản phẩm được bọc bởi lớp đệm mút dày, độ đàn hồi cao, cùng thiết kế hiện
                  đại mang lại cảm giác thoải mái dù giữ nguyên tư thế sử dụng trong 1 thời gian dài
                  <br></br>
                  <br></br>
                  Vỏ bọc: được làm bằng chất liệu Polyester Canvas có ưu điểm mềm mịn, thoáng mát,
                  độ bền cao, dễ dàng vệ sinh, chống nhăn và nấm mốc, bụi bẩn
                  <br></br>
                  <br></br>
                  Chân: được làm bằng gỗ cao su chắc chắn, bo nhẹ các góc cạnh tinh tế, khung xương
                  chắc chắn mang lại sự cân bằng và vững chãi cho toàn bộ sản phẩm đặc biệt có thêm
                  1 chân phụ ở giữa, hỗ trợ chịu lực giúp tránh trường hợp cong vênh sau thời gian
                  dài sử dụng. Khoảng cách từ mặt nệm đến mặt sàn nhà lên tới 17cm, phù hợp với mọi
                  loại máy hút bụi và rất dễ dàng vệ sinh
                </p>
              ) : (
                <p
                  style={{ opacity: tab == 2 ? 1 : 0 }}
                  className="tab-context"
                  value={tab}
                  index={0}
                >
                  Chức năng này chưa hoàn thiện
                </p>
              )}
            </Box>
          </div>
        </div>
      ) : (
        <CircularProgress color="warning" />
      )}
      <div className="detail-recommend p-8">
        <h2>Đề xuất cho bạn</h2>
        <div className="grid-cols-4 grid gap-4">
          {relate ? (
            relate.map((element, index) => {
              return (
                <CardItem
                  key={index}
                  image={element.imageUrl}
                  name={element.name}
                  price={element.price}
                  buyPrice={element.buyPrice}
                />
              )
            })
          ) : (
            <CircularProgress color="warning" />
          )}
        </div>
      </div>
    </div>
  )
}
export default Detail
