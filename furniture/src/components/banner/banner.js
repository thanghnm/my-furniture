import HomeIcon from "../../assets/images/icons/homeicon.png"
const Banner = (props) => {
  return (
    <div className="banner flex flex-col justify-center items-center">
      <img className="branch-logo" src={HomeIcon} alt="banner" />
      <h2>{props.content}</h2>
    </div>
  )
}

export default Banner
