import { Breadcrumbs, Link, Typography } from "@mui/material"
import NavigateNextIcon from "@mui/icons-material/NavigateNext"
import { PropTypes } from "prop-types"
const MyBreadcrumb = (props) => {
  const breadcrumbs = []
  return (
    <div className=" breadcrumb">
      <Breadcrumbs separator="›" aria-label="breadcrumb">
        <Link underline="hover" key="1" color="inherit" href="/">
          Home
        </Link>
        ,
        {props.parentThisPage ? (
          <Link underline="hover" key="2" color="inherit" href={props.hrefParent}>
            {props.parentThisPage}
          </Link>
        ) : (
          []
        )}
        {
          <Typography key="3" color="text.primary">
            {props.thisPage}
          </Typography>
        }
      </Breadcrumbs>
    </div>
  )
}
// MyBreadcrumb.PropTypes = {
//   thisPage: PropTypes.string.isRequire,
//   parentThisPage:PropTypes.string
// }
export default MyBreadcrumb
