/* eslint-disable */

import * as React from "react"
import Box from "@mui/material/Box"
import Drawer from "@mui/material/Drawer"
import Button from "@mui/material/Button"
import List from "@mui/material/List"
import Divider from "@mui/material/Divider"
import ListItem from "@mui/material/ListItem"
import ListItemButton from "@mui/material/ListItemButton"
import ListItemIcon from "@mui/material/ListItemIcon"
import ListItemText from "@mui/material/ListItemText"
import InboxIcon from "@mui/icons-material/MoveToInbox"
import MailIcon from "@mui/icons-material/Mail"
import { ButtonGroup } from "@mui/material"
import { useDispatch } from "react-redux"
import { plusItem } from "actions/detail.action"
import { subtractItem } from "actions/detail.action"
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline"
import { removeItem } from "actions/cart.action"
export default function CartDrawer(props) {
  const dispatch = useDispatch()
  function uint8ArrayToString(uint8Array) {
    return Array.from(uint8Array)
      .map((byte) => String.fromCharCode(byte))
      .join("")
  }
  const transformVndToMvnd = (value) => {
    return `${value / 1000} K`
  }
  const onBtnRemoveClick = (args) => {
    dispatch(removeItem(args))
  }
  const goToCart = () => {
    window.location.href = "/cart"
  }
  const initialValue = 0
  var subTotal = props.cartItems? props.cartItems.reduce(
    (accumulator, currentValue) => accumulator + currentValue.buyPrice * currentValue.quantity,
    initialValue
  ):0

  const list = (anchor) => (
    <Box
      // sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : "auto" }}
      role="presentation"
      // className="side-cart"
    >
      <h2>Giỏ hàng</h2>
      <Divider className="bg-red-600" />

      {props.cartItems
        ? props.cartItems.map((element, index) => {
            return (
              <div className="p-3" key={index}>
                <div className="flex items-center">
                  <button onClick={() => onBtnRemoveClick(index)}>
                    <RemoveCircleOutlineIcon color="error" />
                  </button>
                  <img
                    className="drawer-img"
                    src={`${process.env.REACT_APP_API_UPLOAD}/image-${element.name}`}
                    alt="cart-item"
                  />
                  <p className="drawer-cost">
                    x{element.quantity}
                    <br></br>
                    Đơn giá: {transformVndToMvnd(element.buyPrice)}
                  </p>
                </div>
                <Divider />
              </div>
            )
          })
        : []}
      <div className="p-3 drawer-cost text-red-600">
        Giá trị đơn hàng: {transformVndToMvnd(subTotal)}
      </div>
      <Divider />
      <div className="p-3 flex justify-center items-center">
        <Button className="btn-login" onClick={goToCart}>
          Xem chi tiết giỏ hàng
        </Button>
      </div>
    </Box>
  )

  return (
    <div>
      <React.Fragment  key={"right"}>
        <Drawer className="side-cart" anchor={"right"} open={props.open} onClose={props.close}>
          {list("right")}
        </Drawer>
      </React.Fragment>
    </div>
  )
}
