/* eslint-disable */

import { HEADER_LOG_OUT } from "constants/header.constants"
import { CART_DRAWER_CLOSE } from "constants/header.constants"
import { CART_DRAWER_OPEN } from "constants/header.constants"

export const openCart = (event) => {
  return {
    type: CART_DRAWER_OPEN,
  }
}
export const closeCart = (event) => {
  return {
    type: CART_DRAWER_CLOSE,
  }
}
export const logOutHandler = (event) => {
  return {
    type: HEADER_LOG_OUT,
  }
}
