/* eslint-disable */

import { CART_QUANTITY_PLUS } from "constants/cart.contants"
import { CART_CLOSE_MODAL } from "constants/cart.contants"
import { CART_CLOSE_MODAL_ORDER_SUCCESS } from "constants/cart.contants"
import { CART_CLOSE_ALERT } from "constants/cart.contants"
import { CART_TO_CHECKOUT } from "constants/cart.contants"
import { CART_REMOVE } from "constants/cart.contants"
import { CART_QUANTITY_SUBTRACT } from "constants/cart.contants"

// Ham xu ly su kien click btn remove item de xoa item trong cart
export const removeItem = (index) => {
  return {
    type: CART_REMOVE,
    payload: index,
  }
}
// Ham` xu ly khi click button -
export const subtractItem = (index) => {
  return {
    type: CART_QUANTITY_SUBTRACT,
    payload: index,
  }
}
// Ham` xu ly khi click button +
export const plusItem = (index) => {
  return {
    type: CART_QUANTITY_PLUS,
    payload: index,
  }
}
export const goCheckOut = () => {
  return {
    type: CART_TO_CHECKOUT,
  }
}
export const closeModal = () => {
  return {
    type: CART_CLOSE_MODAL,
  }
}
export const closeAlert = () => {
  return {
    type: CART_CLOSE_ALERT,
  }
}
