/* eslint-disable */

import axios from "axios"
import { PROFILE_NAME_CHANGE } from "constants/profile.constants"
import { PROFILE_PASSWORD_CHANGE } from "constants/profile.constants"
import { PROFILE_UPDATE_PENDING } from "constants/profile.constants"
import { PROFILE_UPDATE_ERROR } from "constants/profile.constants"
import { PROFILE_UPDATE_AVATAR_SUCCESS } from "constants/profile.constants"
import { PROFILE_UPDATE_PASSWORD_INVALID } from "constants/profile.constants"
import { PROFILE_UPDATE_AVATAR_ERROR } from "constants/profile.constants"
import { PROFILE_CLOSE_MODAL } from "constants/profile.constants"
import { PROFILE_UPDATE_SUCCESS } from "constants/profile.constants"
import { PROFILE_NEW_PASSWORD_CHANGE } from "constants/profile.constants"
import { PROFILE_EMAIL_CHANGE } from "constants/profile.constants"
import { PROFILE_IMAGE_CHANGE } from "constants/profile.constants"
import { PROFILE_OPEN_EDIT_FORM } from "constants/profile.constants"

export const editFormAction = () => {
  return {
    type: PROFILE_OPEN_EDIT_FORM
  }
}
export const imageFileChange = (value) => {
  return {
    type: PROFILE_IMAGE_CHANGE,
    payload: value
  }

}
export const nameChange = (value) => {
  return {
    type: PROFILE_NAME_CHANGE,
    payload: value
  }
}
export const emailChange = (value) => {
  return {
    type: PROFILE_EMAIL_CHANGE,
    payload: value
  }
}
export const passwordChange = (value) => {
  return {
    type: PROFILE_PASSWORD_CHANGE,
    payload: value
  }
}
export const newPasswordChange = (value) => {
  return {
    type: PROFILE_NEW_PASSWORD_CHANGE,
    payload: value
  }
}
export const closeModal = (value) => {
  return {
    type: PROFILE_CLOSE_MODAL,
  }
}
// Hàm fetch dữ liệu product
export const updateProfile = (newData) => {
  return async (dispatch) => {
    try {
      // Lấy dữ liệu cho form data
      const formData = new FormData()
      formData.append("phone", newData.phone)
      formData.append("avatar", newData.avatar)
      formData.append("name", newData.name)
      formData.append("email", newData.email)
      formData.append("role", newData.role)
      formData.append("password", newData.password)
      formData.append("newPassword", newData.newPassword)
      await dispatch({
        type: PROFILE_UPDATE_PENDING,
      })
      const response = await axios.put(process.env.REACT_APP_API_HOST + "/users/" + newData.id, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      if (response.status === 400) {
        return dispatch({
          type: PROFILE_UPDATE_PASSWORD_INVALID,
          message: "Mật khẩu cũ chưa chính xác"
        })
      }
      return dispatch({
        type: PROFILE_UPDATE_SUCCESS,
      })
    } catch (error) {
      if (error.response && error.response.status === 400) {
        return dispatch({
          type: PROFILE_UPDATE_PASSWORD_INVALID,
          message: "Mật khẩu cũ chưa chính xác"
        });
      } else {
        console.error("Error:", error);
      }
    }
  }
}