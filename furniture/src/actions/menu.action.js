/* eslint-disable */

import { MENU_PRODUCT_TYPE_FETCH_SUCCESS } from "constants/menu.constants"
import { FILTER_TYPE_CHANGE } from "constants/menu.constants"
import { MENU_PRODUCT_FETCH_SUCCESS } from "constants/menu.constants"
import { MENU_PRODUCT_PAGE_CHANGE } from "constants/menu.constants"
import { ADD_TO_CART } from "constants/menu.constants"
import { FILTER_NAME_CHANGE } from "constants/menu.constants"
import { MENU_PRODUCT_FETCH_PENDIND } from "constants/menu.constants"
import { MENU_PRODUCT_FETCH_ERROR } from "constants/menu.constants"
import { MENU_PRODUCT_TYPE_FETCH_ERROR } from "constants/menu.constants"
import { MENU_PRODUCT_TYPE_FETCH_PENDIND } from "constants/menu.constants"
import { FILTER_PRICE_CHANGE } from "constants/menu.constants"

// Ham xu ly khi click filter gia' tien
export const changePriceFilter = (event) => {
  return {
    type: FILTER_PRICE_CHANGE,
    payload: event.target.value,
  }
}
// Hàm fetch dữ liệu product
export const fetchProductType = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: MENU_PRODUCT_TYPE_FETCH_PENDIND,
      })
      const response = await fetch(process.env.REACT_APP_API_HOST + "/types", requestOptions)
      const data = await response.json()
      return dispatch({
        type: MENU_PRODUCT_TYPE_FETCH_SUCCESS,
        payload: data.result,
      })
    } catch (error) {
      return dispatch({
        type: MENU_PRODUCT_TYPE_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// Ham xu ly khi click filter loai san pham
export const changeTypeFilter = (event) => {
  return {
    type: FILTER_TYPE_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly  fetch data de render
export const fetchProduct = (limit, page, searchData) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: MENU_PRODUCT_FETCH_PENDIND,
      })

      const paramSearch = new URLSearchParams({
        limit: limit,
        page: page,
      })
      const paramSearchTotal = new URLSearchParams({})
      if (searchData.price !== "") {
        paramSearch.append("min", parseInt(searchData.price))
        paramSearchTotal.append("min", parseInt(searchData.price))
        paramSearch.append("max", parseInt(searchData.price) + 10)
        paramSearchTotal.append("max", parseInt(searchData.price) + 10)
      }

      if (searchData.name !== "") {
        paramSearch.append("name", searchData.name.toUpperCase())
        paramSearchTotal.append("name", searchData.name.toUpperCase())
      }
      if (searchData.type !== "") {
        paramSearch.append("type", searchData.type)
        paramSearchTotal.append("type", searchData.type)
      }
      const responseTotal = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearchTotal.toString(),
        requestOptions
      )
      const dataTotal = await responseTotal.json()
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearch.toString(),
        requestOptions
      )
      const data = await response.json()
      return dispatch({
        type: MENU_PRODUCT_FETCH_SUCCESS,
        payload: {
          total: dataTotal.result.length,
          data: data.result,
        },
      })
    } catch (error) {
      return dispatch({
        type: MENU_PRODUCT_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// Ham xu ly su kien chuyen trang
export const pageChangePagination = (page) => {
  return {
    type: MENU_PRODUCT_PAGE_CHANGE,
    page: page,
  }
}
// Ham xu ly input search
export const changeNameFilter = (event) => {
  return {
    type: FILTER_NAME_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly su kien click add cart
export const addCart = (info, qty) => {
  info.quantity = qty ? qty : 1
  return {
    type: ADD_TO_CART,
    payload: info,
  }
}
