// prettier-ignore
/* eslint-disable */
import { jwtDecode } from "jwt-decode"
import { SIGN_IN_PENDING } from "constants/login.constant"
import { SIGN_IN_PHONE_NOT_FOUNDED } from "constants/login.constant"
import { SIGN_IN_SUCCESS } from "constants/login.constant"
import { SIGN_IN_ERROR } from "constants/login.constant"
import { SIGN_IN_WRONG_PASSWORD } from "constants/login.constant"
import { SIGN_IN_PHONE_INVALID } from "constants/login.constant"
import { SIGN_IN_PASSWORD_CHANGE } from "constants/login.constant"
import { SIGN_IN_PHONE_CHANGE } from "constants/login.constant"
import { SIGN_UP_MODE } from "constants/login.constant"
import { SIGN_IN_MODE } from "constants/login.constant"
import { SIGN_UP_PHONE_CHANGE } from "constants/login.constant"
import { SIGN_UP_PASSWORD_CHANGE } from "constants/login.constant"
import { SIGN_UP_CONFIRM_PASSWORD_CHANGE } from "constants/login.constant"
import { SIGN_UP_NAME_CHANGE } from "constants/login.constant"
import { SIGN_UP_EMAIL_CHANGE } from "constants/login.constant"
import { SIGN_UP_PHONE_INVALID } from "constants/login.constant"
import { SIGN_UP_USERNAME_INVALID } from "constants/login.constant"
import { SIGN_UP_EMAIL_INVALID } from "constants/login.constant"
import { SIGN_UP_CONFIRM_PASSWORD_INVALID } from "constants/login.constant"
import { SIGN_UP_PHONE_USED } from "constants/login.constant"
import { SIGN_UP_SUCCESS } from "constants/login.constant"
import { SIGN_UP_ERROR } from "constants/login.constant"
import { SIGN_UP_PASSWORD_INVALID } from "constants/login.constant"
import { SIGN_IN_PASSWORD_BLANK } from "constants/login.constant"
// Ham xu ly khi nhap so dien thoai dang nhap
export const changePhoneInput = (event) => {
  return {
    type: SIGN_IN_PHONE_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly khi nhap mat khau dang nhap
export const changePasswordInput = (event) => {
  return {
    type: SIGN_IN_PASSWORD_CHANGE,
    payload: event.target.value,
  }
}

// Ham xu ly khi tien hanh dang nhap
export const SignIn = (formData) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SIGN_IN_PENDING,
      })
      const vUser = {
        phone: formData.phone,
        password: formData.password,
      }
      // Kiem tra sdt dang nhap
      if (isPhoneValidate(vUser.phone) == false) {
        return dispatch({
          type: SIGN_IN_PHONE_INVALID,
          message: "Số điện thoại không hợp lệ",
        })
      }
      if (vUser.password == "") {
        return dispatch({
          type: SIGN_IN_PASSWORD_BLANK,
          message: "Chưa nhập mật khẩu",
        })
      }
      var requestOptionsGET = {
        method: "GET",
        redirect: "follow",
      }
      var requestOptionsPOST = {
        method: "POST",
        redirect: "follow",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(vUser),
      }

      var response = await fetch(
        process.env.REACT_APP_API_HOST + `/users/login`,
        requestOptionsPOST
      )
      var data = await response.json()
      // TH khong tim thay so dien thoai dang nhap
      if (response.status === 404) {
        return dispatch({
          type: SIGN_IN_PHONE_NOT_FOUNDED,
          message: "Số điện thoại chưa đăng ký",
        })
      }
      // TH khong dung password
      if (response.status === 400) {
        return dispatch({
          type: SIGN_IN_WRONG_PASSWORD,
          message: "Mật khẩu chưa chính xác",
        })
      }
      const token = data.accessToken
      const refreshToken = data.refreshToken
      // Decode token
      const user = jwtDecode(token)
      // Dung id tu token lay data user
      var responseUser = await fetch(
        process.env.REACT_APP_API_HOST + `/users/` + user.id,
        requestOptionsGET
      )
      var dataUser = await responseUser.json()
      var userRole = "user"
      const role = dataUser.result.role
      // Dang nhap thanh cong
      if (response.status === 200) {
        role.map((element, index) => {
          if (element.name === "admin") {
            return (userRole = "admin")
          }
        })
        return dispatch({
          type: SIGN_IN_SUCCESS,
          refreshToken: refreshToken,
          user: {
            id: user.id,
            role: userRole,
            name: dataUser.result.name,
            phone: dataUser.result.phone,
            email: dataUser.result.email,
            avatar:dataUser.result.avatar?dataUser.result.avatar:null
          },
        })
      }
    } catch (error) {
      return dispatch({
        type: SIGN_IN_ERROR,
        error: error.message,
      })
    }
  }
}
//Hàm kiểm tra SĐT
export function isPhoneValidate(paramPhone) {
  var regexPhoneNumber = /(84|0[1|3|7|9])+([0-9]{8})\b/g
  return paramPhone.match(regexPhoneNumber) ? true : false
}
//Hàm kiểm tra Password
export function validatePassword(paramPassword) {
  if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(\W|_)).{5,}$/.test(paramPassword)) {
    // valid password
    return true
  }
  return false
}
//Hàm kiểm tra email
export function isEmailValidate(paramEmail) {
  var vRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return vRegex.test(paramEmail)
}

// Ham xu ly khi nhap so dien thoai dang ky
export const changePhoneInputSignUp = (event) => {
  return {
    type: SIGN_UP_PHONE_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly khi nhap mat khau dang ky
export const changePasswordInputSignUp = (event) => {
  return {
    type: SIGN_UP_PASSWORD_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly khi nhap xac nhan mat khau dang ky
export const changeConfirmPasswordInputSignUp = (event) => {
  return {
    type: SIGN_UP_CONFIRM_PASSWORD_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly khi nhap email dang ky
export const changeEmailInputSignUp = (event) => {
  return {
    type: SIGN_UP_EMAIL_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly khi nhap ten dang ky
export const changeNameInputSignUp = (event) => {
  return {
    type: SIGN_UP_NAME_CHANGE,
    payload: event.target.value,
  }
}
// ham xu ly su kien sign up
export const signUp = (formData) => {
  return async (dispatch) => {
    try {
      if (isPhoneValidate(formData.phone) === false) {
        return dispatch({
          type: SIGN_UP_PHONE_INVALID,
          message: "Số điện thoại không hợp lệ",
        })
      }
      if (formData.name === "") {
        return dispatch({
          type: SIGN_UP_USERNAME_INVALID,
          message: "Chưa nhập tên người dùng",
        })
      }
      if (isEmailValidate(formData.email) === false) {
        return dispatch({
          type: SIGN_UP_EMAIL_INVALID,
          message: "Email không hợp lệ",
        })
      }
      if (validatePassword(formData.password) === false) {
        return dispatch({
          type: SIGN_UP_PASSWORD_INVALID,
          message: "Mật khẩu phải có ít nhất 4 ký tự, 1 chữ hoa, 1 số, 1 ký tự đặc biệt",
        })
      }
      if (formData.confirmPassword !== formData.password) {
        return dispatch({
          type: SIGN_UP_CONFIRM_PASSWORD_INVALID,
          message: "Mật khẩu không trùng khớp nhau",
        })
      }

      const vUser = {
        name: formData.name,
        password: formData.password,
        phone: formData.phone,
        email: formData.email,
      }
      var requestOptionsGET = {
        method: "GET",
        redirect: "follow",
      }
      // check dublicate phone
      var responsePhoneCheck = await fetch(
        process.env.REACT_APP_API_HOST + `/users?phone=` + vUser.phone,
        requestOptionsGET
      )
      var checkDublicatePhone = await responsePhoneCheck.json()
      if (checkDublicatePhone.result.length > 0) {
        return dispatch({
          type: SIGN_UP_PHONE_USED,
          message: "Số điện thoại đã được sử dụng !",
        })
      }
      var requestOptionsPOST = {
        method: "POST",
        redirect: "follow",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(vUser),
      }

      var response = await fetch(process.env.REACT_APP_API_HOST + `/users`, requestOptionsPOST)
      var data = await response.json()
      if (response.status === 201) {
        return dispatch({
          type: SIGN_UP_SUCCESS,
        })
      }
    } catch (error) {
      return dispatch({
        type: SIGN_UP_ERROR,
        error: error.message,
      })
    }
  }
}
