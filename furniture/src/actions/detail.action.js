/* eslint-disable */
import { PRODUCT_DETAIL_QUANTITY_PLUS } from "constants/detail.constants"
import { PRODUCT_DETAIL_DESC_TAB_CHANGE } from "constants/detail.constants"
import { PRODUCT_DETAIL_FETCH_SUCCESS } from "constants/detail.constants"
import { PRODUCT_RELATE_FETCH_PENDING } from "constants/detail.constants"
import { PRODUCT_RELATE_FETCH_ERROR } from "constants/detail.constants"
import { PRODUCT_RELATE_FETCH_SUCCESS } from "constants/detail.constants"
import { PRODUCT_DETAIL_FETCH_ERROR } from "constants/detail.constants"
import { PRODUCT_DETAIL_FETCH_PENDING } from "constants/detail.constants"
import { PRODUCT_DETAIL_QUANTITY_SUBTRACT } from "constants/detail.constants"
import { PRODUCT_DETAIL_SIZE_CHANGE } from "constants/detail.constants"
import { PRODUCT_DETAIL_COLOR_CHANGE } from "constants/detail.constants"

export const chooseColor = (args) => {
  return {
    type: PRODUCT_DETAIL_COLOR_CHANGE,
    payload: args,
  }
}
export const chooseSize = (args) => {
  return {
    type: PRODUCT_DETAIL_SIZE_CHANGE,
    payload: args,
  }
}
export const plusItem = () => {
  return {
    type: PRODUCT_DETAIL_QUANTITY_PLUS,
  }
}
export const subtractItem = () => {
  return {
    type: PRODUCT_DETAIL_QUANTITY_SUBTRACT,
  }
}
export const changeTab = (value) => {
  return {
    type: PRODUCT_DETAIL_DESC_TAB_CHANGE,
    payload: value,
  }
}
// Hàm fetch dữ liệu product
export const fetchProductInfo = (productId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: PRODUCT_DETAIL_FETCH_PENDING,
      })
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/products/" + productId,
        requestOptions
      )
      const data = await response.json()
      await dispatch({
        type: PRODUCT_DETAIL_FETCH_SUCCESS,
        payload: data.result,
      })
      await dispatch({
        type: PRODUCT_RELATE_FETCH_PENDING,
      })
      const paramSearch = new URLSearchParams({
        limit: 4,
        type: data.result.type,
      })
      const responseRelate = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearch,
        requestOptions
      )
      const dataRelate = await responseRelate.json()
      return dispatch({
        type: PRODUCT_RELATE_FETCH_SUCCESS,
        payload: dataRelate.result,
      })
    } catch (error) {
      return dispatch({
        type: PRODUCT_DETAIL_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// Hàm fetch dữ liệu relate product
export const fetchRelateProduct = (productType) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: PRODUCT_RELATE_FETCH_PENDING,
      })
      const paramSearch = new URLSearchParams({
        limit: 4,
        type: productType,
      })
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearch,
        requestOptions
      )
      const data = await response.json()
      return dispatch({
        type: PRODUCT_RELATE_FETCH_SUCCESS,
        payload: data.result,
      })
    } catch (error) {
      return dispatch({
        type: PRODUCT_RELATE_FETCH_ERROR,
        error: error,
      })
    }
  }
}
