/* eslint-disable */
import axios from "axios"
import { ADMIN_PRODUCT_ADD_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_PRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_AMOUNT_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_IMG_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_PRICE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_IMAGE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_CREATE_PENDIND } from "constants/admin.constants"
import { ADMIN_PRODUCT_TYPE_FETCH_PENDIND } from "constants/admin.constants"
import { ADMIN_PRODUCT_TYPE_FETCH_ERROR } from "constants/admin.constants"
import { ADMIN_PRODUCT_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_OPEN_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_PRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_AMOUNT_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_IMG_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_PENDIND } from "constants/admin.constants"
import { ADMIN_PRODUCT_OPEN_DELETE_MODAL } from "constants/admin.constants"
import { ADMIN_PRODUCT_DELETE_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_AMOUNT_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_FILTER_AMOUNT_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_FILTER_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_DELETE_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_PAGE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_DESCRIPTION_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_BUYPRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_EDIT_TYPE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_FULFILL_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_PRODUCT_FETCH_ERROR } from "constants/admin.constants"
import { ADMIN_PRODUCT_FETCH_PENDIND } from "constants/admin.constants"
import { ADMIN_PRODUCT_TYPE_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_CREATE_SUCCESS } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_AMOUNT_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_CREATE_ERROR } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_BUYPRICE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_TYPE_INVALID } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_CONFIRM } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_DESCRIPTION_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_BUYPRICE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_ADD_TYPE_CHANGE } from "constants/admin.constants"
import { ADMIN_PRODUCT_CLOSE_MODAL } from "constants/admin.constants"
import { ADMIN_PRODUCT_OPEN_ADD_MODAL } from "constants/admin.constants"

// hàm xử lý khi click button +
export const openAddProductModal = () => {
  return {
    type: ADMIN_PRODUCT_OPEN_ADD_MODAL,
  }
}
// hàm xử lý khi click button +
export const closeProductModal = () => {
  return {
    type: ADMIN_PRODUCT_CLOSE_MODAL,
  }
}
// Ham xu ly khi nhap ten san pham
export const changeNameAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_NAME_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeTypeAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_TYPE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap gia san pham
export const changePriceAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_PRICE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap gia khuyen mai
export const changeBuyPriceAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_BUYPRICE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap so luong
export const changeAmountAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_AMOUNT_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap ten san pham
export const changeDescAddProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_ADD_DESCRIPTION_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi them hinh anh
export const changeImageAddProduct = (event) => {
  return {
    type: ADMIN_PRODUCT_ADD_IMG_CHANGE,
    payload: event.target.files[0],
  }
}
// Ham xu ly khi click btn luu san pham
export const createProduct = (stateData) => {
  return async (dispatch) => {
    try {
      const vProduct = {
        name: stateData.name,
        price: stateData.price,
        buyPrice: stateData.buyPrice,
        description: stateData.description,
        amount: stateData.amount,
        type: stateData.type,
        imageUrl: stateData.imageUrl,
      }
      if (!vProduct.name) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_NAME_INVALID,
          message: "Tên sản phẩm không được bỏ trống",
        })
      }
      if (vProduct.type === "") {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_TYPE_INVALID,
          message: "Hãy chọn loại sản phẩm",
        })
      }
      if (!vProduct.price) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_PRICE_INVALID,
          message: "Hãy nhập giá gốc",
        })
      }
      if (!vProduct.buyPrice) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_BUYPRICE_INVALID,
          message: "Hãy nhập giá khuyến mãi",
        })
      }
      if (!vProduct.amount) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_AMOUNT_INVALID,
          message: "Hãy nhập giá khuyến mãi",
        })
      }
      if (!vProduct.imageUrl) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_IMAGE_INVALID,
          message: "Hãy thêm hình ảnh sản phẩm",
        })
      }
      // Kiem tra xem ten san pham co trung lap hay khong
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      const paramsName = new URLSearchParams({
        name: vProduct.name,
      })
      const checkDublicate = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramsName.toString(),
        requestOptions
      )
      const data = await checkDublicate.json()
      if (data.result.length > 0) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_NAME_INVALID,
          message: "Tên sản phẩm đã được sử dụng",
        })
      } else {
        // Lấy dữ liệu cho form data
        const formData = new FormData()
        formData.append("name", stateData.name)
        formData.append("productImg", stateData.imageUrl)
        formData.append("price", stateData.price)
        formData.append("buyPrice", stateData.buyPrice)
        formData.append("description", stateData.description)
        formData.append("amount", stateData.amount)
        formData.append("type", stateData.type)

        await dispatch({
          type: ADMIN_PRODUCT_CREATE_PENDIND,
        })
        // Post Product
        const product = await axios.post(process.env.REACT_APP_API_HOST + "/products", formData, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        if (product.status === 400) {
          console.log(product.response.data.message)
        }
        else if (product.status===201) {
          return dispatch({
            type: ADMIN_PRODUCT_CREATE_SUCCESS,
          })
        }
      }
    } catch (error) {
      console.log(error)
      return dispatch({
        type: ADMIN_PRODUCT_CREATE_ERROR,
        error: error,
      })
    }
  }
}
// Hàm fetch dữ liệu product
export const fetchProductType = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: ADMIN_PRODUCT_TYPE_FETCH_PENDIND,
      })
      const response = await fetch(process.env.REACT_APP_API_HOST + "/types", requestOptions)
      const data = await response.json()
      return dispatch({
        type: ADMIN_PRODUCT_TYPE_FETCH_SUCCESS,
        payload: data.result,
      })
    } catch (error) {
      return dispatch({
        type: ADMIN_PRODUCT_TYPE_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// Hàm fetch dữ liệu product
// Ham xu ly khi click filter loai san pham
export const changeTypeFilter = (event) => {
  return {
    type: FILTER_TYPE_CHANGE,
    payload: event.target.value,
  }
}
// Ham xu ly  fetch data de render
export const fetchProduct = (limit, page, searchData) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: ADMIN_PRODUCT_FETCH_PENDIND,
      })

      const paramSearch = new URLSearchParams({
        limit: limit,
        page: page,
      })
      const paramSearchTotal = new URLSearchParams({})
      if (searchData && searchData.price&&searchData.price !== "") {
        paramSearch.append("min", parseInt(searchData.price))
        paramSearchTotal.append("min", parseInt(searchData.price))
        paramSearch.append("max", parseInt(searchData.price) + 10)
        paramSearchTotal.append("max", parseInt(searchData.price) + 10)
      }

      if (searchData && searchData.name !== "") {
        paramSearch.append("name", searchData.name.toUpperCase())
        paramSearchTotal.append("name", searchData.name.toUpperCase())
      }
      if (searchData && searchData.type&&searchData.type !== "") {
        paramSearch.append("type", searchData.type)
        paramSearchTotal.append("type", searchData.type)
      }
      if (searchData && searchData.amount&&searchData.amount !== "") {
        paramSearch.append("amount", searchData.amount)
        paramSearchTotal.append("amount", searchData.amount)
      }
      const responseTotal = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearchTotal.toString(),
        requestOptions
      )

      const dataTotal = await responseTotal.json()
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramSearch.toString(),
        requestOptions
      )

      const data = await response.json()
      return dispatch({
        type: ADMIN_PRODUCT_FETCH_SUCCESS,
        payload: {
          total: dataTotal.result.length,
          data: data.result,
        },
      })
    } catch (error) {
      return dispatch({
        type: ADMIN_PRODUCT_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// EDIT ACTION
export const openEditModalAction = (data) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: ADMIN_PRODUCT_OPEN_EDIT_MODAL,
      })

      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      const product = await fetch(
        process.env.REACT_APP_API_HOST + "/products/" + data,
        requestOptions
      )
      const productData = await product.json()
      return await dispatch({
        type: ADMIN_PRODUCT_FULFILL_EDIT_MODAL,
        payload: productData.result,
      })
    } catch (error) {
      console.log(error)
      return dispatch({
        type: ADMIN_PRODUCT_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// Ham xu ly khi nhap ten san pham
export const changeNameEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_NAME_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeTypeEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_TYPE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap gia san pham
export const changePriceEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_PRICE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap gia khuyen mai
export const changeBuyPriceEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_BUYPRICE_CHANGE,
    payload: value
  }
}
// Ham xu ly khi nhap so luong
export const changeAmountEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_AMOUNT_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap ten san pham
export const changeDescEditProduct = (value) => {
  return {
    type: ADMIN_PRODUCT_EDIT_DESCRIPTION_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi them hinh anh
export const changeImageEditProduct = (event) => {
  return {
    type: ADMIN_PRODUCT_EDIT_IMG_CHANGE,
    payload: event.target.files[0],
  }
}
// Ham xu ly su kien chuyen trang
export const pageChangePagination = (page) => {
  return {
    type: ADMIN_PRODUCT_PAGE_CHANGE,
    page: page,
  }
}
// Ham xu ly khi click btn sua san pham
export const editProduct = (stateData, id) => {
  return async (dispatch) => {
    try {
      const vProduct = {
        name: stateData.name,
        price: stateData.price,
        buyPrice: stateData.buyPrice,
        description: stateData.description,
        amount: stateData.amount,
        type: stateData.type,
        imageUrl: stateData.imageUrl,
      }
      // if (!vProduct.name) {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_NAME_INVALID,
      //     message: "Tên sản phẩm không được bỏ trống",
      //   })
      // }
      // if (vProduct.type === "") {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_TYPE_INVALID,
      //     message: "Hãy chọn loại sản phẩm",
      //   })
      // }
      // if (!vProduct.price) {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_PRICE_INVALID,
      //     message: "Hãy nhập giá gốc",
      //   })
      // }
      // if (!vProduct.buyPrice) {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_BUYPRICE_INVALID,
      //     message: "Hãy nhập giá khuyến mãi",
      //   })
      // }
      // if (!vProduct.amount) {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_AMOUNT_INVALID,
      //     message: "Hãy nhập giá khuyến mãi",
      //   })
      // }
      // if (!vProduct.imageUrl) {
      //   return dispatch({
      //     type: ADMIN_PRODUCT_ADD_IMAGE_INVALID,
      //     message: "Hãy thêm hình ảnh sản phẩm",
      //   })
      // }
      // Kiem tra xem ten san pham co trung lap hay khong
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      const paramsName = new URLSearchParams({
        name: vProduct.name,
      })
      const checkDublicate = await fetch(
        process.env.REACT_APP_API_HOST + "/products?" + paramsName.toString(),
        requestOptions
      )
      const data = await checkDublicate.json()
      if (data.result.length > 1) {
        return dispatch({
          type: ADMIN_PRODUCT_ADD_NAME_INVALID,
          message: "Tên sản phẩm đã được sử dụng",
        })
      } else {
        // Lấy dữ liệu cho form data
        const formData = new FormData()
        formData.append("name", stateData.name)
        formData.append("productImg", stateData.imageUrl)
        formData.append("price", stateData.price)
        formData.append("buyPrice", stateData.buyPrice)
        formData.append("description", stateData.description)
        formData.append("amount", stateData.amount)
        formData.append("type", stateData.type)
        await dispatch({
          type: ADMIN_PRODUCT_EDIT_PENDIND,
        })
        // Post Product
        const product = await axios.put(process.env.REACT_APP_API_HOST + "/products/" + id, formData, {
          headers: { "Content-Type": "multipart/form-data" },
        })
        if (product.status === 400) {
          console.log(product.response.data.message)
        }
        if (product.status===200) {
          return dispatch({
            type: ADMIN_PRODUCT_EDIT_SUCCESS,
          })
        }
      }
    } catch (error) {
      return dispatch({
        type: ADMIN_PRODUCT_CREATE_ERROR,
        error: error,
      })
    }
  }
}
export const openDeleteModalAction = (name,id) => {
  return {
    type: ADMIN_PRODUCT_OPEN_DELETE_MODAL,
    payload:{name,id},
  }}

  export const deleteProduct = (id) => {
    return async (dispatch) => {
      try {
        const deleteItem = await axios.delete(process.env.REACT_APP_API_HOST + "/products/"+id )
        return dispatch({
          type: ADMIN_PRODUCT_DELETE_SUCCESS,
        })
      } catch (error) {
        return dispatch({
          type: ADMIN_PRODUCT_DELETE_ERROR,
          error: error,
        })
      }
    }}
    // Ham xu ly su kien input filter name
export const inputNameFilter = (value) => {
  return {
    type: ADMIN_PRODUCT_FILTER_NAME_CHANGE,
    payload: value,
  }
}
    // Ham xu ly su kien input filter amount
    export const inputAmountFilter = (value) => {
      return {
        type: ADMIN_PRODUCT_FILTER_AMOUNT_CHANGE,
        payload: value,
      }
    }