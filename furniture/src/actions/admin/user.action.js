/* eslint-disable */

import { isEmailValidate } from "actions/login.action"
import { validatePassword } from "actions/login.action"
import { isPhoneValidate } from "actions/login.action"
import { ADMIN_USER_ADD_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_PASSWORD_INVALID } from "constants/admin.constants"
import { ADMIN_USER_CREATE_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_CLOSE_MODAL } from "constants/admin.constants"
import { ADMIN_USER_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_PAGE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_OPEN_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_USER_EDIT_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_EDIT_NAME_INVALID } from "constants/admin.constants"
import { ADMIN_USER_EDIT_ERROR } from "constants/admin.constants"
import { ADMIN_USER_EDIT_SUCCESS } from "constants/admin.constants"
import { ADMIN_USER_EDIT_EMAIL_INVALID } from "constants/admin.constants"
import { ADMIN_USER_EDIT_ROLE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_EDIT_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_FETCH_TO_EDIT_PENDIND } from "constants/admin.constants"
import { ADMIN_USER_FETCH_ERROR } from "constants/admin.constants"
import { ADMIN_USER_FETCH_PENDING } from "constants/admin.constants"
import { ADMIN_USER_CREATE_ERROR } from "constants/admin.constants"
import { ADMIN_USER_ADD_CONFIRM_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_EMAIL_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_PHONE_INVALID } from "constants/admin.constants"
import { ADMIN_USER_ADD_PASSWORD_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_CONFIRM_PASSWORD_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_PHONE_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_ADD_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_USER_OPEN_ADD_MODAL } from "constants/admin.constants"

export const openAddModalAction = () => {
    return {
      type: ADMIN_USER_OPEN_ADD_MODAL,
    }
  }
  export const closeModal = () => {
    return {
      type: ADMIN_USER_CLOSE_MODAL,
    }
  }
  export const nameChangeAddAction = (value) => {
    return {
      type: ADMIN_USER_ADD_NAME_CHANGE,
      payload:value
    }
  }
  export const emailChangeAddAction = (value) => {
    return {
      type: ADMIN_USER_ADD_EMAIL_CHANGE,
      payload:value
    }
  }
  export const phoneChangeAddAction = (value) => {
    return {
      type: ADMIN_USER_ADD_PHONE_CHANGE,
      payload:value
    }
  }
  export const passwordChangeAddAction = (value) => {
    return {
      type: ADMIN_USER_ADD_PASSWORD_CHANGE,
      payload:value
    }
  }
  export const confirmChangeAddAction = (value) => {
    return {
      type: ADMIN_USER_ADD_CONFIRM_PASSWORD_CHANGE,
      payload:value
    }
  }
  // Ham xu ly khi click btn them user
export const createUser = (formData) => {
  return async (dispatch) => {
    try {
        if (formData.name === "") {
            return dispatch({
              type: ADMIN_USER_ADD_NAME_INVALID,
              message: "Chưa nhập tên người dùng",
            })
          }
          if (isEmailValidate(formData.email) === false) {
            return dispatch({
              type: ADMIN_USER_ADD_EMAIL_INVALID,
              message: "Email không hợp lệ",
            })
          }
        if (isPhoneValidate(formData.phone) === false) {
            return dispatch({
              type: ADMIN_USER_ADD_PHONE_INVALID,
              message: "Số điện thoại không hợp lệ",
            })
          }
          if (validatePassword(formData.password) === false) {
            return dispatch({
              type: ADMIN_USER_ADD_PASSWORD_INVALID,
              message: "Mật khẩu phải có ít nhất 4 ký tự, 1 chữ hoa, 1 số, 1 ký tự đặc biệt",
            })
          }
          if (formData.confirmPassword !== formData.password) {
            return dispatch({
              type: ADMIN_USER_ADD_CONFIRM_INVALID,
              message: "Mật khẩu không trùng khớp nhau",
            })
          }
          
        const vUser = {
            name: formData.name,
            password: formData.password,
            phone: formData.phone,
            email: formData.email,
          }
          var requestOptionsGET = {
            method: "GET",
            redirect: "follow",
          }
          // check dublicate phone
          var responsePhoneCheck = await fetch(
            process.env.REACT_APP_API_HOST + `/users?phone=` + vUser.phone,
            requestOptionsGET
          )
          var checkDublicatePhone = await responsePhoneCheck.json()
          if (checkDublicatePhone.result.length > 0) {
            return dispatch({
              type: ADMIN_USER_ADD_PHONE_INVALID,
              message: "Số điện thoại đã được sử dụng !",
            })
          }
          var requestOptionsPOST = {
            method: "POST",
            redirect: "follow",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(vUser),
          }
    
          var response = await fetch(process.env.REACT_APP_API_HOST + `/users`, requestOptionsPOST)
          var data = await response.json()
          if (response.status === 201) {
            return dispatch({
              type: ADMIN_USER_CREATE_SUCCESS,
            })
          }
    } catch (error) {
      console.log(error)
      return dispatch({
        type: ADMIN_USER_CREATE_ERROR,
        error: error,
      })
    }
  }
}



 // Ham xu ly khi fetch lai user sau cac action
 export const fetchUser = (limit,page,filter) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
              method: "GET",
              redirect: "follow",
            }
            await dispatch({
              type: ADMIN_USER_FETCH_PENDING,
            })
            const paramSearch = new URLSearchParams({
              limit: limit,
              page: page,
            })
            const responseTotal = await fetch(
              process.env.REACT_APP_API_HOST + "/users",
              requestOptions
            )
            const dataTotal = await responseTotal.json()
            const responseOrder = await fetch(
              process.env.REACT_APP_API_HOST + "/users?" + paramSearch.toString(),
              requestOptions
            )
            const UserData = await responseOrder.json()
            return dispatch({
              type: ADMIN_USER_FETCH_SUCCESS,
              payload: {
                total: dataTotal.result.length,
                data: UserData.result,
              },
            })
          } catch (error) {
            console.log(error)
            return dispatch({
              type: ADMIN_USER_FETCH_ERROR,
              error: error,
            })
          }
        }
  }
    // Ham xu ly su kien chuyen trang
export const pageChangePagination = (page) => {
    return {
      type: ADMIN_USER_PAGE_CHANGE,
      page: page,
    }
  }
  // open modal edit order
export const openEditUserModal =(userId)=>{
    return async (dispatch)=>{
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: ADMIN_USER_FETCH_TO_EDIT_PENDIND,
      })
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/users/" + userId,
        requestOptions
      )
      const data = await response.json()
      return dispatch({
        type:ADMIN_USER_OPEN_EDIT_MODAL,
        payload:data.result
      })
  }
     catch (error) {
      return dispatch({
          type: ADMIN_ORDER_FETCH_TO_EDIT_ERROR,
          error: error,
        })
    }
   }
  }
  export const nameChangeEditAction = (value) => {
    return {
      type: ADMIN_USER_EDIT_NAME_CHANGE,
      payload:value
    }
  }
  export const emailChangeEditAction = (value) => {
    return {
      type: ADMIN_USER_EDIT_EMAIL_CHANGE,
      payload:value
    }
  }
  export const roleChangeEditAction = (value) => {
    return {
      type: ADMIN_USER_EDIT_ROLE_CHANGE,
      payload:value
    }
  }
   // Ham xu ly khi click btn them user
export const editUser = (formData,userId) => {
    return async (dispatch) => {
      try {
          if (formData.name === "") {
              return dispatch({
                type: ADMIN_USER_EDIT_NAME_INVALID,
                message: "Chưa nhập tên người dùng",
              })
            }
            if (isEmailValidate(formData.email) === false) {
              return dispatch({
                type: ADMIN_USER_EDIT_EMAIL_INVALID,
                message: "Email không hợp lệ",
              })
            }
          const vUser = {
              name: formData.name,
              email: formData.email,
              role:formData.role
            }
            var requestOptionsPUT = {
              method: "PUT",
              redirect: "follow",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(vUser),
            }
            var response = await fetch(process.env.REACT_APP_API_HOST + `/users/`+userId, requestOptionsPUT)
            var data = await response.json()
            if (response.status === 200) {
              return dispatch({
                type: ADMIN_USER_EDIT_SUCCESS,
              })
            }
      } catch (error) {
        console.log(error)
        return dispatch({
          type: ADMIN_USER_EDIT_ERROR,
          error: error,
        })
      }
    }
  }
  
  
  