/* eslint-disable */
import { isPhoneValidate } from "actions/login.action"
import { isEmailValidate } from "actions/login.action"
import { ADMIN_ORDER_CREATE_ADDRESS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NAME_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NOTE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CLOSE_MODAL } from "constants/admin.constants"
import { CREATE_ORDER_ERROR } from "constants/admin.constants"
import { CREATE_ORDER_NAME_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_EMAIL_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_CART_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_QUANTITY_PLUS } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_NEXT_STEP } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_PENDING } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_TO_EDIT_PENDING } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_NOTE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_CITY_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_EDIT_QUANTITY_SUBTRACT } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_CART_CHANGE } from "constants/admin.constants"
import { EDIT_ORDER_BLANK_CART } from "constants/admin.constants"
import { EDIT_ORDER_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_DELETE_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_CODE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_PAGE_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_FILTER_STATUS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_DELETE_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_DELETE_SUCCESS } from "constants/admin.constants"
import { EDIT_ORDER_SUCCESS } from "constants/admin.constants"
import { EDIT_ORDER_PENDING } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_EDIT_QUANTITY_PLUS } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_STATUS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_EDIT_ADDRESS_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_TO_EDIT_ERROR } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_EDIT_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_FETCH_SUCCESS } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_PREV_STEP } from "constants/admin.constants"
import { CREATE_ORDER_BLANK_CART } from "constants/admin.constants"
import { ADMIN_ORDER_MINICART_QUANTITY_SUBTRACT } from "constants/admin.constants"
import { ADMIN_CLOSE_MODAL_ORDER_SUCCESS } from "constants/admin.constants"
import { CREATE_ORDER_SHIPDATE_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_CITY_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_ADDRESS_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_PHONE_INVALID } from "constants/admin.constants"
import { CREATE_ORDER_PENDING } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_OUTSTOCK } from "constants/admin.constants"
import { DATE_PICKER_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_OPEN_ADD_MODAL } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_CITY_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_EMAIL_CHANGE } from "constants/admin.constants"
import { ADMIN_ORDER_CREATE_PHONE_CHANGE } from "constants/admin.constants"

// Ham xu ly khi nhap ten khach hang
export const changeNameCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_NAME_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap so dien thoai
export const changePhoneCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_PHONE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap email
export const changeEmailCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_EMAIL_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap note
export const changeNoteCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_NOTE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap dia chi
export const changeAddressCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_ADDRESS_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeCityCreateOrder = (value) => {
  return {
    type: ADMIN_ORDER_CREATE_CITY_CHANGE,
    payload: value,
  }
}
export const openCreateOrderModal = () => {
  return {
    type: ADMIN_ORDER_OPEN_ADD_MODAL,
  }
}
// Ham xu ly date time picker
export const datePicked = (event) => {
  
  return {
    type: DATE_PICKER_CHANGE,
    payload: event.$d,
  }
}
export const closeModalHandler = (event) => {
  return {
    type: ADMIN_ORDER_CLOSE_MODAL,
  }
}
// Ham xu ly khi click btn create order

export const createOrder = (data, itemArr) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: CREATE_ORDER_PENDING,
      })
      // validate data
      if (data.name === "") {
        return dispatch({
          type: CREATE_ORDER_NAME_INVALID,
          message: "Hãy nhập tên"
        })
      }
      if (!isPhoneValidate(data.phone)) {
        return dispatch({
          type: CREATE_ORDER_PHONE_INVALID,
          message: "Số điện thoại không hợp lệ"
        })
      }
      if (!isEmailValidate(data.email)) {
        return dispatch({
          type: CREATE_ORDER_EMAIL_INVALID,
          message: "Email không hợp lệ"
        })
      }
      if (data.address === "") {
        return dispatch({
          type: CREATE_ORDER_ADDRESS_INVALID,
          message: "Hãy nhập địa chỉ nhận hàng"
        })
      }
      if (data.city === "") {
        return dispatch({
          type: CREATE_ORDER_CITY_INVALID,
          message: "Hãy chọn thành phố"
        })
      }
      if (!data.shippedDate) {
        return dispatch({
          type: CREATE_ORDER_SHIPDATE_INVALID,
          message: "Hãy chọn thời gian nhận hàng"
        })
      }
      if (itemArr.length<1) {
        return dispatch({
          type: CREATE_ORDER_BLANK_CART,
          message: "Bạn chưa chọn món nào cho giỏ hàng của mình"
        })
      }
      // validate so luong trong kho co du de tao don hang khong
      var order = []
      itemArr.map((element, index) => {
        order.push({ id: element._id, name: element.name, quantity: element.quantity })
      })

      var requestOptionsCheckStock = {
        method: "POST",
        redirect: "follow",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(order),
      }
      var checkStock = await fetch(
        process.env.REACT_APP_API_HOST + `/products/checkstock/`,
        requestOptionsCheckStock
      )
      var stock = await checkStock.json()
      if (stock.message.length > 0) {
        return dispatch({
          type: ADMIN_ORDER_CREATE_OUTSTOCK,
          message: stock.message,
        })
      }
      var query = new URLSearchParams({
        phone: data.phone
      })
      var requestOptionsGet = {
        method: 'GET',
        redirect: 'follow',
      };
      const initialValue = 0
      var subTotal = itemArr.reduce(
        (accumulator, currentValue) => accumulator + currentValue.buyPrice * currentValue.quantity,
        initialValue
      )
      var shippingFee
      shippingFee = subTotal < 10000000 ? 300000 : 0
      const cost = shippingFee + subTotal
      const vOrder = {
        shippedDate: data.shippedDate,
        note: data.note,
        cost: cost,
        address: data.address,
        city: data.city,
        details:itemArr
      }
      var requestOptionsPostOrder = {
        method: 'POST',
        redirect: 'follow',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(vOrder)
      };
      // Kiem tra nguoi dung co ton tai hay chua
      var findUser = await fetch(process.env.REACT_APP_API_HOST + `/users?` + query.toString(), requestOptionsGet)
      var dataUser = await findUser.json()
      if (dataUser.result.length === 0) {
        // Neu chua thi create user
        const vUser = {
          name: data.name,
          phone: data.phone,
          email: data.email,
          password: "createdByAdmin1!"
        }
        var requestOptionsPostUser = {
          method: 'POST',
          redirect: 'follow',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(vUser)
        };
        var createUser = await fetch(process.env.REACT_APP_API_HOST + `/users`, requestOptionsPostUser)
        const dataCreateCustomer = await createUser.json()
        var createOrder = await fetch(process.env.REACT_APP_API_HOST + `/orders/` + dataCreateCustomer.result.phone, requestOptionsPostOrder)
        var dataCreateOrder = await createOrder.json()

        return dispatch({
          type: CREATE_ORDER_SUCCESS,
        })
      }
      else {
        // Neu user da ton tai thi  create order
        var createOrder = await fetch(process.env.REACT_APP_API_HOST + `/orders/` + dataUser.result[0].phone, requestOptionsPostOrder)
        const dataCreateOrder = await createOrder.json()
        return dispatch({
          type: CREATE_ORDER_SUCCESS,
        })
      }
    } catch (error) {
      console.log(error)
      return dispatch({
        type: CREATE_ORDER_ERROR,
        error: error.message,
      })
    }
  }
}
export const closeModalOrderSuccess = () => {
  return {
    type: ADMIN_CLOSE_MODAL_ORDER_SUCCESS,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeMiniCartSelectCreateOrder = (value) => {
  if (value.length > 0) {
    value[value.length - 1].quantity = 1
  }
  return {
    type: ADMIN_ORDER_CREATE_CART_CHANGE,
    payload: value,
  }
}
// Ham` xu ly khi click button -
export const subtractItem = (index) => {
  return {
    type: ADMIN_ORDER_MINICART_QUANTITY_SUBTRACT,
    payload: index,
  }
}
// Ham` xu ly khi click button +
export const plusItem = (index) => {
  return {
    type: ADMIN_ORDER_MINICART_QUANTITY_PLUS,
    payload: index,
  }
}
// Ham` xu ly khi click button +
export const nextStep = () => {
  return {
    type: ADMIN_ORDER_CREATE_NEXT_STEP,
  }
}
// Ham` xu ly khi click button -
export const prevStep = () => {
  return {
    type: ADMIN_ORDER_CREATE_PREV_STEP,
  }
}
// Ham xu ly  fetch data de render
export const fetchOrder = (limit, page, searchData) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      }
      await dispatch({
        type: ADMIN_ORDER_FETCH_PENDING,
      })

      const paramSearch = new URLSearchParams({
        limit: limit,
        page: page,
      })
      const paramSearchTotal = new URLSearchParams({})
      if (searchData && searchData.code !== "") {
        paramSearch.append("code", searchData.code.toUpperCase())
        paramSearchTotal.append("code", searchData.code.toUpperCase())
      }
      if (searchData && searchData.status&&searchData.status !== "") {
        paramSearch.append("status", searchData.status)
        paramSearchTotal.append("status", searchData.status)
      }
      const responseTotal = await fetch(
        process.env.REACT_APP_API_HOST + "/orders?" + paramSearchTotal.toString(),
        requestOptions
      )
      const dataTotal = await responseTotal.json()
      const response = await fetch(
        process.env.REACT_APP_API_HOST + "/orders?" + paramSearch.toString(),
        requestOptions
      )

      const data = await response.json()
      return dispatch({
        type: ADMIN_ORDER_FETCH_SUCCESS,
        payload: {
          total: dataTotal.result.length,
          data: data.result,
        },
      })
    } catch (error) {
      console.log(error);
      return dispatch({
        type: ADMIN_ORDER_FETCH_ERROR,
        error: error,
      })
    }
  }
}
// open modal edit order
export const openEditOrderModal =(orderId)=>{
  return async (dispatch)=>{
  try {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    }
    await dispatch({
      type: ADMIN_ORDER_FETCH_TO_EDIT_PENDING,
    })
    const response = await fetch(
      process.env.REACT_APP_API_HOST + "/orders/all/" + orderId,
      requestOptions
    )
    const data = await response.json()
    return dispatch({
      type:ADMIN_ORDER_OPEN_EDIT_MODAL,
      payload:data
    })
}
   catch (error) {
    return dispatch({
        type: ADMIN_ORDER_FETCH_TO_EDIT_ERROR,
        error: error,
      })
  }
 }
}
// Ham xu ly khi nhap note
export const changeNoteEditOrder = (value) => {
  return {
    type: ADMIN_ORDER_EDIT_NOTE_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap dia chi
export const changeAddressEditOrder = (value) => {
  return {
    type: ADMIN_ORDER_EDIT_ADDRESS_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeCityEditOrder = (value) => {
  return {
    type: ADMIN_ORDER_EDIT_CITY_CHANGE,
    payload: value,
  }
}
// Ham xu ly khi nhap loai san pham
export const changeStatusEditOrder = (value) => {
  return {
    type: ADMIN_ORDER_EDIT_STATUS_CHANGE,
    payload: value,
  }
}
// Ham` xu ly khi click button -
export const subtractItemEdit = (index) => {
  return {
    type: ADMIN_ORDER_MINICART_EDIT_QUANTITY_SUBTRACT,
    payload: index,
  }
}
// Ham` xu ly khi click button +
export const plusItemEdit = (index) => {
  return {
    type: ADMIN_ORDER_MINICART_EDIT_QUANTITY_PLUS,
    payload: index,
  }
}
// Ham xu ly khi nhap san pham
export const changeMiniCartSelectEditOrder = (value) => {
  if (value.length > 0) {
    value[value.length - 1].quantity = 1
  }
  return {
    type: ADMIN_ORDER_EDIT_CART_CHANGE,
    payload: value,
  }
}
// Ham xu ly edit order
export const editOrder = (data, itemArr) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: EDIT_ORDER_PENDING,
      })
      // validate data
      if (itemArr.length<1) {
        return dispatch({
          type: EDIT_ORDER_BLANK_CART,
          message: "Bạn chưa chọn món nào cho giỏ hàng của mình"
        })
      }
      // validate so luong trong kho co du de tao don hang khong
      var order = []
      itemArr.map((element, index) => {
        order.push({ id: element._id, name: element.name, quantity: element.quantity })
      })

      var requestOptionsGet = {
        method: 'GET',
        redirect: 'follow',
      };
      const initialValue = 0
      var subTotal = itemArr.reduce(
        (accumulator, currentValue) => accumulator + currentValue.buyPrice * currentValue.quantity,
        initialValue
      )
      var shippingFee
      shippingFee = subTotal < 10000000 ? 300000 : 0
      const cost = shippingFee + subTotal
      const vOrder = {
        shippedDate: data.shippedDate,
        note: data.note,
        cost: cost,
        address: data.address,
        city: data.city,
        details:itemArr,
        status:data.status
      }
      var requestOptionsPutOrder = {
        method: 'PUT',
        redirect: 'follow',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(vOrder)
      };
        var updateOrder = await fetch(process.env.REACT_APP_API_HOST + `/orders/` + data.id, requestOptionsPutOrder)
        const dataUpdateOrder = await updateOrder.json()
        return dispatch({
          type: EDIT_ORDER_SUCCESS,
          payload:dataUpdateOrder
        })
    } catch (error) {
      console.log(error)
      return dispatch({
        type: EDIT_ORDER_ERROR,
        error: error.message,
      })
    }
  }
}
export const openDeleteModalAction = (code,id) => {
  return {
    type: ADMIN_ORDER_OPEN_DELETE_MODAL,
    payload:{code:code,id:id},
  }}
export const deleteOrder = (id) => {
  return async (dispatch) => {
    try {
      const deleteOrder = await axios.delete(process.env.REACT_APP_API_HOST + "/orders/"+id )
      return dispatch({
        type: ADMIN_ORDER_DELETE_SUCCESS,
      })
    } catch (error) {
      return dispatch({
        type: ADMIN_ORDER_DELETE_ERROR,
        error: error,
      })
    }
  }}
  export const inputCodeFilter =(value)=>{
    return{
      type:ADMIN_ORDER_FILTER_CODE_CHANGE,
      payload:value
    }
  }
  export const inputStatusFilter =(value)=>{
    return{
      type:ADMIN_ORDER_FILTER_STATUS_CHANGE,
      payload:value
    }
  }
  // Ham xu ly su kien chuyen trang
export const pageChangePagination = (page) => {
  return {
    type: ADMIN_ORDER_PAGE_CHANGE,
    page: page,
  }
}