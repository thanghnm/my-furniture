/* eslint-disable */
export const PROFILE_OPEN_EDIT_FORM = "open edit form profile"
export const PROFILE_IMAGE_CHANGE = "open change avatar"
export const PROFILE_NAME_CHANGE = "open change name profile"
export const PROFILE_EMAIL_CHANGE = "open change email"
export const PROFILE_PASSWORD_CHANGE = "open change password profile"
export const PROFILE_NEW_PASSWORD_CHANGE = "open change new password"
export const PROFILE_FETCH_SUCCESS = "fetch profile success"
export const PROFILE_FETCH_ERROR = "fetch profile errror"
export const PROFILE_UPDATE_PENDING = "UPDATE profile PENDING"
export const PROFILE_UPDATE_SUCCESS = "UPDATE profile success"
export const PROFILE_UPDATE_ERROR = "UPDATE profile errror"
export const PROFILE_CLOSE_MODAL = "close modal profile"
export const PROFILE_UPDATE_AVATAR_SUCCESS = "UPDATE_AVATAR profile success"
export const PROFILE_UPDATE_AVATAR_ERROR = "UPDATE_AVATAR profile errror"
export const PROFILE_UPDATE_PASSWORD_INVALID = "UPDATE password invalid"

