/* eslint-disable */
export const CART_QUANTITY_PLUS = "signal plus quantity of each item"
export const CART_QUANTITY_SUBTRACT = "signal subtract quantity of each item"
export const CART_REMOVE = "signal remove of each item"
export const CART_TO_CHECKOUT = "signal validate user and go to check out page"
export const CART_CLOSE_MODAL = "signal close modal"
export const CART_CLOSE_ALERT = "signal close alert"