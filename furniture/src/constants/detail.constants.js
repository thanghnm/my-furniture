/* eslint-disable */

export const PRODUCT_DETAIL_COLOR_CHANGE = "signal change color"
export const PRODUCT_DETAIL_SIZE_CHANGE = "signal change size"
export const PRODUCT_DETAIL_QUANTITY_PLUS = "signal plus quantity item"
export const PRODUCT_DETAIL_QUANTITY_SUBTRACT = "signal subtract quantity item"
export const PRODUCT_DETAIL_DESC_TAB_CHANGE = "signal change tab description"
export const PRODUCT_DETAIL_FETCH_PENDING = "signal fetch product detail pending"
export const PRODUCT_DETAIL_FETCH_SUCCESS = "signal fetch product detail success"
export const PRODUCT_DETAIL_FETCH_ERROR = "signal fetch product detail error"
export const PRODUCT_RELATE_FETCH_PENDING = "signal fetch product RELATE pending"
export const PRODUCT_RELATE_FETCH_SUCCESS = "signal fetch product RELATE success"
export const PRODUCT_RELATE_FETCH_ERROR = "signal fetch product RELATE error"
