// prettier-ignore
/* eslint-disable */

export const SIGN_IN_PHONE_CHANGE = "Signal signin form textfield phone change"
export const SIGN_IN_PASSWORD_CHANGE = "Signal signin form textfield password change"
export const SIGN_IN_BUTTON_CLICK = "Connect to sign in"
export const SIGN_IN_PENDING = "Signal sign in pending"
export const SIGN_IN_PHONE_INVALID = "Signal sign in phone number invalid"
export const SIGN_IN_PHONE_NOT_FOUNDED = "Signal sign in wrong phone number"
export const SIGN_IN_PASSWORD_BLANK = "Signal sign in blank password"
export const SIGN_IN_WRONG_PASSWORD = "Signal sign in wrong password"
export const SIGN_IN_SUCCESS = "Signal sign in success"
export const SIGN_IN_ERROR = "Signal sign in error"
export const SIGN_UP_PHONE_CHANGE = "Signal signup form textfield phone change"
export const SIGN_UP_NAME_CHANGE = "Signal signup form textfield name change"
export const SIGN_UP_EMAIL_CHANGE = "Signal signup form textfield email change"
export const SIGN_UP_PASSWORD_CHANGE = "Signal signup form textfield password change"
export const SIGN_UP_CONFIRM_PASSWORD_CHANGE =
  "Signal signup form textfield confirm password change"
export const SIGN_UP_PENDING = "Signal sign up pending"
export const SIGN_UP_PHONE_INVALID = "Signal signup form textfield phone invalid"
export const SIGN_UP_USERNAME_INVALID = "Signal signup form textfield username invalid"
export const SIGN_UP_EMAIL_INVALID = "Signal signup form textfield email invalid"
export const SIGN_UP_PASSWORD_INVALID = "Signal signup form textfield password invalid"
export const SIGN_UP_CONFIRM_PASSWORD_INVALID =
  "Signal signup form textfield confirm password invalid"
export const SIGN_UP_PHONE_USED = "Signal phone number is used"
export const SIGN_UP_SUCCESS = "Signal sign up success"
export const SIGN_UP_ERROR = "Signal sign up error"

// export const SIGN_UP_MODE = "Signal turn login page to sign up"
// export const SIGN_IN_MODE = "Signal turn login page to sign in"
