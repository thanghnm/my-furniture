/* eslint-disable */
// PRODUCT
// create
export const ADMIN_PRODUCT_OPEN_ADD_MODAL = "signal open modal add product"
export const ADMIN_PRODUCT_CLOSE_MODAL = "signal close modal product"
export const ADMIN_PRODUCT_ADD_NAME_CHANGE = "signal change input name modal add product"
export const ADMIN_PRODUCT_ADD_TYPE_CHANGE = "signal change select type modal add product"
export const ADMIN_PRODUCT_ADD_PRICE_CHANGE = "signal change input price modal add product"
export const ADMIN_PRODUCT_ADD_BUYPRICE_CHANGE = "signal change input buy price modal add product"
export const ADMIN_PRODUCT_ADD_AMOUNT_CHANGE = "signal change input amount modal add product"
export const ADMIN_PRODUCT_ADD_DESCRIPTION_CHANGE = "signal change input desc modal add product"
export const ADMIN_PRODUCT_ADD_IMG_CHANGE = "signal change input img modal add product"
export const ADMIN_PRODUCT_ADD_CONFIRM = "signal click button create items"
export const ADMIN_PRODUCT_ADD_NAME_INVALID = "signal input name modal add product invalid"
export const ADMIN_PRODUCT_ADD_TYPE_INVALID = "signal select type modal add product invalid"
export const ADMIN_PRODUCT_ADD_PRICE_INVALID = "signal input price modal add product invalid"
export const ADMIN_PRODUCT_ADD_BUYPRICE_INVALID = "signal input buy price modal add product invalid"
export const ADMIN_PRODUCT_ADD_AMOUNT_INVALID = "signal input amount modal add product invalid"
export const ADMIN_PRODUCT_ADD_IMAGE_INVALID = "signal input image modal add product invalid"
export const ADMIN_PRODUCT_CREATE_ERROR = "signal create error"
export const ADMIN_PRODUCT_CREATE_PENDIND = "signal create pending"
export const ADMIN_PRODUCT_CREATE_SUCCESS = "signal create success"
// read
export const ADMIN_PRODUCT_TYPE_FETCH_ERROR = "signal type fetched error"
export const ADMIN_PRODUCT_TYPE_FETCH_PENDIND = "signal type fetched pending"
export const ADMIN_PRODUCT_TYPE_FETCH_SUCCESS = "signal type fetch success"
export const ADMIN_PRODUCT_INIT_SUCCESS = "signal fetch initial success"
export const ADMIN_PRODUCT_FETCH_ERROR = "signal product fetched error"
export const ADMIN_PRODUCT_FETCH_PENDIND = "signal product fetched pending"
export const ADMIN_PRODUCT_FETCH_SUCCESS = "signal product fetch success"
export const ADMIN_PRODUCT_PAGE_CHANGE = "signal change page admin product"

// update
export const ADMIN_PRODUCT_OPEN_EDIT_MODAL = "signal open modal edit product"
export const ADMIN_PRODUCT_FULFILL_EDIT_MODAL = "signal fulfill modal edit product"
export const ADMIN_PRODUCT_EDIT_NAME_CHANGE = "signal change input name modal EDIT product"
export const ADMIN_PRODUCT_EDIT_TYPE_CHANGE = "signal change select type modal EDIT product"
export const ADMIN_PRODUCT_EDIT_PRICE_CHANGE = "signal change input price modal EDIT product"
export const ADMIN_PRODUCT_EDIT_BUYPRICE_CHANGE = "signal change input buy price modal EDIT product"
export const ADMIN_PRODUCT_EDIT_AMOUNT_CHANGE = "signal change input amount modal EDIT product"
export const ADMIN_PRODUCT_EDIT_DESCRIPTION_CHANGE = "signal change input desc modal EDIT product"
export const ADMIN_PRODUCT_EDIT_IMG_CHANGE = "signal change input img modal EDIT product"
export const ADMIN_PRODUCT_EDIT_ERROR = "signal EDIT error"
export const ADMIN_PRODUCT_EDIT_PENDIND = "signal EDIT pending"
export const ADMIN_PRODUCT_EDIT_SUCCESS = "signal EDIT success"

// DELETE
export const ADMIN_PRODUCT_OPEN_DELETE_MODAL="signal delete model"
export const ADMIN_PRODUCT_DELETE_ERROR = "signal DELETE error"
export const ADMIN_PRODUCT_DELETE_SUCCESS = "signal DELETE success"
// FILTER
export const ADMIN_PRODUCT_FILTER_NAME_CHANGE = "signal change input name FILTER "
export const ADMIN_PRODUCT_FILTER_AMOUNT_CHANGE = "signal change input amount FILTER "
export const ADMIN_PRODUCT_FILT = "signal filting data to admin "
// ORDER
export const ADMIN_ORDER_OPEN_ADD_MODAL = "signal open modal add ORDER"
export const ADMIN_ORDER_CLOSE_MODAL = "signal close modal ORDER"
export const ADMIN_ORDER_CREATE_NAME_CHANGE = "signal change input name modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_PHONE_CHANGE = "signal change input phone modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_EMAIL_CHANGE = "signal change input email modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_ADDRESS_CHANGE = "signal change input ADDRESS modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_CITY_CHANGE = "signal change select CITY modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_CART_CHANGE = "signal change select cart modal CREATE ORDER"
export const ADMIN_ORDER_CREATE_SHIPPED_TIME_CHANGE = "signal change date picker create order"
export const ADMIN_ORDER_CREATE_NOTE_CHANGE = "signal change note create order"
export const DATE_PICKER_CHANGE = "signal change date time picker"
export const ADMIN_ORDER_CREATE_OUTSTOCK = "signal item out of stock"
export const CREATE_ORDER_ERROR = "create order Error"
export const CREATE_ORDER_PENDING = "create order pending"
export const CREATE_ORDER_SUCCESS = "create order success"
export const CREATE_ORDER_NAME_INVALID = "input name create order invalid"
export const CREATE_ORDER_PHONE_INVALID = "input PHONE create order invalid"
export const CREATE_ORDER_EMAIL_INVALID = "input EMAIL create order invalid"
export const CREATE_ORDER_ADDRESS_INVALID = "input ADDRESS create order invalid"
export const CREATE_ORDER_SHIPDATE_INVALID = "input SHIPDATE create order invalid"
export const CREATE_ORDER_CITY_INVALID = "input CITY create order invalid"
export const CREATE_ORDER_BLANK_CART = "cart have no items"
export const ADMIN_CLOSE_MODAL_ORDER_SUCCESS = "order success"
export const ADMIN_ORDER_INIT_SUCCESS = "signal fetch initial order success"
export const ADMIN_ORDER_FETCH_SUCCESS = "signal fetch  order success"
export const ADMIN_ORDER_FETCH_PENDING = "signal fetch  order pending"
export const ADMIN_ORDER_FETCH_ERROR = "signal fetch  order error"
export const ADMIN_ORDER_MINICART_QUANTITY_PLUS =" signal plus quantity minicart"
export const ADMIN_ORDER_MINICART_QUANTITY_SUBTRACT =" signal subtract quantity minicart"
export const ADMIN_ORDER_PAGE_CHANGE = "signal change page admin ORDER"

//EDIT
export const ADMIN_ORDER_CREATE_NEXT_STEP = "signal next step modal create"
export const ADMIN_ORDER_CREATE_PREV_STEP = "signal prev step modal create"
export const ADMIN_ORDER_OPEN_EDIT_MODAL = "signal open modal edit order"
export const ADMIN_ORDER_FETCH_TO_EDIT_PENDING = "signal fetching data of order to edit"
export const ADMIN_ORDER_FETCH_TO_EDIT_ERROR = "signal fetching data of order to edit error"
export const ADMIN_ORDER_EDIT_ADDRESS_CHANGE = "signal change input ADDRESS modal EDIT ORDER"
export const ADMIN_ORDER_EDIT_CITY_CHANGE = "signal change select CITY modal EDIT ORDER"
export const ADMIN_ORDER_EDIT_CART_CHANGE = "signal change select cart modal EDIT ORDER"
export const ADMIN_ORDER_EDIT_SHIPPED_TIME_CHANGE = "signal change date picker EDIT order"
export const ADMIN_ORDER_EDIT_NOTE_CHANGE = "signal change note EDIT order"
export const ADMIN_ORDER_EDIT_STATUS_CHANGE = "signal change status EDIT order"
export const DATE_PICKER_EDIT_CHANGE = "signal change date time picker edit modal"
export const ADMIN_ORDER_MINICART_EDIT_QUANTITY_PLUS =" signal plus quantity minicart modal edit"
export const ADMIN_ORDER_MINICART_EDIT_QUANTITY_SUBTRACT =" signal subtract quantity minicart modal edit"
export const EDIT_ORDER_ERROR = "EDIT order Error"
export const EDIT_ORDER_PENDING = "EDIT order pending"
export const EDIT_ORDER_SUCCESS = "EDIT order success"
export const EDIT_ORDER_BLANK_CART = "cart edit modal have no items"
// DELETE
export const ADMIN_ORDER_OPEN_DELETE_MODAL="signal open modal delete order"
export const ADMIN_ORDER_DELETE_ERROR = "signal DELETE order error"
export const ADMIN_ORDER_DELETE_SUCCESS = "signal DELETE order success"
// FILTER
export const ADMIN_ORDER_FILTER_CODE_CHANGE = "signal change input CODE FILTER "
export const ADMIN_ORDER_FILTER_STATUS_CHANGE = "signal change input STATUS FILTER "
export const ADMIN_ORDER_FILT = "signal filting order to admin "
// USER
// READ
export const ADMIN_USER_INIT_SUCCESS = "signal fetch initial user success"
export const ADMIN_USER_INIT_ERROR = "signal fetch initial user error"
export const ADMIN_USER_INIT_PENDING = "signal fetch initial user pending"
export const ADMIN_USER_FETCH_SUCCESS = "signal fetch FETCH admin user success"
export const ADMIN_USER_FETCH_ERROR = "signal fetch FETCH admin user error"
export const ADMIN_USER_FETCH_PENDING = "signal fetch FETCH admin user pending"
export const ADMIN_USER_PAGE_CHANGE = "signal change page admin USER"

// create
export const ADMIN_USER_OPEN_ADD_MODAL = "signal open modal add USER"
export const ADMIN_USER_CLOSE_MODAL = "signal close modal USER"
export const ADMIN_USER_ADD_NAME_CHANGE = "signal change input name modal add USER"
export const ADMIN_USER_ADD_EMAIL_CHANGE = "signal change select EMAIL modal add USER"
export const ADMIN_USER_ADD_PHONE_CHANGE = "signal change input PHONE modal add USER"
export const ADMIN_USER_ADD_PASSWORD_CHANGE = "signal change input password modal add USER"
export const ADMIN_USER_ADD_CONFIRM_PASSWORD_CHANGE = "signal change input cf password modal add USER"
export const ADMIN_USER_ADD_CONFIRM = "signal click button create user"
export const ADMIN_USER_ADD_NAME_INVALID = "signal input name modal add user invalid"
export const ADMIN_USER_ADD_PHONE_INVALID = "signal select PHONE modal add USER invalid"
export const ADMIN_USER_ADD_PASSWORD_INVALID = "signal input PASSWORD modal add USER invalid"
export const ADMIN_USER_ADD_CONFIRM_INVALID = "signal input CONFIRM modal add USER invalid"
export const ADMIN_USER_ADD_EMAIL_INVALID = "signal input EMAIL modal add USER invalid"

export const ADMIN_USER_CREATE_ERROR = "signal create user error"
export const ADMIN_USER_CREATE_PENDIND = "signal create user pending"
export const ADMIN_USER_CREATE_SUCCESS = "signal create user success"
// update
export const ADMIN_USER_OPEN_EDIT_MODAL = "signal open modal edit USER"
export const ADMIN_USER_FETCH_TO_EDIT_ERROR = "signal FETCH_TO_EDIT user error"
export const ADMIN_USER_FETCH_TO_EDIT_PENDIND = "signal FETCH_TO_EDIT user pending"
export const ADMIN_USER_EDIT_NAME_CHANGE = "signal change input name modal EDIT USER"
export const ADMIN_USER_EDIT_EMAIL_CHANGE = "signal change select EMAIL modal EDIT USER"
export const ADMIN_USER_EDIT_ROLE_CHANGE = "signal change input ROLE modal EDIT USER"
export const ADMIN_USER_EDIT_NAME_INVALID = "signal input name modal EDIT user invalid"
export const ADMIN_USER_EDIT_EMAIL_INVALID = "signal select EMAIL modal EDIT USER invalid"
export const ADMIN_USER_EDIT_ERROR = "signal EDIT user error"
export const ADMIN_USER_EDIT_PENDIND = "signal EDIT user pending"
export const ADMIN_USER_EDIT_SUCCESS = "signal EDIT user success"