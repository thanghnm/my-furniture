/* eslint-disable */

export const ADD_TO_CART = "Signal click button add to cart"
export const FILTER_PRICE_CHANGE = "signal change price radio filter"
export const FILTER_TYPE_CHANGE = "signal change type radio filter"
export const FILTER_NAME_CHANGE = "signal change name input filter"
export const MENU_PRODUCT_TYPE_FETCH_PENDIND = "get product type for filter fetch PENDING"
export const MENU_PRODUCT_TYPE_FETCH_SUCCESS = "get product type for filter fetch SUCCESS"
export const MENU_PRODUCT_TYPE_FETCH_ERROR = "get product type for filter fetch ERROR"
export const MENU_PRODUCT_FETCH_PENDIND = "get product fetch PENDING"
export const MENU_PRODUCT_FETCH_SUCCESS = "get product fetch SUCCESS"
export const MENU_PRODUCT_FETCH_ERROR = "get product fetch ERROR"
export const MENU_PRODUCT_PAGE_CHANGE = "get product page change"
export const MENU_FILTER_CLICK = "get product page change"

